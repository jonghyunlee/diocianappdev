package com.diocian.diocianappdev.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.util.ImageFetcher;

import java.util.ArrayList;

public class PortletVideoRecyclerAdapter extends RecyclerView.Adapter<PortletVideoRecyclerAdapter.ViewHolder> {

    private ArrayList<Post> videos;
    private ImageFetcher imageFetcher;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView genre_name;
        public ImageView image;
        public TextView title;
        public TextView creator_name;
        public String post_sn;
        public TextView post_snTextView;
        public String youtube_video_id;

        public ViewHolder(View view) {
            super(view);
            genre_name = (TextView) view.findViewById(R.id.genre_name);
            image = (ImageView) view.findViewById(R.id.image);
            title = (TextView) view.findViewById(R.id.title);
            creator_name = (TextView) view.findViewById(R.id.creator_name);
            post_snTextView = (TextView) view.findViewById(R.id.post_snTextView);
        }
    }

    public PortletVideoRecyclerAdapter(ArrayList<Post> posts_param, ImageFetcher imageFetcher, Context context) {
        videos = posts_param;
        this.imageFetcher = imageFetcher;
        this.context = context;
    }

    @Override
    public PortletVideoRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.portlet_item_video, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.genre_name.setText(videos.get(position).getGenre_name());
        holder.title.setText(videos.get(position).getTitle());
        holder.creator_name.setText(videos.get(position).getCreator_name());
        imageFetcher.setImageToImageView(videos.get(position).getImage_file_url(), holder.image, 104);

        holder.post_sn = videos.get(position).getPost_sn();
        holder.post_snTextView.setText(holder.post_sn);
        holder.youtube_video_id = videos.get(position).getYoutube_video_id();

        // TODO : Video portlet link UI(or interaction 표준화

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ((MainActivity) context).viewVideo(holder.post_sn);
                Intent i = new Intent(Intent.ACTION_VIEW);
                Uri u = Uri.parse("https://www.youtube.com/watch?v=" +holder.youtube_video_id);
                i.setData(u);
                context.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return videos.size();
    }

}
