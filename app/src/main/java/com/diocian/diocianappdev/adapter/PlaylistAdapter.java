package com.diocian.diocianappdev.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.model.Playlist;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;

import java.util.ArrayList;

public class PlaylistAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Playlist> playlists;
    private ImageFetcher imageFetcher;
    private Context context;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;

    private boolean isFooter = true;
    private boolean autoStart = false;
    private boolean isReset = false;
    private String post_sn = "";

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View view) {
            super(view);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout linearLayoutPlaylist;
        public ImageView imageViewCoverImage;
        public TextView textViewTitle;
        public TextView textViewArtistName;
        public ImageButton imageButtonPlay;
        public ImageButton imageButtonLike;
        public ImageButton imageButtonDelete;
        public String playlist_sn;
        public String target_sn;
        public String price;
        public String cover_image_url;
        public String audio_file_url;
        public TextView textViewIsActive;
        public TextView textViewPostSn;
        public TextView textViewPlaylistSn;
        public TextView textViewCoverImageUrl;
        public TextView textViewAudio_file_url;

        public ViewHolder(View view) {
            super(view);
            linearLayoutPlaylist = (LinearLayout) view.findViewById(R.id.linearLayoutPlaylist);
            imageViewCoverImage = (ImageView) view.findViewById(R.id.imageViewCoverImage);
            textViewTitle = (TextView) view.findViewById(R.id.textViewTitle);
            textViewArtistName = (TextView) view.findViewById(R.id.textViewArtistName);
            imageButtonPlay = (ImageButton) view.findViewById(R.id.imageButtonPlay);
            imageButtonLike = (ImageButton) view.findViewById(R.id.imageButtonLike);
            imageButtonDelete = (ImageButton) view.findViewById(R.id.imageButtonDelete);
            textViewIsActive = (TextView) view.findViewById(R.id.textViewIsActive);
            textViewPostSn = (TextView) view.findViewById(R.id.textViewPostSn);
            textViewPlaylistSn = (TextView) view.findViewById(R.id.textViewPlaylistSn);
            textViewCoverImageUrl = (TextView) view.findViewById(R.id.textViewCoverImageUrl);
            textViewAudio_file_url = (TextView) view.findViewById(R.id.textViewAudio_file_url);
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View view) {
            super(view);
        }
    }

    public PlaylistAdapter(ArrayList<Playlist> playlists, ImageFetcher imageFetcher, Context context, boolean autoStart, boolean isReset, String post_sn) {
        this.playlists = playlists;
        this.imageFetcher = imageFetcher;
        this.context = context;
        this.autoStart = autoStart;
        this.isReset = isReset;
        this.post_sn = post_sn;
    }

    @Override
    public int getItemViewType(int position) {
        if (isFooter) {
            if (position == 0) {
                return TYPE_HEADER;
            } else if (position == (playlists.size() + 1)) {
                return TYPE_FOOTER;
            }
        } else {
            if (position == 0) {
                return TYPE_HEADER;
            }
        }
        return TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_header_none, parent, false);
            return new HeaderViewHolder(view);
        } else if (viewType == TYPE_FOOTER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_footer, parent, false);
            return new FooterViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_playlist, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder itemHolder = (ViewHolder) holder;
            Playlist playlist = playlists.get(position - 1);

            if (playlist.getCover_image_url().equals("") || playlist.getCover_image_url().equals(GlobalConstants.DOMAIN)) {
                itemHolder.imageViewCoverImage.setImageResource(R.drawable.image_none);
                itemHolder.cover_image_url = "";
            } else {
                imageFetcher.setImageToImageView(playlist.getCover_image_url(), itemHolder.imageViewCoverImage, 300);
                itemHolder.cover_image_url = playlist.getCover_image_url();
                itemHolder.textViewCoverImageUrl.setText(playlist.getCover_image_url());
            }

            itemHolder.audio_file_url = playlist.getAudio_file_url();
            itemHolder.textViewPostSn.setText(playlist.getTarget_sn());
            itemHolder.textViewPlaylistSn.setText(playlist.getPlaylist_sn());
            itemHolder.textViewAudio_file_url.setText(playlist.getAudio_file_url());

            itemHolder.textViewTitle.setText(playlist.getTitle());
            if (playlist.getArtist_name().equals("")) {
                itemHolder.textViewArtistName.setText(playlist.getMember_name());
            } else {
                itemHolder.textViewArtistName.setText(playlist.getArtist_name());
            }

            if (!playlist.getAudio_file_url().equals(GlobalConstants.DOMAIN)) {
                itemHolder.linearLayoutPlaylist.setVisibility(View.VISIBLE);
            } else {
                itemHolder.linearLayoutPlaylist.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
                itemHolder.textViewIsActive.setTag("");
                itemHolder.textViewPostSn.setTag("");
                itemHolder.textViewPlaylistSn.setTag("");
                itemHolder.linearLayoutPlaylist.setTag("");
            }

            if(post_sn.equals("")){
                if (position == 1) {
                    itemHolder.linearLayoutPlaylist.setBackgroundColor(ContextCompat.getColor(context, R.color.DIOCIAN));
                    itemHolder.textViewIsActive.setText("Y");
                } else {
                    itemHolder.textViewIsActive.setText("N");
                }
            }else{
                if (playlist.getTarget_sn().equals(post_sn)) {
                    itemHolder.linearLayoutPlaylist.setBackgroundColor(ContextCompat.getColor(context, R.color.DIOCIAN));
                    itemHolder.textViewIsActive.setText("Y");
                } else {
                    itemHolder.textViewIsActive.setText("N");
                }
            }


            itemHolder.linearLayoutPlaylist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity) context).setPlaylistActive(itemHolder.textViewPostSn.getText().toString());
//                    ArrayList<View> views = Utils.getViewsByTag((ViewGroup) v.getParent(), "isActive");
//                    for (int i = 0; i < views.size(); i++) {
//                        if (((TextView) views.get(i)).getText().equals("Y")) {
//                            ((LinearLayout) views.get(i).getParent()).setBackgroundColor(ContextCompat.getColor(context, R.color.dummy));
//                            ((TextView) views.get(i)).setText("N");
//                        }
//                    }
//                    v.setBackgroundColor(ContextCompat.getColor(context, R.color.DIOCIAN));
//                    itemHolder.textViewIsActive.setText("Y");
//                    ((MainActivity) context).setBottomPlayer(true, itemHolder.textViewTitle.getText().toString(), itemHolder.textViewArtistName.getText().toString(), itemHolder.cover_image_url, itemHolder.audio_file_url);

                }
            });

            itemHolder.imageButtonDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if(((TextView) ((LinearLayout) v.getParent()).findViewById(R.id.textViewIsActive)).getText().equals("Y"))
                    ((MainActivity) context).deletePlaylist(itemHolder.textViewPlaylistSn.getText().toString());
                }
            });

        } else if (holder instanceof HeaderViewHolder) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        } else if (holder instanceof FooterViewHolder) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        }
    }

    @Override
    public int getItemCount() {
        if (isFooter) {
            return playlists.size() + 2;
        }
        return playlists.size() + 1;
    }

    public void removeFooter() {
        isFooter = false;
    }

}
