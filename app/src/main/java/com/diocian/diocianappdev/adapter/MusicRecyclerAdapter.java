package com.diocian.diocianappdev.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.ImageFetcher;

import java.util.ArrayList;

public class MusicRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Post> musics;
    private ImageFetcher imageFetcher;
    private Context context;
    RecyclerView.Adapter adapter = this;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;

    private boolean isFooter = true;

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View view) {
            super(view);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public View itemView;
        public TextView genre_name;
        public ImageView image;
        public TextView title;
        public TextView artist_name;
        public TextView like_count;
        public TextView comment_count;
        public TextView creator_name;
        public ImageView creator_image;
        public String audio_file_url;
        public String post_sn;
        public TextView post_snTextView;
        public String creator_sn;
        public String follow_sn;
        public ImageButton imageButtonFollow;
        public String my_like_count;
        public ImageButton imageButtonLike;

        public ViewHolder(View view) {
            super(view);
            itemView = view;
            genre_name = (TextView) view.findViewById(R.id.genre_name);
            image = (ImageView) view.findViewById(R.id.image);
            title = (TextView) view.findViewById(R.id.title);
            artist_name = (TextView) view.findViewById(R.id.artist_name);
            like_count = (TextView) view.findViewById(R.id.like_count);
            comment_count = (TextView) view.findViewById(R.id.comment_count);
            creator_name = (TextView) view.findViewById(R.id.creator_name);
            creator_image = (ImageView) view.findViewById(R.id.creator_image);
            post_snTextView = (TextView) view.findViewById(R.id.post_sn);
            imageButtonFollow = (ImageButton) view.findViewById(R.id.imageButtonFollow);
            imageButtonLike = (ImageButton) view.findViewById(R.id.imageButtonLike);
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View view) {
            super(view);
        }
    }

    public MusicRecyclerAdapter(ArrayList<Post> posts_param, ImageFetcher imageFetcher, Context context) {
        musics = posts_param;
        this.imageFetcher = imageFetcher;
        this.context = context;

    }

    @Override
    public int getItemViewType(int position) {
        if (isFooter) {
            if (position == 0) {
                return TYPE_HEADER;
            } else if (position == (musics.size() + 1)) {
                return TYPE_FOOTER;
            }
        } else {
            if (position == 0) {
                return TYPE_HEADER;
            }
        }
        return TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_header_none, parent, false);
            return new HeaderViewHolder(view);
        } else if (viewType == TYPE_FOOTER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_footer, parent, false);
            return new FooterViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_music, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder itemHolder = (ViewHolder) holder;

            final Post music = musics.get(position - 1);
            itemHolder.genre_name.setText(music.getGenre_name());
            imageFetcher.setImageToImageView(music.getImage_file_url(), itemHolder.image, 168);
            itemHolder.title.setText(music.getTitle());
            itemHolder.artist_name.setText(music.getArtist_name());
            itemHolder.like_count.setText(music.getLike_count());
            itemHolder.comment_count.setText(music.getComment_count());
            itemHolder.creator_name.setText(music.getCreator_name());
            if (music.getCreator_image_file_url().equals(GlobalConstants.DOMAIN)) {
                itemHolder.creator_image.setImageResource(R.drawable.image_none2);
            }else{
                imageFetcher.setImageToImageView(music.getCreator_image_file_url(), itemHolder.creator_image, 24);
            }
            itemHolder.audio_file_url = music.getAudio_file_url();
            itemHolder.post_sn = music.getPost_sn();
            itemHolder.post_snTextView.setText(itemHolder.post_sn);

            // TODO : Music link UI(or interaction 표준화

            itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).playMediaPlayer(music);
                }
            });

            itemHolder.creator_sn = music.getCreator_sn();
            itemHolder.creator_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewMember(itemHolder.creator_sn);
                }
            });
            itemHolder.creator_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewMember(itemHolder.creator_sn);
                }
            });

            itemHolder.follow_sn = music.getFollow_sn();
            if (itemHolder.follow_sn != null && !itemHolder.follow_sn.equals("")) {
                itemHolder.imageButtonFollow.setColorFilter(ContextCompat.getColor(context, R.color.DIOCIAN));
            } else {
                itemHolder.imageButtonFollow.setColorFilter(null);
            }

            itemHolder.imageButtonFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int myFollowCount;
                    if (itemHolder.follow_sn != null && !itemHolder.follow_sn.equals("")) {
                        myFollowCount = 1;
                    } else {
                        myFollowCount = 0;
                    }
                    ((MainActivity) context).setFollow2(itemHolder.creator_sn, myFollowCount, position - 1, musics, adapter);
                }
            });

            itemHolder.my_like_count = music.getMy_like_count();
            if (itemHolder.my_like_count != null && itemHolder.my_like_count.equals("1")) {
                itemHolder.imageButtonLike.setColorFilter(ContextCompat.getColor(context, R.color.DIOCIAN));
            } else {
                itemHolder.imageButtonLike.setColorFilter(null);
            }

            itemHolder.imageButtonLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).setLike2("post", itemHolder.post_sn, itemHolder.my_like_count, position - 1, musics, adapter);
                }
            });

        } else if (holder instanceof HeaderViewHolder) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        } else if (holder instanceof FooterViewHolder) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        }
    }

    @Override
    public int getItemCount() {
        if (isFooter) {
            return musics.size() + 2;
        }
        return musics.size() + 1;
    }

    public void removeFooter() {
        isFooter = false;
    }

}
