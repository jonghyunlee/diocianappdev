package com.diocian.diocianappdev.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.ImageFetcher;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class PortletRecyclerAdapter extends RecyclerView.Adapter<PortletRecyclerAdapter.ViewHolder> {

    private ArrayList<Post> posts;
    private ImageFetcher imageFetcher;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        public TextView postSnTextView;
        public ImageView coverImageView;
        public TextView titleTextView;
        public TextView subTitleTextView;
        public ImageButton playImageButton;
        public ImageButton downloadImageButton;
        public ImageButton playVideoImageButton;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            postSnTextView = (TextView) view.findViewById(R.id.postSnTextView);
            coverImageView = (ImageView) view.findViewById(R.id.coverImageView);
            titleTextView = (TextView) view.findViewById(R.id.titleTextView);
            subTitleTextView = (TextView) view.findViewById(R.id.subTitleTextView);
            playImageButton = (ImageButton) view.findViewById(R.id.playImageButton);
            downloadImageButton = (ImageButton) view.findViewById(R.id.downloadImageButton);
            playVideoImageButton = (ImageButton) view.findViewById(R.id.playVideoImageButton);
        }
    }

    public PortletRecyclerAdapter(ArrayList<Post> posts_param, ImageFetcher imageFetcher, Context context) {
        posts = posts_param;
        this.imageFetcher = imageFetcher;
        this.context = context;
    }

    @Override
    public PortletRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.portlet_item_source, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.postSnTextView.setText(posts.get(position).getPost_sn());
        imageFetcher.setImageToImageView(posts.get(position).getImage_file_url(), holder.coverImageView, 128);
        holder.titleTextView.setText(posts.get(position).getTitle());
        holder.subTitleTextView.setText(posts.get(position).getArtist_name());

//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ((MainActivity) context).viewMusic(holder.post_sn);
//                ((MainActivity) context).playMediaPlayer(musics.get(position));
//            }
//        });

        holder.playImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) context).playMediaPlayer(posts.get(position));
            }
        });

        holder.downloadImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                Uri u = null;
                try {
                    u = Uri.parse(GlobalConstants.DOMAIN + "/getMp3FileByPath2.do?audio_file_url=" + URLEncoder.encode(posts.get(position).getAudio_download_file_url(), "UTF-8") + "&title=" + URLEncoder.encode(posts.get(position).getTitle(), "UTF-8") + "&artist_name=" + URLEncoder.encode(posts.get(position).getArtist_name(), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                i.setData(u);
                context.startActivity(i);
            }
        });

        holder.playVideoImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                Uri u = Uri.parse("https://www.youtube.com/watch?v=" + posts.get(position).getYoutube_video_id());
                i.setData(u);
                context.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

}
