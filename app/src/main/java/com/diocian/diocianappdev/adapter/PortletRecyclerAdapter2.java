package com.diocian.diocianappdev.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.ImageFetcher;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class PortletRecyclerAdapter2 extends RecyclerView.Adapter<PortletRecyclerAdapter2.ViewHolder> {

    private ArrayList<Post> posts;
    private ImageFetcher imageFetcher;
    private Context context;
    RecyclerView.Adapter adapter = this;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        public TextView postSnTextView;
        public ImageView coverImageView;
        public TextView titleTextView;
        public ImageButton playVideoImageButton;
        public ImageButton likeImageButton;
        public TextView likeCountTextView;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            postSnTextView = (TextView) view.findViewById(R.id.postSnTextView);
            coverImageView = (ImageView) view.findViewById(R.id.coverImageView);
            titleTextView = (TextView) view.findViewById(R.id.titleTextView);
            playVideoImageButton = (ImageButton) view.findViewById(R.id.playVideoImageButton);
            likeImageButton = (ImageButton) view.findViewById(R.id.likeImageButton);
            likeCountTextView = (TextView) view.findViewById(R.id.likeCountTextView);
        }
    }

    public PortletRecyclerAdapter2(ArrayList<Post> posts_param, ImageFetcher imageFetcher, Context context) {
        posts = posts_param;
        this.imageFetcher = imageFetcher;
        this.context = context;
    }

    @Override
    public PortletRecyclerAdapter2.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.portlet_item_video2, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.postSnTextView.setText(posts.get(position).getPost_sn());
        imageFetcher.setImageToImageView("http://img.youtube.com/vi/" +  posts.get(position).getYoutube_video_id() + "/0.jpg", holder.coverImageView, 128);
        holder.titleTextView.setText(posts.get(position).getTitle());
        holder.likeCountTextView.setText(posts.get(position).getLike_count());

//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ((MainActivity) context).viewMusic(holder.post_sn);
//                ((MainActivity) context).playMediaPlayer(musics.get(position));
//            }
//        });

        holder.playVideoImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                Uri u = Uri.parse("https://www.youtube.com/watch?v=" + posts.get(position).getYoutube_video_id());
                i.setData(u);
                context.startActivity(i);
            }
        });

        if (posts.get(position).getMy_like_count() != null && posts.get(position).getMy_like_count().equals("1")) {
            holder.likeImageButton.setColorFilter(ContextCompat.getColor(context, R.color.DIOCIAN));
            holder.likeImageButton.setAlpha(1.0f);
        } else {
            holder.likeImageButton.setColorFilter(null);
            holder.likeImageButton.setAlpha(0.54f);
        }

        holder.likeImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) context).setLike3("post", posts.get(position).getPost_sn(), posts.get(position).getMy_like_count(), position, posts, adapter);
            }
        });

    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

}
