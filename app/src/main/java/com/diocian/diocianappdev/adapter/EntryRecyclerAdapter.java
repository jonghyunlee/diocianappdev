package com.diocian.diocianappdev.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.ImageFetcher;

import java.util.ArrayList;

public class EntryRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Post> videos;
    private ImageFetcher imageFetcher;
    private Context context;
    RecyclerView.Adapter adapter = this;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;

    private boolean isFooter = true;

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View view) {
            super(view);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView genre_name;
        public ImageView image;
        public TextView title;
        public TextView created_datetime;
        public TextView like_count;
        public TextView comment_count;
        public TextView creator_name;
        public ImageView creator_image;
        public String post_sn;
        public TextView post_snTextView;
        public String youtube_video_id;
        public String my_like_count;
        public ImageButton imageButtonThumbUp;
        public String creator_sn;
        public String follow_sn;
        public ImageButton imageButtonFollow;
        public String audio_file_url;

        public ViewHolder(View view) {
            super(view);
            genre_name = (TextView) view.findViewById(R.id.genre_name);
            image = (ImageView) view.findViewById(R.id.image);
            title = (TextView) view.findViewById(R.id.title);
            created_datetime = (TextView) view.findViewById(R.id.created_datetime);
            like_count = (TextView) view.findViewById(R.id.like_count);
            comment_count = (TextView) view.findViewById(R.id.comment_count);
            creator_name = (TextView) view.findViewById(R.id.creator_name);
            creator_image = (ImageView) view.findViewById(R.id.creator_image);
            post_snTextView = (TextView) view.findViewById(R.id.post_snTextView);
            imageButtonThumbUp = (ImageButton) view.findViewById(R.id.imageButtonThumbUp);
            imageButtonFollow = (ImageButton) view.findViewById(R.id.imageButtonFollow);
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View view) {
            super(view);
        }
    }

    public EntryRecyclerAdapter(ArrayList<Post> posts_param, ImageFetcher imageFetcher, Context context) {
        videos = posts_param;
        this.imageFetcher = imageFetcher;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (isFooter) {
            if (position == 0) {
                return TYPE_HEADER;
            } else if (position == (videos.size() + 1)) {
                return TYPE_FOOTER;
            }
        } else {
            if (position == 0) {
                return TYPE_HEADER;
            }
        }
        return TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_header_entries, parent, false);
            return new HeaderViewHolder(view);
        } else if (viewType == TYPE_FOOTER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_footer, parent, false);
            return new FooterViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_entry, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder itemHolder = (ViewHolder) holder;
            final Post video = videos.get(position - 1);
            itemHolder.genre_name.setText(video.getGenre_name());
            imageFetcher.setImageToImageView(video.getImage_file_url(), itemHolder.image, 168);
            itemHolder.title.setText(video.getTitle());
            itemHolder.created_datetime.setText(video.getCreated_datetime());
            itemHolder.like_count.setText(video.getLike_count());
            itemHolder.comment_count.setText(video.getComment_count());
            itemHolder.creator_name.setText(video.getCreator_name());
            if (video.getCreator_image_file_url().equals(GlobalConstants.DOMAIN)) {
                itemHolder.creator_image.setImageResource(R.drawable.image_none2);
            } else {
                imageFetcher.setImageToImageView(video.getCreator_image_file_url(), itemHolder.creator_image, 24);
            }
            itemHolder.post_sn = video.getPost_sn();
            itemHolder.post_snTextView.setText(itemHolder.post_sn);
            itemHolder.youtube_video_id = video.getYoutube_video_id();
            itemHolder.audio_file_url = video.getAudio_file_url();

            itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(GlobalConstants.LOG_TAG,"itemHolder.audio_file_url : " + itemHolder.audio_file_url);
                    if(!(itemHolder.audio_file_url == null || itemHolder.audio_file_url.equals(""))) {
                        ((MainActivity) context).playMediaPlayer(video);
                    } else {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        Uri u = Uri.parse("https://www.youtube.com/watch?v=" + itemHolder.youtube_video_id);
                        i.setData(u);
                        context.startActivity(i);
                    }
                }
            });

            itemHolder.my_like_count = video.getMy_like_count();
            if (itemHolder.my_like_count != null && itemHolder.my_like_count.equals("1")) {
                itemHolder.imageButtonThumbUp.setColorFilter(ContextCompat.getColor(context, R.color.DIOCIAN));
            } else {
                itemHolder.imageButtonThumbUp.setColorFilter(null);
            }

            itemHolder.imageButtonThumbUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).setLike2("post", itemHolder.post_sn, itemHolder.my_like_count, position - 1, videos, adapter);
                }
            });

            itemHolder.creator_sn = video.getCreator_sn();
            itemHolder.creator_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewMember(itemHolder.creator_sn);
                }
            });
            itemHolder.creator_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewMember(itemHolder.creator_sn);
                }
            });

            // TODO : Competition 등록시간 안내

            itemHolder.follow_sn = video.getFollow_sn();
            if (itemHolder.follow_sn != null && !itemHolder.follow_sn.equals("")) {
                itemHolder.imageButtonFollow.setColorFilter(ContextCompat.getColor(context, R.color.DIOCIAN));
            } else {
                itemHolder.imageButtonFollow.setColorFilter(null);
            }

            itemHolder.imageButtonFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int myFollowCount;
                    if (itemHolder.follow_sn != null && !itemHolder.follow_sn.equals("")) {
                        myFollowCount = 1;
                    } else {
                        myFollowCount = 0;
                    }
                    ((MainActivity) context).setFollow2(itemHolder.creator_sn, myFollowCount, position - 1, videos, adapter);
                }
            });

        } else if (holder instanceof HeaderViewHolder) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        } else if (holder instanceof FooterViewHolder) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        }
    }

    @Override
    public int getItemCount() {
        if (isFooter) {
            return videos.size() + 2;
        }
        return videos.size() + 1;
    }

    public void removeFooter() {
        isFooter = false;
    }

    //    public void setLike(String target_dc, String target_sn, final TextView like_count, final ImageButton imageButtonThumbUp, final String my_like_count) {


}
