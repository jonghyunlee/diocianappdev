package com.diocian.diocianappdev.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.R;

import java.util.ArrayList;

public class PortletStudioRecyclerAdapter extends RecyclerView.Adapter<PortletStudioRecyclerAdapter.ViewHolder> {

    private ArrayList<Post> studios;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView genre_name;
        public TextView job_name;
        public TextView title;
        public TextView creator_name;
        public String post_sn;

        public ViewHolder(View view) {
            super(view);
            genre_name = (TextView) view.findViewById(R.id.genre_name);
            job_name = (TextView) view.findViewById(R.id.job_name);
            title = (TextView) view.findViewById(R.id.title);
            creator_name = (TextView) view.findViewById(R.id.creator_name);
        }
    }

    public PortletStudioRecyclerAdapter(ArrayList<Post> posts_param, Context context) {
        studios = posts_param;
        this.context = context;
    }

    @Override
    public PortletStudioRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.portlet_item_studio, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.genre_name.setText(studios.get(position).getGenre_name());
        holder.job_name.setText(studios.get(position).getJob_name());
        holder.title.setText(studios.get(position).getTitle());
        holder.creator_name.setText(studios.get(position).getCreator_name());
        holder.post_sn = studios.get(position).getPost_sn();

        // TODO : Studio portlet link UI(or interaction 표준화

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) context).viewStudio(holder.post_sn);
            }
        });


    }

    @Override
    public int getItemCount() {
        return studios.size();
    }

}
