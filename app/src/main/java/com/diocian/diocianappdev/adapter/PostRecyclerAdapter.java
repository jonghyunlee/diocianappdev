package com.diocian.diocianappdev.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.fragment.ListFragment;
import com.diocian.diocianappdev.fragment.MemberFragment;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.ImageFetcher;

import java.util.ArrayList;

public class PostRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Post> posts;
    private ImageFetcher imageFetcher;
    private Context context;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;

    private static final int TYPE_ARTICLE = 11;
    private static final int TYPE_STUDYNOTE = 12;
    private static final int TYPE_STUDIO = 13;
    private static final int TYPE_MUSIC = 14;
    private static final int TYPE_VIDEO = 15;
    private static final int TYPE_MARKET = 16;

    private boolean isFooter = true;

    private Fragment redirectFragment;
    private Bundle paramBundle;

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View view) {
            super(view);
        }
    }

    public class ViewHolderArticle extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView textViewDescription;
        public TextView created_datetime;
        public TextView like_count;
        public TextView comment_count;
        public TextView creator_name;
        public ImageView creator_image;
        public String post_sn;
        public TextView post_snTextView;
        public String creator_sn;
        public TextView textViewCreatorSn;
        public String follow_sn;
        public ImageButton imageButtonFollow;
        public String my_like_count;
        public ImageButton imageButtonLike;

        public ViewHolderArticle(View view) {
            super(view);
            image = (ImageView) view.findViewById(R.id.image);
            textViewDescription = (TextView) view.findViewById(R.id.textViewDescription);
            created_datetime = (TextView) view.findViewById(R.id.created_datetime);
            like_count = (TextView) view.findViewById(R.id.like_count);
            comment_count = (TextView) view.findViewById(R.id.comment_count);
            creator_name = (TextView) view.findViewById(R.id.creator_name);
            creator_image = (ImageView) view.findViewById(R.id.creator_image);
            textViewCreatorSn = (TextView) view.findViewById(R.id.textViewCreatorSn);
            post_snTextView = (TextView) view.findViewById(R.id.post_snTextView);
            imageButtonFollow = (ImageButton) view.findViewById(R.id.imageButtonFollow);
            imageButtonLike = (ImageButton) view.findViewById(R.id.imageButtonLike);
        }
    }

    public class ViewHolderStudynote extends RecyclerView.ViewHolder {
        public TextView genre_name;
        public TextView job_name;
        public ImageView image;
        public TextView title;
        public TextView created_datetime;
        public TextView like_count;
        public TextView comment_count;
        public TextView creator_name;
        public ImageView creator_image;
        public TextView price;
        public TextView duration;
        public String audio_file_url;
        public String post_sn;
        public TextView post_snTextView;
        public String creator_sn;
        public String follow_sn;
        public ImageButton imageButtonFollow;
        public String my_like_count;
        public ImageButton imageButtonLike;

        public ViewHolderStudynote(View view) {
            super(view);
            genre_name = (TextView) view.findViewById(R.id.genre_name);
            job_name = (TextView) view.findViewById(R.id.job_name);
            image = (ImageView) view.findViewById(R.id.image);
            title = (TextView) view.findViewById(R.id.title);
            created_datetime = (TextView) view.findViewById(R.id.created_datetime);
            like_count = (TextView) view.findViewById(R.id.like_count);
            comment_count = (TextView) view.findViewById(R.id.comment_count);
            creator_name = (TextView) view.findViewById(R.id.creator_name);
            creator_image = (ImageView) view.findViewById(R.id.creator_image);
            price = (TextView) view.findViewById(R.id.price);
            duration = (TextView) view.findViewById(R.id.duration);
            post_snTextView = (TextView) view.findViewById(R.id.post_sn);
            imageButtonFollow = (ImageButton) view.findViewById(R.id.imageButtonFollow);
            imageButtonLike = (ImageButton) view.findViewById(R.id.imageButtonLike);
        }
    }

    public class ViewHolderStudio extends RecyclerView.ViewHolder {
        public TextView genre_name;
        public TextView job_name;
        public ImageView image;
        public ImageView gradient;
        public TextView title;
        public TextView created_datetime;
        public TextView like_count;
        public TextView comment_count;
        public TextView creator_name;
        public ImageView creator_image;
        public String post_sn;
        public String creator_sn;
        public String follow_sn;
        public ImageButton imageButtonFollow;
        public String my_like_count;
        public ImageButton imageButtonLike;

        public ViewHolderStudio(View view) {
            super(view);
            genre_name = (TextView) view.findViewById(R.id.genre_name);
            job_name = (TextView) view.findViewById(R.id.job_name);
            image = (ImageView) view.findViewById(R.id.image);
            gradient = (ImageView) view.findViewById(R.id.gradient);
            title = (TextView) view.findViewById(R.id.title);
            created_datetime = (TextView) view.findViewById(R.id.created_datetime);
            like_count = (TextView) view.findViewById(R.id.like_count);
            comment_count = (TextView) view.findViewById(R.id.comment_count);
            creator_name = (TextView) view.findViewById(R.id.creator_name);
            creator_image = (ImageView) view.findViewById(R.id.creator_image);
            imageButtonFollow = (ImageButton) view.findViewById(R.id.imageButtonFollow);
            imageButtonLike = (ImageButton) view.findViewById(R.id.imageButtonLike);
        }
    }

    public class ViewHolderMusic extends RecyclerView.ViewHolder {
        public View itemView;
        public TextView genre_name;
        public ImageView image;
        public TextView title;
        public TextView artist_name;
        public TextView like_count;
        public TextView comment_count;
        public TextView creator_name;
        public ImageView creator_image;
        public String audio_file_url;
        public String post_sn;
        public TextView post_snTextView;
        public String creator_sn;
        public String follow_sn;
        public ImageButton imageButtonFollow;
        public String my_like_count;
        public ImageButton imageButtonLike;

        public ViewHolderMusic(View view) {
            super(view);
            itemView = view;
            genre_name = (TextView) view.findViewById(R.id.genre_name);
            image = (ImageView) view.findViewById(R.id.image);
            title = (TextView) view.findViewById(R.id.title);
            artist_name = (TextView) view.findViewById(R.id.artist_name);
            like_count = (TextView) view.findViewById(R.id.like_count);
            comment_count = (TextView) view.findViewById(R.id.comment_count);
            creator_name = (TextView) view.findViewById(R.id.creator_name);
            creator_image = (ImageView) view.findViewById(R.id.creator_image);
            post_snTextView = (TextView) view.findViewById(R.id.post_sn);
            imageButtonFollow = (ImageButton) view.findViewById(R.id.imageButtonFollow);
            imageButtonLike = (ImageButton) view.findViewById(R.id.imageButtonLike);
        }
    }

    public class ViewHolderVideo extends RecyclerView.ViewHolder {
        public TextView genre_name;
        public ImageView image;
        public TextView title;
        public TextView created_datetime;
        public TextView like_count;
        public TextView comment_count;
        public TextView creator_name;
        public ImageView creator_image;
        public String post_sn;
        public TextView post_snTextView;
        public String youtube_video_id;
        public String creator_sn;
        public String follow_sn;
        public ImageButton imageButtonFollow;
        public String my_like_count;
        public ImageButton imageButtonLike;

        public ViewHolderVideo(View view) {
            super(view);
            genre_name = (TextView) view.findViewById(R.id.genre_name);
            image = (ImageView) view.findViewById(R.id.image);
            title = (TextView) view.findViewById(R.id.title);
            created_datetime = (TextView) view.findViewById(R.id.created_datetime);
            like_count = (TextView) view.findViewById(R.id.like_count);
            comment_count = (TextView) view.findViewById(R.id.comment_count);
            creator_name = (TextView) view.findViewById(R.id.creator_name);
            creator_image = (ImageView) view.findViewById(R.id.creator_image);
            post_snTextView = (TextView) view.findViewById(R.id.post_snTextView);
            imageButtonFollow = (ImageButton) view.findViewById(R.id.imageButtonFollow);
            imageButtonLike = (ImageButton) view.findViewById(R.id.imageButtonLike);
        }
    }

    public class ViewHolderMarket extends RecyclerView.ViewHolder {
        public TextView price;
        public ImageView image;
        public TextView title;
        public TextView created_datetime;
        public TextView like_count;
        public TextView comment_count;
        public TextView creator_name;
        public ImageView creator_image;
        public String post_sn;
        public String creator_sn;
        public String follow_sn;
        public ImageButton imageButtonFollow;
        public String my_like_count;
        public ImageButton imageButtonLike;

        public ViewHolderMarket(View view) {
            super(view);
            price = (TextView) view.findViewById(R.id.price);
            image = (ImageView) view.findViewById(R.id.image);
            title = (TextView) view.findViewById(R.id.title);
            created_datetime = (TextView) view.findViewById(R.id.created_datetime);
            like_count = (TextView) view.findViewById(R.id.like_count);
            comment_count = (TextView) view.findViewById(R.id.comment_count);
            creator_name = (TextView) view.findViewById(R.id.creator_name);
            creator_image = (ImageView) view.findViewById(R.id.creator_image);
            imageButtonFollow = (ImageButton) view.findViewById(R.id.imageButtonFollow);
            imageButtonLike = (ImageButton) view.findViewById(R.id.imageButtonLike);
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View view) {
            super(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isFooter) {
            if (position == 0) {
                return TYPE_HEADER;
            } else if (position == (posts.size() + 1)) {
                return TYPE_FOOTER;
            }
        } else {
            if (position == 0) {
                return TYPE_HEADER;
            }
        }
        if (posts.get(position - 1).getTarget_dc().equals("article")) {
            return TYPE_ARTICLE;
        } else if (posts.get(position - 1).getTarget_dc().equals("study note")) {
            return TYPE_STUDYNOTE;
        } else if (posts.get(position - 1).getTarget_dc().equals("studio")) {
            return TYPE_STUDIO;
        } else if (posts.get(position - 1).getTarget_dc().equals("music")) {
            return TYPE_MUSIC;
        } else if (posts.get(position - 1).getTarget_dc().equals("video")) {
            return TYPE_VIDEO;
        } else if (posts.get(position - 1).getTarget_dc().equals("market")) {
            return TYPE_MARKET;
        }
        return TYPE_ITEM;
    }

    public PostRecyclerAdapter(ArrayList<Post> posts, ImageFetcher imageFetcher, Context context) {
        this.posts = posts;
        this.imageFetcher = imageFetcher;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_header_none, parent, false);
            return new HeaderViewHolder(view);
        } else if (viewType == TYPE_FOOTER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_footer, parent, false);
            return new FooterViewHolder(view);
        } else if (viewType == TYPE_ARTICLE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_article, parent, false);
            return new ViewHolderArticle(view);
        } else if (viewType == TYPE_STUDYNOTE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_studynote, parent, false);
            return new ViewHolderStudynote(view);
        } else if (viewType == TYPE_STUDIO) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_studio, parent, false);
            return new ViewHolderStudio(view);
        } else if (viewType == TYPE_MUSIC) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_music, parent, false);
            return new ViewHolderMusic(view);
        } else if (viewType == TYPE_VIDEO) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_video, parent, false);
            return new ViewHolderVideo(view);
        } else if (viewType == TYPE_MARKET) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_market, parent, false);
            return new ViewHolderMarket(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderArticle) {
            final ViewHolderArticle itemHolder = (ViewHolderArticle) holder;
            Post video = posts.get(position - 1);
            if (video.getImage_file_url() != null) {
                imageFetcher.setImageToImageView(video.getImage_file_url(), itemHolder.image, 168);
                itemHolder.image.setVisibility(View.VISIBLE);
            } else {
                itemHolder.image.setVisibility(View.GONE);
            }
            itemHolder.textViewDescription.setText(video.getDescription());
            itemHolder.created_datetime.setText(video.getCreated_datetime());
            itemHolder.like_count.setText(video.getLike_count());
            itemHolder.comment_count.setText(video.getComment_count());
            itemHolder.creator_name.setText(video.getCreator_name());
            if (video.getCreator_image_file_url().equals(GlobalConstants.DOMAIN)) {
                itemHolder.creator_image.setImageResource(R.drawable.image_none2);
            } else {
                imageFetcher.setImageToImageView(video.getCreator_image_file_url(), itemHolder.creator_image, 24);
            }
            itemHolder.post_sn = video.getPost_sn();
            itemHolder.post_snTextView.setText(itemHolder.post_sn);
            itemHolder.creator_sn = video.getCreator_sn();
            itemHolder.textViewCreatorSn.setText(itemHolder.creator_sn);

            // TODO : Article link UI(or interaction 표준화

            itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewArticle(itemHolder.post_sn);
                }
            });

            itemHolder.creator_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewMember(itemHolder.creator_sn);
                }
            });
            itemHolder.creator_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewMember(itemHolder.creator_sn);
                }
            });

            itemHolder.follow_sn = video.getFollow_sn();
            if (itemHolder.follow_sn != null && !itemHolder.follow_sn.equals("")) {
                itemHolder.imageButtonFollow.setColorFilter(ContextCompat.getColor(context, R.color.DIOCIAN));
            } else {
                itemHolder.imageButtonFollow.setColorFilter(null);
            }

            redirectFragment = video.getRedirectFragment();
            paramBundle = video.getParamBundle();

            itemHolder.imageButtonFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int myFollowCount;
                    if (itemHolder.follow_sn != null && !itemHolder.follow_sn.equals("")) {
                        myFollowCount = 1;
                    } else {
                        myFollowCount = 0;
                    }
                    ((MainActivity) context).setFollow(itemHolder.creator_sn, myFollowCount, redirectFragment, paramBundle);
                }
            });

            itemHolder.my_like_count = video.getMy_like_count();
            if (itemHolder.my_like_count != null && itemHolder.my_like_count.equals("1")) {
                itemHolder.imageButtonLike.setColorFilter(ContextCompat.getColor(context, R.color.DIOCIAN));
            } else {
                itemHolder.imageButtonLike.setColorFilter(null);
            }

            itemHolder.imageButtonLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).setLike("post", itemHolder.post_sn, itemHolder.my_like_count, redirectFragment, paramBundle);
                }
            });


        } else if (holder instanceof ViewHolderStudynote) {
            final ViewHolderStudynote itemHolder = (ViewHolderStudynote) holder;
            final Post studynote = posts.get(position - 1);
            if (!studynote.getJob_name().equals("")) {
                itemHolder.genre_name.setVisibility(View.GONE);
                itemHolder.job_name.setVisibility(View.VISIBLE);
            } else {
                itemHolder.genre_name.setVisibility(View.VISIBLE);
                itemHolder.job_name.setVisibility(View.GONE);
            }
            itemHolder.genre_name.setText(studynote.getGenre_name());
            itemHolder.job_name.setText(studynote.getJob_name());
            imageFetcher.setImageToImageView(studynote.getWave_image_file_url(), itemHolder.image, 168);
            itemHolder.title.setText(studynote.getTitle());
            itemHolder.created_datetime.setText(studynote.getCreated_datetime());
            itemHolder.like_count.setText(studynote.getLike_count());
            itemHolder.comment_count.setText(studynote.getComment_count());
            itemHolder.creator_name.setText(studynote.getCreator_name());
            if (studynote.getCreator_image_file_url().equals(GlobalConstants.DOMAIN)) {
                itemHolder.creator_image.setImageResource(R.drawable.image_none2);
            } else {
                imageFetcher.setImageToImageView(studynote.getCreator_image_file_url(), itemHolder.creator_image, 24);
            }
            itemHolder.price.setText(studynote.getPrice());
            itemHolder.duration.setText(studynote.getDuration());
            itemHolder.audio_file_url = studynote.getAudio_file_url();
            itemHolder.post_sn = studynote.getPost_sn();
            itemHolder.post_snTextView.setText(itemHolder.post_sn);
            itemHolder.creator_sn = studynote.getCreator_sn();

            // TODO : Study note link UI(or interaction 표준화

            itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    ((MainActivity) context).viewStudynote(itemHolder.post_sn);
                    ((MainActivity) context).playMediaPlayer(studynote);
                }
            });

            itemHolder.creator_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewMember(itemHolder.creator_sn);
                }
            });
            itemHolder.creator_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewMember(itemHolder.creator_sn);
                }
            });

            redirectFragment = studynote.getRedirectFragment();
            paramBundle = studynote.getParamBundle();

            itemHolder.follow_sn = studynote.getFollow_sn();
            if (itemHolder.follow_sn != null && !itemHolder.follow_sn.equals("")) {
                itemHolder.imageButtonFollow.setColorFilter(ContextCompat.getColor(context, R.color.DIOCIAN));
            } else {
                itemHolder.imageButtonFollow.setColorFilter(null);
            }

            itemHolder.imageButtonFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int myFollowCount;
                    if (itemHolder.follow_sn != null && !itemHolder.follow_sn.equals("")) {
                        myFollowCount = 1;
                    } else {
                        myFollowCount = 0;
                    }
                    ((MainActivity) context).setFollow(itemHolder.creator_sn, myFollowCount, redirectFragment, paramBundle);
                }
            });

            itemHolder.my_like_count = studynote.getMy_like_count();
            if (itemHolder.my_like_count != null && itemHolder.my_like_count.equals("1")) {
                itemHolder.imageButtonLike.setColorFilter(ContextCompat.getColor(context, R.color.DIOCIAN));
            } else {
                itemHolder.imageButtonLike.setColorFilter(null);
            }

            itemHolder.imageButtonLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).setLike("post", itemHolder.post_sn, itemHolder.my_like_count, redirectFragment, paramBundle);
                }
            });

        } else if (holder instanceof ViewHolderStudio) {
            final ViewHolderStudio itemHolder = (ViewHolderStudio) holder;
            Post studio = posts.get(position - 1);
            itemHolder.genre_name.setText(studio.getGenre_name());
            itemHolder.job_name.setText(studio.getJob_name());
            imageFetcher.setImageToImageView(studio.getImage_file_url(), itemHolder.image, 168);
            itemHolder.title.setText(studio.getTitle());
            itemHolder.created_datetime.setText(studio.getCreated_datetime());
            itemHolder.like_count.setText(studio.getLike_count());
            itemHolder.comment_count.setText(studio.getComment_count());
            itemHolder.creator_name.setText(studio.getCreator_name());
            if (studio.getCreator_image_file_url().equals(GlobalConstants.DOMAIN)) {
                itemHolder.creator_image.setImageResource(R.drawable.image_none2);
            } else {
                imageFetcher.setImageToImageView(studio.getCreator_image_file_url(), itemHolder.creator_image, 24);
            }
            itemHolder.post_sn = studio.getPost_sn();

            // TODO : Studio link UI(or interaction 표준화

            itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewStudio(itemHolder.post_sn);
                }
            });

            itemHolder.creator_sn = studio.getCreator_sn();
            itemHolder.creator_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewMember(itemHolder.creator_sn);
                }
            });
            itemHolder.creator_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewMember(itemHolder.creator_sn);
                }
            });

            redirectFragment = studio.getRedirectFragment();
            paramBundle = studio.getParamBundle();

            itemHolder.follow_sn = studio.getFollow_sn();
            if (itemHolder.follow_sn != null && !itemHolder.follow_sn.equals("")) {
                itemHolder.imageButtonFollow.setColorFilter(ContextCompat.getColor(context, R.color.DIOCIAN));
            } else {
                itemHolder.imageButtonFollow.setColorFilter(null);
            }

            itemHolder.imageButtonFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int myFollowCount;
                    if (itemHolder.follow_sn != null && !itemHolder.follow_sn.equals("")) {
                        myFollowCount = 1;
                    } else {
                        myFollowCount = 0;
                    }
                    ((MainActivity) context).setFollow(itemHolder.creator_sn, myFollowCount, redirectFragment, paramBundle);
                }
            });

            itemHolder.my_like_count = studio.getMy_like_count();
            if (itemHolder.my_like_count != null && itemHolder.my_like_count.equals("1")) {
                itemHolder.imageButtonLike.setColorFilter(ContextCompat.getColor(context, R.color.DIOCIAN));
            } else {
                itemHolder.imageButtonLike.setColorFilter(null);
            }

            itemHolder.imageButtonLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).setLike("post", itemHolder.post_sn, itemHolder.my_like_count, redirectFragment, paramBundle);
                }
            });

        } else if (holder instanceof ViewHolderMusic) {
            final ViewHolderMusic itemHolder = (ViewHolderMusic) holder;
            final Post music = posts.get(position - 1);
            itemHolder.genre_name.setText(music.getGenre_name());
            imageFetcher.setImageToImageView(music.getImage_file_url(), itemHolder.image, 168);
            itemHolder.title.setText(music.getTitle());
            itemHolder.artist_name.setText(music.getArtist_name());
            itemHolder.like_count.setText(music.getLike_count());
            itemHolder.comment_count.setText(music.getComment_count());
            itemHolder.creator_name.setText(music.getCreator_name());
            if (music.getCreator_image_file_url().equals(GlobalConstants.DOMAIN)) {
                itemHolder.creator_image.setImageResource(R.drawable.image_none2);
            } else {
                imageFetcher.setImageToImageView(music.getCreator_image_file_url(), itemHolder.creator_image, 24);
            }
            itemHolder.audio_file_url = music.getAudio_file_url();
            itemHolder.post_sn = music.getPost_sn();
            itemHolder.post_snTextView.setText(itemHolder.post_sn);

            // TODO : Music link UI(or interaction 표준화

            itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    ((MainActivity) context).viewMusic(itemHolder.post_sn);
                    ((MainActivity) context).playMediaPlayer(music);
                }
            });

            itemHolder.creator_sn = music.getCreator_sn();
            itemHolder.creator_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewMember(itemHolder.creator_sn);
                }
            });
            itemHolder.creator_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewMember(itemHolder.creator_sn);
                }
            });

            redirectFragment = music.getRedirectFragment();
            paramBundle = music.getParamBundle();

            itemHolder.follow_sn = music.getFollow_sn();
            if (itemHolder.follow_sn != null && !itemHolder.follow_sn.equals("")) {
                itemHolder.imageButtonFollow.setColorFilter(ContextCompat.getColor(context, R.color.DIOCIAN));
            } else {
                itemHolder.imageButtonFollow.setColorFilter(null);
            }

            itemHolder.imageButtonFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int myFollowCount;
                    if (itemHolder.follow_sn != null && !itemHolder.follow_sn.equals("")) {
                        myFollowCount = 1;
                    } else {
                        myFollowCount = 0;
                    }
                    ((MainActivity) context).setFollow(itemHolder.creator_sn, myFollowCount, redirectFragment, paramBundle);
                }
            });

            itemHolder.my_like_count = music.getMy_like_count();
            if (itemHolder.my_like_count != null && itemHolder.my_like_count.equals("1")) {
                itemHolder.imageButtonLike.setColorFilter(ContextCompat.getColor(context, R.color.DIOCIAN));
            } else {
                itemHolder.imageButtonLike.setColorFilter(null);
            }

            itemHolder.imageButtonLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).setLike("post", itemHolder.post_sn, itemHolder.my_like_count, redirectFragment, paramBundle);
                }
            });

        } else if (holder instanceof ViewHolderVideo) {
            final ViewHolderVideo itemHolder = (ViewHolderVideo) holder;
            Post video = posts.get(position - 1);
            itemHolder.genre_name.setText(video.getGenre_name());
            imageFetcher.setImageToImageView(video.getImage_file_url(), itemHolder.image, 168);
            itemHolder.title.setText(video.getTitle());
            itemHolder.created_datetime.setText(video.getCreated_datetime());
            itemHolder.like_count.setText(video.getLike_count());
            itemHolder.comment_count.setText(video.getComment_count());
            itemHolder.creator_name.setText(video.getCreator_name());
            if (video.getCreator_image_file_url().equals(GlobalConstants.DOMAIN)) {
                itemHolder.creator_image.setImageResource(R.drawable.image_none2);
            } else {
                imageFetcher.setImageToImageView(video.getCreator_image_file_url(), itemHolder.creator_image, 24);
            }
            itemHolder.post_sn = video.getPost_sn();
            itemHolder.post_snTextView.setText(itemHolder.post_sn);
            itemHolder.youtube_video_id = video.getYoutube_video_id();

            // TODO : Video link UI(or interaction 표준화

            itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    ((MainActivity) context).viewVideo(itemHolder.post_sn);

                    Intent i = new Intent(Intent.ACTION_VIEW);
                    Uri u = Uri.parse("https://www.youtube.com/watch?v=" + itemHolder.youtube_video_id);
                    i.setData(u);
                    context.startActivity(i);
                }
            });

            itemHolder.creator_sn = video.getCreator_sn();
            itemHolder.creator_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewMember(itemHolder.creator_sn);
                }
            });
            itemHolder.creator_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewMember(itemHolder.creator_sn);
                }
            });

            redirectFragment = video.getRedirectFragment();
            paramBundle = video.getParamBundle();

            itemHolder.follow_sn = video.getFollow_sn();
            if (itemHolder.follow_sn != null && !itemHolder.follow_sn.equals("")) {
                itemHolder.imageButtonFollow.setColorFilter(ContextCompat.getColor(context, R.color.DIOCIAN));
            } else {
                itemHolder.imageButtonFollow.setColorFilter(null);
            }

            itemHolder.imageButtonFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int myFollowCount;
                    if (itemHolder.follow_sn != null && !itemHolder.follow_sn.equals("")) {
                        myFollowCount = 1;
                    } else {
                        myFollowCount = 0;
                    }
                    ((MainActivity) context).setFollow(itemHolder.creator_sn, myFollowCount, redirectFragment, paramBundle);
                }
            });

            itemHolder.my_like_count = video.getMy_like_count();
            if (itemHolder.my_like_count != null && itemHolder.my_like_count.equals("1")) {
                itemHolder.imageButtonLike.setColorFilter(ContextCompat.getColor(context, R.color.DIOCIAN));
            } else {
                itemHolder.imageButtonLike.setColorFilter(null);
            }

            itemHolder.imageButtonLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).setLike("post", itemHolder.post_sn, itemHolder.my_like_count, redirectFragment, paramBundle);
                }
            });

        } else if (holder instanceof ViewHolderMarket) {
            final ViewHolderMarket itemHolder = (ViewHolderMarket) holder;
            Post market = posts.get(position - 1);
            itemHolder.price.setText(market.getPrice());
            imageFetcher.setImageToImageView(market.getImage_file_url(), itemHolder.image, 168);
            itemHolder.title.setText(market.getTitle());
            itemHolder.created_datetime.setText(market.getCreated_datetime());
            itemHolder.like_count.setText(market.getLike_count());
            itemHolder.comment_count.setText(market.getComment_count());
            itemHolder.creator_name.setText(market.getCreator_name());
            if (market.getCreator_image_file_url().equals(GlobalConstants.DOMAIN)) {
                itemHolder.creator_image.setImageResource(R.drawable.image_none2);
            } else {
                imageFetcher.setImageToImageView(market.getCreator_image_file_url(), itemHolder.creator_image, 24);
            }
            itemHolder.post_sn = market.getPost_sn();

            // TODO : Market link UI(or interaction 표준화

            itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewMarket(itemHolder.post_sn);
                }
            });

            itemHolder.creator_sn = market.getCreator_sn();
            itemHolder.creator_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewMember(itemHolder.creator_sn);
                }
            });
            itemHolder.creator_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewMember(itemHolder.creator_sn);
                }
            });

            redirectFragment = market.getRedirectFragment();
            paramBundle = market.getParamBundle();

            itemHolder.follow_sn = market.getFollow_sn();
            if (itemHolder.follow_sn != null && !itemHolder.follow_sn.equals("")) {
                itemHolder.imageButtonFollow.setColorFilter(ContextCompat.getColor(context, R.color.DIOCIAN));
            } else {
                itemHolder.imageButtonFollow.setColorFilter(null);
            }

            itemHolder.imageButtonFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int myFollowCount;
                    if (itemHolder.follow_sn != null && !itemHolder.follow_sn.equals("")) {
                        myFollowCount = 1;
                    } else {
                        myFollowCount = 0;
                    }
                    ((MainActivity) context).setFollow(itemHolder.creator_sn, myFollowCount, redirectFragment, paramBundle);
                }
            });

            itemHolder.my_like_count = market.getMy_like_count();
            if (itemHolder.my_like_count != null && itemHolder.my_like_count.equals("1")) {
                itemHolder.imageButtonLike.setColorFilter(ContextCompat.getColor(context, R.color.DIOCIAN));
            } else {
                itemHolder.imageButtonLike.setColorFilter(null);
            }

            itemHolder.imageButtonLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).setLike("post", itemHolder.post_sn, itemHolder.my_like_count, redirectFragment, paramBundle);
                }
            });

        } else if (holder instanceof HeaderViewHolder) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        } else if (holder instanceof FooterViewHolder) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        }
    }

    @Override
    public int getItemCount() {
        if (isFooter) {
            return posts.size() + 2;
        }
        return posts.size() + 1;
    }

    public void removeFooter() {
        isFooter = false;
    }

}
