package com.diocian.diocianappdev.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.ImageFetcher;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class SourceRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Post> videos;
    private ImageFetcher imageFetcher;
    private Context context;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;

    private boolean isFooter = true;

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View view) {
            super(view);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView genre_name;
        public ImageView image;
        public TextView title;
        public TextView artist_name;
        public String post_sn;
        public String youtube_video_id;
        public ImageButton imageButtonDownload;
        public ImageButton imageButtonShare;
        public String audio_file_url;

        public ViewHolder(View view) {
            super(view);
            genre_name = (TextView) view.findViewById(R.id.genre_name);
            image = (ImageView) view.findViewById(R.id.image);
            title = (TextView) view.findViewById(R.id.title);
            artist_name = (TextView) view.findViewById(R.id.artist_name);
            imageButtonDownload = (ImageButton) view.findViewById(R.id.imageButtonDownload);
            imageButtonShare = (ImageButton) view.findViewById(R.id.imageButtonShare);
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View view) {
            super(view);
        }
    }

    public SourceRecyclerAdapter(ArrayList<Post> posts_param, ImageFetcher imageFetcher, Context context) {
        videos = posts_param;
        this.imageFetcher = imageFetcher;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (isFooter) {
            if (position == 0) {
                return TYPE_HEADER;
            } else if (position == (videos.size() + 1)) {
                return TYPE_FOOTER;
            }
        } else {
            if (position == 0) {
                return TYPE_HEADER;
            }
        }
        return TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_header_mrs, parent, false);
            return new HeaderViewHolder(view);
        } else if (viewType == TYPE_FOOTER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_footer, parent, false);
            return new FooterViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_mrs, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder itemHolder = (ViewHolder) holder;
            final Post video = videos.get(position - 1);
            itemHolder.genre_name.setText(video.getGenre_name());
            imageFetcher.setImageToImageView(video.getImage_file_url(), itemHolder.image, 168);
            itemHolder.title.setText(video.getTitle());
            itemHolder.artist_name.setText(video.getArtist_name());
            itemHolder.post_sn = video.getPost_sn();
            itemHolder.youtube_video_id = video.getYoutube_video_id();
            itemHolder.audio_file_url = video.getAudio_download_file_url();

            itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).playMediaPlayer(video);
//                    ((MainActivity) context).viewVideo(itemHolder.post_sn);
//                    Intent i = new Intent(Intent.ACTION_VIEW);
//                    Uri u = Uri.parse("https://www.youtube.com/watch?v=" + itemHolder.youtube_video_id);
//                    i.setData(u);
//                    context.startActivity(i);
                }
            });

            itemHolder.imageButtonDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    Uri u = null;
                    try {
                        u = Uri.parse(GlobalConstants.DOMAIN + "/getMp3FileByPath2.do?audio_file_url=" + URLEncoder.encode(itemHolder.audio_file_url, "UTF-8") + "&title=" + URLEncoder.encode((String) itemHolder.title.getText(), "UTF-8") + "&artist_name=" + URLEncoder.encode((String) itemHolder.artist_name.getText(), "UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    i.setData(u);
                    context.startActivity(i);
                }
            });

            itemHolder.imageButtonShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    Uri u = Uri.parse(GlobalConstants.DOMAIN + "/share?target=source&post_sn=" + itemHolder.post_sn);
                    i.setData(u);
                    context.startActivity(i);
                }
            });

        } else if (holder instanceof HeaderViewHolder) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        } else if (holder instanceof FooterViewHolder) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        }
    }

    @Override
    public int getItemCount() {
        if (isFooter) {
            return videos.size() + 2;
        }
        return videos.size() + 1;
    }

    public void removeFooter() {
        isFooter = false;
    }

}
