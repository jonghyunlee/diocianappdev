package com.diocian.diocianappdev.widget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.util.LogManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by USER on 2017-02-16.
 */

public class MusicPlayer extends AppWidgetProvider{
    private static String TAG = MusicPlayer.class.getName();
    private static PendingIntent mSender;
    private static AlarmManager mManager;

    @Override
    public void onReceive(Context context, Intent intent) {
        LogManager.d(TAG, "onReceive" + "...getAction : " + intent.getAction());
        if(intent.getAction().equals("android.appwidget.action.APPWIDGET_UPDATE"))
        {//
            LogManager.w(TAG, "android.appwidget.action.APPWIDGET_UPDATE");
            removePreviousAlarm();

            long firstTime = System.currentTimeMillis() + 1000;
            mSender = PendingIntent.getBroadcast(context, 0, intent, 0);
            mManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            mManager.set(AlarmManager.RTC, firstTime, mSender);
        }
        super.onReceive(context, intent);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        LogManager.d(TAG, "onUpdate");
        for (int appWdgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWdgetId);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onEnabled(Context context) {
        LogManager.d(TAG,"onEnabled");
        super.onEnabled(context);
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        LogManager.d(TAG,"onDeleted");
        super.onDeleted(context, appWidgetIds);
    }

    private static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId){
        /**
         * getting current time
         */
        Calendar mCalendar = Calendar.getInstance();
        SimpleDateFormat mFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm", Locale.KOREA);

        RemoteViews updateViews = new RemoteViews(context.getPackageName(), R.layout.music_player);
        updateViews.setTextViewText(R.id.testTextView, mFormat.format(mCalendar.getTime()));

        appWidgetManager.updateAppWidget(appWidgetId, updateViews);

        LogManager.d(TAG, "updateAppWidget appWidgetId : " + appWidgetId + "...updateViews : " + updateViews + "...time : " + mFormat.format(mCalendar.getTime()));
    }

    public void removePreviousAlarm()
    {
        if(mManager != null && mSender != null)
        {
            mSender.cancel();
            mManager.cancel(mSender);
        }
    }
}
