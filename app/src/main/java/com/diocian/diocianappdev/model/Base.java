package com.diocian.diocianappdev.model;

import java.io.Serializable;

public class Base implements Serializable {
    private static final long serialVersionUID = 3906465660546117746L;
    private String created_datetime;
    private String creator_sn;

    public String getCreated_datetime() {
        return created_datetime;
    }

    public void setCreated_datetime(String created_datetime) {
        this.created_datetime = created_datetime;
    }

    public String getCreator_sn() {
        return creator_sn;
    }

    public void setCreator_sn(String creator_sn) {
        this.creator_sn = creator_sn;
    }
}
