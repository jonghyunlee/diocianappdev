package com.diocian.diocianappdev.model;

public class Notification extends Base {

    private String notification_sn;
    private String target_member_sn;
    private String target_dc;
    private String target_sn;
    private String post_dc;
    private String action_cd;
    private String action_member_sn;
    private String description;
    private String read_yn;
    private String fcm_token;

    public Notification setNotification(String notification_sn, String target_member_sn, String target_dc, String target_sn, String post_dc, String action_cd, String action_member_sn, String description, String read_yn, String created_datetime) {
        this.notification_sn = notification_sn;
        this.target_member_sn = target_member_sn;
        this.target_dc = target_dc;
        this.target_sn = target_sn;
        this.post_dc = post_dc;
        this.action_cd = action_cd;
        this.action_member_sn = action_member_sn;
        this.description = description;
        this.read_yn = read_yn;
        this.setCreated_datetime(created_datetime);
        return this;
    }

    public String getPost_dc() {
        return post_dc;
    }

    public void setPost_dc(String post_dc) {
        this.post_dc = post_dc;
    }

    public String getFcm_token() {
        return fcm_token;
    }

    public void setFcm_token(String fcm_token) {
        this.fcm_token = fcm_token;
    }

    public String getNotification_sn() {
        return notification_sn;
    }

    public void setNotification_sn(String notification_sn) {
        this.notification_sn = notification_sn;
    }

    public String getTarget_member_sn() {
        return target_member_sn;
    }

    public void setTarget_member_sn(String target_member_sn) {
        this.target_member_sn = target_member_sn;
    }

    public String getTarget_dc() {
        return target_dc;
    }

    public void setTarget_dc(String target_dc) {
        this.target_dc = target_dc;
    }

    public String getTarget_sn() {
        return target_sn;
    }

    public void setTarget_sn(String target_sn) {
        this.target_sn = target_sn;
    }

    public String getAction_cd() {
        return action_cd;
    }

    public void setAction_cd(String action_cd) {
        this.action_cd = action_cd;
    }

    public String getAction_member_sn() {
        return action_member_sn;
    }

    public void setAction_member_sn(String action_member_sn) {
        this.action_member_sn = action_member_sn;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRead_yn() {
        return read_yn;
    }

    public void setRead_yn(String read_yn) {
        this.read_yn = read_yn;
    }
}
