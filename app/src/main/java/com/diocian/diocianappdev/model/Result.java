package com.diocian.diocianappdev.model;

public class Result extends Base {

    private String success_yn = "N";
    private String message;

    public String getSuccess_yn() {
        return success_yn;
    }

    public void setSuccess_yn(String success_yn) {
        this.success_yn = success_yn;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
