package com.diocian.diocianappdev.model;

public class Member extends Base {

    private String member_sn;
    private String member_dc;
    private String member_id;
    private String member_pw;
    private String member_pw_new;
    private String pw_reset_cd;
    private String email_auth_cd;
    private String email_auth_yn;
    private String email_address;
    private String member_name;
    private String member_image_url;
    private String member_summary;
    private String home_path;
    private String home_image_url;
    private String text_color_hex;
    private String member_image_delete_yn;
    private String home_image_delete_yn;
    private String session_member_sn;
    private String login_yn;
    private String parent_sn = "0";
    private String strCurrentUrl;
    private String follow_sn;
    private String search_word;
    private String admin_yn;
    private String storage_plan_sn;
    private String storage_size_byte;
    private String myContentOnlyYn = "";
    private String creator_sn;

    public Member setMember(String member_sn, String member_name, String member_image_url, String member_summary, String creator_sn, String follow_sn) {
        this.member_sn = member_sn;
        this.member_name = member_name;
        this.member_image_url = member_image_url;
        this.member_summary = member_summary;
        this.creator_sn = creator_sn;
        this.follow_sn = follow_sn;
        return this;
    }

    @Override
    public String getCreator_sn() {
        return creator_sn;
    }

    @Override
    public void setCreator_sn(String creator_sn) {
        this.creator_sn = creator_sn;
    }

    public String getMember_sn() {
        return member_sn;
    }

    public void setMember_sn(String member_sn) {
        this.member_sn = member_sn;
    }

    public String getMember_dc() {
        return member_dc;
    }

    public void setMember_dc(String member_dc) {
        this.member_dc = member_dc;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getMember_pw() {
        return member_pw;
    }

    public void setMember_pw(String member_pw) {
        this.member_pw = member_pw;
    }

    public String getMember_pw_new() {
        return member_pw_new;
    }

    public void setMember_pw_new(String member_pw_new) {
        this.member_pw_new = member_pw_new;
    }

    public String getPw_reset_cd() {
        return pw_reset_cd;
    }

    public void setPw_reset_cd(String pw_reset_cd) {
        this.pw_reset_cd = pw_reset_cd;
    }

    public String getEmail_auth_cd() {
        return email_auth_cd;
    }

    public void setEmail_auth_cd(String email_auth_cd) {
        this.email_auth_cd = email_auth_cd;
    }

    public String getEmail_auth_yn() {
        return email_auth_yn;
    }

    public void setEmail_auth_yn(String email_auth_yn) {
        this.email_auth_yn = email_auth_yn;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public String getMember_name() {
        return member_name;
    }

    public void setMember_name(String member_name) {
        this.member_name = member_name;
    }

    public String getMember_image_url() {
        return member_image_url;
    }

    public void setMember_image_url(String member_image_url) {
        this.member_image_url = member_image_url;
    }

    public String getMember_summary() {
        return member_summary;
    }

    public void setMember_summary(String member_summary) {
        this.member_summary = member_summary;
    }

    public String getHome_path() {
        return home_path;
    }

    public void setHome_path(String home_path) {
        this.home_path = home_path;
    }

    public String getHome_image_url() {
        return home_image_url;
    }

    public void setHome_image_url(String home_image_url) {
        this.home_image_url = home_image_url;
    }

    public String getText_color_hex() {
        return text_color_hex;
    }

    public void setText_color_hex(String text_color_hex) {
        this.text_color_hex = text_color_hex;
    }

    public String getMember_image_delete_yn() {
        return member_image_delete_yn;
    }

    public void setMember_image_delete_yn(String member_image_delete_yn) {
        this.member_image_delete_yn = member_image_delete_yn;
    }

    public String getHome_image_delete_yn() {
        return home_image_delete_yn;
    }

    public void setHome_image_delete_yn(String home_image_delete_yn) {
        this.home_image_delete_yn = home_image_delete_yn;
    }

    public String getSession_member_sn() {
        return session_member_sn;
    }

    public void setSession_member_sn(String session_member_sn) {
        this.session_member_sn = session_member_sn;
    }

    public String getLogin_yn() {
        return login_yn;
    }

    public void setLogin_yn(String login_yn) {
        this.login_yn = login_yn;
    }

    public String getParent_sn() {
        return parent_sn;
    }

    public void setParent_sn(String parent_sn) {
        this.parent_sn = parent_sn;
    }

    public String getStrCurrentUrl() {
        return strCurrentUrl;
    }

    public void setStrCurrentUrl(String strCurrentUrl) {
        this.strCurrentUrl = strCurrentUrl;
    }

    public String getFollow_sn() {
        return follow_sn;
    }

    public void setFollow_sn(String follow_sn) {
        this.follow_sn = follow_sn;
    }

    public String getSearch_word() {
        return search_word;
    }

    public void setSearch_word(String search_word) {
        this.search_word = search_word;
    }

    public String getAdmin_yn() {
        return admin_yn;
    }

    public void setAdmin_yn(String admin_yn) {
        this.admin_yn = admin_yn;
    }

    public String getStorage_plan_sn() {
        return storage_plan_sn;
    }

    public void setStorage_plan_sn(String storage_plan_sn) {
        this.storage_plan_sn = storage_plan_sn;
    }

    public String getStorage_size_byte() {
        return storage_size_byte;
    }

    public void setStorage_size_byte(String storage_size_byte) {
        this.storage_size_byte = storage_size_byte;
    }

    public String getMyContentOnlyYn() {
        return myContentOnlyYn;
    }

    public void setMyContentOnlyYn(String myContentOnlyYn) {
        this.myContentOnlyYn = myContentOnlyYn;
    }
}
