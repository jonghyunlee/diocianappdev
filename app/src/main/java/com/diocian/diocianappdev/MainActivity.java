package com.diocian.diocianappdev;

import android.app.Activity;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.provider.SearchRecentSuggestions;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.diocian.diocianappdev.fragment.ArticleFragment;
import com.diocian.diocianappdev.fragment.Bmv1R2Fragment;
import com.diocian.diocianappdev.fragment.Bmv2Fragment;
import com.diocian.diocianappdev.fragment.EntryFragment;
import com.diocian.diocianappdev.fragment.FacebookLoginFragment;
import com.diocian.diocianappdev.fragment.HomeFragment;
import com.diocian.diocianappdev.fragment.ListFragment;
import com.diocian.diocianappdev.fragment.LoginFragment;
import com.diocian.diocianappdev.fragment.MarketFragment;
import com.diocian.diocianappdev.fragment.MemberFragment;
import com.diocian.diocianappdev.fragment.MusicFragment;
import com.diocian.diocianappdev.fragment.NotificationsFragment;
import com.diocian.diocianappdev.fragment.PlaylistsFragment;
import com.diocian.diocianappdev.fragment.PurchaseHistoryFragment;
import com.diocian.diocianappdev.fragment.SettingFragment;
import com.diocian.diocianappdev.fragment.SignupFragment;
import com.diocian.diocianappdev.fragment.StudioFragment;
import com.diocian.diocianappdev.fragment.StudynoteFragment;
import com.diocian.diocianappdev.fragment.VideoFragment;
import com.diocian.diocianappdev.fragment.Bmv1Fragment;
import com.diocian.diocianappdev.model.Playlist;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.provider.RecentSuggestionProvider;
import com.diocian.diocianappdev.service.NotificationPlayerConstants;
import com.diocian.diocianappdev.service.NotificationPlayerService;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, NotificationPlayerService.Callbacks {

    NavigationView navigationView;

    private ImageFetcher imageFetcher;
    private MediaPlayer mediaPlayer;
    private TextView titleTextView;
    private TextView artist_nameTextView;
    private ImageView mediaImageView;
    private ImageButton btn_playImageButton;
    private ImageButton btn_next;
    private ImageButton btn_prev;

    private CoordinatorLayout fragment_container;
    private CoordinatorLayout playlistContainer;

    //    private LinearLayout linearLayoutMediaPlayer;
    private ImageButton imageButtonShowPlaylist;
    public boolean isPlaylist = false;

    MainActivity activity = this;

    private FirebaseAnalytics mFirebaseAnalytics;


//    private View view;

    public void playMediaPlayer(final Post post) {

        final ArrayList<View> recyclerViewPlayLists = Utils.getViewsByTag((ViewGroup) findViewById(R.id.recyclerViewPlayLists), "post_sn");
        boolean isAddedPlaylist = false;
        for (int i = 0; i < recyclerViewPlayLists.size(); i++) {
            if (((TextView) recyclerViewPlayLists.get(i)).getText().equals(post.getPost_sn())) {
                isAddedPlaylist = true;
            }
        }
        if (isAddedPlaylist) {
            setPlaylistActive(post.getPost_sn());
        } else {

            isLogined(false, new Callback_isLoggedIn() {
                @Override
                public void callback(Context context, boolean isLoggedIn) {
                    if (isLoggedIn) {
                        String prev_playlist_sn;
                        if (recyclerViewPlayLists == null | recyclerViewPlayLists.size() == 0) {
                            prev_playlist_sn = "0";
                        } else {
                            TextView textViewPlaylistSn = (TextView) ((LinearLayout) recyclerViewPlayLists.get(0).getParent()).findViewById(R.id.textViewPlaylistSn);
                            prev_playlist_sn = textViewPlaylistSn.getText().toString();
                        }
                        setPlaylist("post", post.getPost_sn(), prev_playlist_sn);
                    } else {
                        String artist_name = post.getArtist_name();
                        if (artist_name == null) {
                            artist_name = post.getCreator_name();
                        }
                        setBottomPlayer(true, post.getTitle(), artist_name, post.getImage_file_url(), post.getAudio_file_url(), true);
                    }
                }
            });
        }
    }

    public void setBottomPlayer(final boolean isAutoPlay, final String title, final String artist_name, final String cover_image_url, final String audio_file_url, boolean isRestart) {
        ImageView imageViewCoverImage = (ImageView) playlistContainer.findViewById(R.id.imageViewCoverImage);
        TextView textViewTitle = (TextView) playlistContainer.findViewById(R.id.textViewTitle);
        TextView textViewArtistName = (TextView) playlistContainer.findViewById(R.id.textViewArtistName);
//        try {
        titleTextView.setText(title);
        if (textViewTitle != null) textViewTitle.setText(title);
        artist_nameTextView.setText(artist_name);
        if (textViewArtistName != null) textViewArtistName.setText(artist_name);
        if (cover_image_url == null || cover_image_url.equals("") || cover_image_url.equals(GlobalConstants.DOMAIN)) {
            mediaImageView.setImageResource(R.drawable.image_none);
        } else {
            imageFetcher.setImageToImageView(cover_image_url, mediaImageView, 40);
            if (imageViewCoverImage != null)
                imageFetcher.setImageToImageView(cover_image_url, imageViewCoverImage, 300);
        }
        startNotificationPlayerService(cover_image_url, title, artist_name, audio_file_url, isAutoPlay, isRestart, getPostSnActivePlaylist());
//            if (isRestart) {
//                if (mediaPlayer != null) {
//                    mediaPlayer.release();
//                    mediaPlayer = null;
//                }
//                mediaPlayer = new MediaPlayer();
//                mediaPlayer.setDataSource(audio_file_url);
//                startNotificationPlayerService(cover_image_url, title, artist_name, audio_file_url, isAutoPlay, isRestart);
//                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                    @Override
//                    public void onPrepared(MediaPlayer mp) {
//                        if (isAutoPlay) {
//                            mp.start();
//                            playNotificationPlayerService(cover_image_url, title, artist_name, audio_file_url);
//                            btn_playImageButton.setImageResource(R.drawable.ic_pause_white_24dp);
//                        }
//                    }
//                });
//                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                    @Override
//                    public void onCompletion(MediaPlayer mp) {
//                        playNextPlaylist();
//                    }
//                });
//
//                mediaPlayer.prepareAsync();
//            }

//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    public void setPlaylistActive(String post_sn) {
        setPlaylistActive(post_sn, true, true);
    }

    public void setPlaylistActive(String post_sn, boolean isRestart, boolean isAutoPlay) {
        ArrayList<View> playlistIsActives = Utils.getViewsByTag((ViewGroup) findViewById(R.id.recyclerViewPlayLists), "isActive");
        for (int i = 0; i < playlistIsActives.size(); i++) {
            if (((TextView) playlistIsActives.get(i)).getText().equals("Y")) {
                ((LinearLayout) playlistIsActives.get(i).getParent()).setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.dummy));
                ((TextView) playlistIsActives.get(i)).setText("N");
            }
        }
        ArrayList<View> playlistPostSns = Utils.getViewsByTag((ViewGroup) findViewById(R.id.recyclerViewPlayLists), "post_sn");
        for (int i = 0; i < playlistPostSns.size(); i++) {
            if (((TextView) playlistPostSns.get(i)).getText().equals(post_sn)) {
                LinearLayout linearLayoutPlaylist = (LinearLayout) playlistPostSns.get(i).getParent();
                linearLayoutPlaylist.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.DIOCIAN));
                ((TextView) linearLayoutPlaylist.findViewById(R.id.textViewIsActive)).setText("Y");
                TextView textViewTitle = (TextView) linearLayoutPlaylist.findViewById(R.id.textViewTitle);
                TextView textViewArtistName = (TextView) linearLayoutPlaylist.findViewById(R.id.textViewArtistName);
                TextView textViewCoverImageUrl = (TextView) linearLayoutPlaylist.findViewById(R.id.textViewCoverImageUrl);
                TextView textViewAudio_file_url = (TextView) linearLayoutPlaylist.findViewById(R.id.textViewAudio_file_url);
//                if (isRestart) {
                setBottomPlayer(isAutoPlay, textViewTitle.getText().toString(), textViewArtistName.getText().toString(), textViewCoverImageUrl.getText().toString(), textViewAudio_file_url.getText().toString(), isRestart);
//                }
            }
        }
    }

    public String getPostSnActivePlaylist() {
        String activePostSn = "";
        ArrayList<View> playlistIsActives = Utils.getViewsByTag((ViewGroup) findViewById(R.id.recyclerViewPlayLists), "isActive");
        if (playlistIsActives.size() != 0) {
            int activedIndex = 0;
            for (int i = 0; i < playlistIsActives.size(); i++) {
                if (((TextView) playlistIsActives.get(i)).getText().equals("Y")) {
                    activedIndex = i;
                }
            }
            ArrayList<View> playlistPostSns = Utils.getViewsByTag((ViewGroup) findViewById(R.id.recyclerViewPlayLists), "post_sn");
            activePostSn = ((TextView) playlistPostSns.get(activedIndex)).getText().toString();
        }
        return activePostSn;
    }

    public void playNextPlaylist() {
        ArrayList<View> playlistIsActives = Utils.getViewsByTag((ViewGroup) findViewById(R.id.recyclerViewPlayLists), "isActive");
        if (playlistIsActives.size() != 0) {
            int activedIndex = 0;
            for (int i = 0; i < playlistIsActives.size(); i++) {
                if (((TextView) playlistIsActives.get(i)).getText().equals("Y")) {
                    if (i != (playlistIsActives.size() - 1)) {
                        activedIndex = i + 1;
                    }
                }
            }
            String activePostSn;
            ArrayList<View> playlistPostSns = Utils.getViewsByTag((ViewGroup) findViewById(R.id.recyclerViewPlayLists), "post_sn");
            activePostSn = ((TextView) playlistPostSns.get(activedIndex)).getText().toString();
            if (!activePostSn.equals("")) {
                setPlaylistActive(activePostSn);
            }
        }
    }

    public void playPrevPlaylist() {
        ArrayList<View> playlistIsActives = Utils.getViewsByTag((ViewGroup) findViewById(R.id.recyclerViewPlayLists), "isActive");
        if (playlistIsActives.size() != 0) {
            int activedIndex = playlistIsActives.size() - 1;
            for (int i = 0; i < playlistIsActives.size(); i++) {
                if (((TextView) playlistIsActives.get(i)).getText().equals("Y")) {
                    if (i != 0) {
                        activedIndex = i - 1;
                    }
                }
            }
            String activePostSn;
            ArrayList<View> playlistPostSns = Utils.getViewsByTag((ViewGroup) findViewById(R.id.recyclerViewPlayLists), "post_sn");
            activePostSn = ((TextView) playlistPostSns.get(activedIndex)).getText().toString();
            if (!activePostSn.equals("")) {
                setPlaylistActive(activePostSn);
            }
        }
    }

    public void playMediaPlayer2(Post post) {

        CharSequence title = post.getTitle();
        CharSequence artist_name = post.getArtist_name();
        if (artist_name == null) {
            artist_name = post.getCreator_name();
        }
        String image_file_url = post.getImage_file_url();

        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
//        final AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
//        final int originalVolume = audioManager.getStreamVolume(AudioManager.STREAM_RING);
//        audioManager.setStreamVolume(AudioManager.STREAM_RING, (int) (audioManager.getStreamMaxVolume(AudioManager.STREAM_RING)*0.2), 0);

        try {
            titleTextView.setText(title);
            artist_nameTextView.setText(artist_name);
            if (image_file_url == null) {
                mediaImageView.setImageResource(R.drawable.image_none);
            } else {
                imageFetcher.setImageToImageView(image_file_url, mediaImageView, 40);
            }

            mediaPlayer.setDataSource(post.getAudio_file_url());

            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                    btn_playImageButton.setImageResource(R.drawable.ic_pause_white_24dp);
                }
            });

            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mediaPlayer.release();
                    mediaPlayer = null;
                    btn_playImageButton.setImageResource(R.drawable.ic_play_arrow_white_24dp);
//                    audioManager.setStreamVolume(AudioManager.STREAM_RING, originalVolume, 0);
                }
            });

            mediaPlayer.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopMediaPlayer2() {
        mediaPlayer.pause();
        btn_playImageButton.setImageResource(R.drawable.ic_play_arrow_white_24dp);
    }

    boolean isCalledPlayerService;
    String activePlayPostSn;
    boolean isPlayWithStart;
    boolean isNotification;

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    public String query;

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            query = intent.getStringExtra(SearchManager.QUERY);
            Log.v(GlobalConstants.LOG_TAG, "query : " + query);
            Bundle bundle = new Bundle();
            bundle.putString("query", query);
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this, RecentSuggestionProvider.AUTHORITY, RecentSuggestionProvider.MODE);
            suggestions.saveRecentQuery(query, null);
            Utils.replaceFragment(this, new HomeFragment(), bundle, false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(serviceConnection);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        String location = getPreferences("location");
        if (location == null || location.equals("")) {
            savePreferences("location", getResources().getString(R.string.location_code));
        }

        Intent intent = getIntent();
        isCalledPlayerService = intent.hasExtra("isCalledPlayerService") ? intent.getBooleanExtra("isCalledPlayerService", false) : false;
        activePlayPostSn = intent.hasExtra("activePlayPostSn") ? intent.getStringExtra("activePlayPostSn") : "0";
        isPlayWithStart = intent.hasExtra("isPlayWithStart") ? intent.getBooleanExtra("isPlayWithStart", false) : false;
        isNotification = intent.hasExtra("isNotification") ? intent.getBooleanExtra("isNotification", false) : false;

        Log.d(GlobalConstants.LOG_TAG, "isCalledPlayerService : " + isCalledPlayerService);
        Log.d(GlobalConstants.LOG_TAG, "activePlayPostSn : " + activePlayPostSn);
        Log.d(GlobalConstants.LOG_TAG, "isPlayWithStart : " + isPlayWithStart);

        if (intent.hasExtra("testextra")) {
            Log.d(GlobalConstants.LOG_TAG, "intent.getStringExtra(\"testextra\") : " + intent.getStringExtra("testextra"));
        }

        fragment_container = (CoordinatorLayout) findViewById(R.id.fragment_container);
        playlistContainer = (CoordinatorLayout) findViewById(R.id.playlistContainer);

        titleTextView = (TextView) findViewById(R.id.title);
        artist_nameTextView = (TextView) findViewById(R.id.artist_name);
        mediaImageView = (ImageView) findViewById(R.id.image);
        btn_playImageButton = (ImageButton) findViewById(R.id.btn_play);
        btn_playImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificationPlayerService.playPausePlayer();
            }
        });

//        mediaPlayerSeekBar = (SeekBar) findViewById(R.id.mediaPlayerSeekBar);

//        linearLayoutMediaPlayer = (LinearLayout) findViewById(R.id.linearLayoutMediaPlayer);
//        linearLayoutMediaPlayer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showPlaylists();
//            }
//        });

        imageButtonShowPlaylist = (ImageButton) findViewById(R.id.imageButtonShowPlaylist);
        imageButtonShowPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPlaylists();
            }
        });

        btn_next = (ImageButton) findViewById(R.id.btn_next);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playNextPlaylist();
            }
        });

        btn_prev = (ImageButton) findViewById(R.id.btn_prev);
        btn_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playPrevPlaylist();
            }
        });

        // TODO : Player SeekBar 추가

        Bundle bundle = new Bundle();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Log.v(GlobalConstants.LOG_TAG, "query : " + query);
            bundle.putString("query", query);
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this, RecentSuggestionProvider.AUTHORITY, RecentSuggestionProvider.MODE);
            suggestions.saveRecentQuery(query, null);

        }
        bundle.putBoolean("isNotification", isNotification);
        Utils.replaceFragment(this, new HomeFragment(), bundle, false);

//        Utils.replaceFragment(this, new HomeFragment(), getIntent().getExtras(), false);

        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(this);
            navigationView.setItemIconTintList(null);
        }

        imageFetcher = new ImageFetcher(this);

        isLogined(false, null);

        final Activity thisActivity = this;
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerOpened(View drawerView) {
                Log.v(GlobalConstants.LOG_TAG, "onDrawerOpened");
                super.onDrawerOpened(drawerView);
                final TextView textViewMessageCount = (TextView) drawerView.findViewById(R.id.textViewMessageCount);
                Log.v(GlobalConstants.LOG_TAG, "textViewMessageCount : " + textViewMessageCount);
                if (textViewMessageCount != null && getPreferences("member_sn") != null) {
                    HttpRequest httpRequest = new HttpRequest(thisActivity);
                    HttpRequest.HttpGetRequestTask notificationCountHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                        @Override
                        public void callback(JSONObject jsonObject, Context context) {
                            try {
                                textViewMessageCount.setText(jsonObject.getString("count"));
                                if (jsonObject.getString("count").equals("0")) {
                                    textViewMessageCount.setVisibility(View.GONE);
                                } else {
                                    textViewMessageCount.setVisibility(View.VISIBLE);
                                }
                                textViewMessageCount.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        viewNotifications();
                                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                                        if (drawer != null) {
                                            drawer.closeDrawer(GravityCompat.START);
                                        }
                                    }
                                });
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    notificationCountHttpGetRequestTask.execute(GlobalConstants.DOMAIN + "/getNotifiactionCountByMember.do?target_member_sn=" + getPreferences("member_sn"));
                }
            }
        };
        if (drawer != null) {
            drawer.addDrawerListener(toggle);
        }
        toggle.syncState();


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                FragmentManager fragmentManager = this.getSupportFragmentManager();
                if(fragmentManager.getBackStackEntryCount() == 0 && query != null) {
                    query = null;
                    Utils.replaceFragment(this, new HomeFragment(), new Bundle(), false);
                } else {
                    super.onBackPressed();
                }
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.item_studynote) {
            viewPosts(2);
        } else if (id == R.id.item_studio) {
            viewPosts(3);
        } else if (id == R.id.item_music) {
            viewPosts(0);
        } else if (id == R.id.item_video) {
            viewPosts(1);
        } else if (id == R.id.item_market) {
            viewPosts(4);
        } else if (id == R.id.itemBeMyVoice) {
            Bundle bundle = new Bundle();
            bundle.putInt("tabNo", 0);
            Utils.replaceFragment(this, new Bmv1Fragment(), bundle);
        } else if (id == R.id.itemBeMyVoice1R2) {
            Utils.replaceFragment(this, new Bmv1R2Fragment(), new Bundle());
        } else if (id == R.id.itemBeMyVoice2R1) {
            Utils.replaceFragment(this, new Bmv2Fragment(), new Bundle());
        } else if (id == R.id.itemUserHome) {
            viewMember(getPreferences("member_sn"));
        } else if (id == R.id.itemLegal) {
            Utils.showLegal(this);
        } else if (id == R.id.itemAboutUs) {
            Utils.showOtherAppByUri(this, "http://diocian.com/about.view");
        } else if (id == R.id.itemPartner) {
            Utils.showOtherAppByUri(this, "http://diocian.com/partner.view");
        } else if (id == R.id.itemPurchaseHistory) {
            Bundle bundle = new Bundle();
            Utils.replaceFragment(this, new PurchaseHistoryFragment(), bundle);
        }
//        } else if (id == R.id.item_message) {
//            Intent intent = new Intent(MainActivity.this, MessagingActivity.class);
//            startActivity(intent);
//            finish();
//            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//        }
//        } else if (id == R.id.itemWebViewTest) {
//            Intent intent = new Intent(MainActivity.this, WebViewTestActivity.class);
//            startActivity(intent);
//            finish();
//            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            drawer.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    public void showLegal(View v) {
        Utils.showLegal(this);
    }

    public void viewLogIn(View view) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            drawer.closeDrawers();
        }
        Utils.replaceFragment(this, new LoginFragment(), new Bundle());
    }

    public void viewSignUpForm(View view) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            drawer.closeDrawers();
        }
        Utils.replaceFragment(this, new SignupFragment(), new Bundle());
    }

    public void setLoginInfo() {

        btn_prev.setVisibility(View.VISIBLE);
        btn_next.setVisibility(View.VISIBLE);
        imageButtonShowPlaylist.setVisibility(View.VISIBLE);

//        String member_sn = getPreferences("member_sn");
        String member_name = getPreferences("member_name");
        String member_summary = getPreferences("member_summary");
        String member_image_url = getPreferences("member_image_url");
        String home_image_url = getPreferences("home_image_url");
//        String session_id = getPreferences("session_id");
//        String session_id_raw = getPreferences("session_id_raw");
//        String client_auth_cd = getPreferences("client_auth_cd");

        if (!member_image_url.equals("")) {
            member_image_url = GlobalConstants.DOMAIN + member_image_url;
        }
        if (!home_image_url.equals("")) {
            home_image_url = GlobalConstants.DOMAIN + home_image_url;
        }

        LinearLayout headerLinearLayout = (LinearLayout) findViewById(R.id.navigation_header_linearLayout);

        if (navigationView != null) {
            if (headerLinearLayout != null) {
                navigationView.removeHeaderView(headerLinearLayout);
                navigationView.inflateHeaderView(R.layout.navigation_header_login);
            }

            TextView textView_name = (TextView) navigationView.findViewById(R.id.textView_name);
            TextView textView_summary = (TextView) navigationView.findViewById(R.id.textView_summary);
            ImageView user_profile_image = (ImageView) navigationView.findViewById(R.id.user_profile_image);
            ImageView home_image_view = (ImageView) navigationView.findViewById(R.id.home_image_view);

            if (textView_name != null) {
                textView_name.setText(member_name);
            }

            if (textView_summary != null) {
                textView_summary.setText(member_summary);
            }

            if (user_profile_image != null && !member_image_url.equals("")) {
                imageFetcher.setImageToImageView(member_image_url, user_profile_image, 64);
            }
            if (home_image_view != null && !home_image_url.equals("")) {
                imageFetcher.setImageToImageView(home_image_url, home_image_view, 300);
            }
        }//

        if (navigationView != null) {
            Menu navigationMenu = navigationView.getMenu();
            navigationMenu.setGroupVisible(R.id.groupUser, true);
            navigationMenu.getItem(0).setTitle(getPreferences("member_name"));
        }

        FloatingActionButton fab_logout = (FloatingActionButton) findViewById(R.id.fab_logout);
        if (fab_logout != null) {
            fab_logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    logout();
                }
            });
        }

        FloatingActionButton fab_settings = (FloatingActionButton) findViewById(R.id.fab_settings);
        if (fab_settings != null) {
            fab_settings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Utils.replaceFragment(activity, new SettingFragment(), new Bundle());
                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    if (drawer != null) {
                        drawer.closeDrawer(GravityCompat.START);
                    }
                }
            });
        }

        FloatingActionButton fab_notification = (FloatingActionButton) findViewById(R.id.fab_notification);
        if (fab_notification != null) {
            fab_notification.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewNotifications();
                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    if (drawer != null) {
                        drawer.closeDrawer(GravityCompat.START);
                    }
                }
            });
        }

        if (!isPlaylist) {
            if (isCalledPlayerService) {
                getPlaylists(isPlayWithStart, false, activePlayPostSn);
            } else {
                getPlaylists();
            }

        }


    }

    public void logout() {
        RelativeLayout headerRelativeLayout = (RelativeLayout) findViewById(R.id.navigation_header_relativeLayout);
        if (navigationView != null && headerRelativeLayout != null) {
            navigationView.removeHeaderView(headerRelativeLayout);
            navigationView.inflateHeaderView(R.layout.navigation_header);
            removeAllPreferences();
            Menu navigationMenu = navigationView.getMenu();
            navigationMenu.setGroupVisible(R.id.groupUser, false);
            getPlaylists();
            btn_prev.setVisibility(View.GONE);
            btn_next.setVisibility(View.GONE);
            imageButtonShowPlaylist.setVisibility(View.GONE);

            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this, RecentSuggestionProvider.AUTHORITY, RecentSuggestionProvider.MODE);
            suggestions.clearHistory();

            HttpRequest httpRequest = new HttpRequest(this);
            HttpRequest.HttpGetRequestTask logoutHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                @Override
                public void callback(JSONObject jsonObject, Context context) {
                    Log.v(GlobalConstants.LOG_TAG, "Logout : " + jsonObject);
                }
            });
            logoutHttpGetRequestTask.execute(GlobalConstants.DOMAIN + "/logout.do?fcm_token=" + Utils.getFcmToken());
        }
    }

    public void viewMusics(View view) {
        viewPosts(0);
    }

    public void viewVideos(View view) {
        viewPosts(1);
    }

    public void viewStudynotes(View view) {
        viewPosts(2);
    }

    public void viewStudios(View view) {
        viewPosts(3);
    }

    public void viewMarkets(View view) {
        viewPosts(4);
    }

    public void viewPosts(int tabNo) {
        Bundle bundle = new Bundle();
        bundle.putInt("tabNo", tabNo);
        Utils.replaceFragment(this, new ListFragment(), bundle);
    }

    public void viewMember(String member_sn) {
        Bundle bundle = new Bundle();
        bundle.putString("member_sn", member_sn);
        Utils.replaceFragment(this, new MemberFragment(), bundle);
    }

    public void viewArticle(String post_sn) {
        Bundle bundle = new Bundle();
        bundle.putString("post_sn", post_sn);
        Utils.replaceFragment(this, new ArticleFragment(), bundle);
    }

    public void showPlaylists() {
        fragment_container.setVisibility(View.GONE);
//        playlistContainer.setVisibility(View.VISIBLE);
        playlistContainer.setFocusableInTouchMode(true);
        playlistContainer.requestFocus();
        playlistContainer.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    hidePlaylists();
                    return true;
                }
                return false;
            }
        });
    }

    public void hidePlaylists() {
//        playlistContainer.setVisibility(View.GONE);
        fragment_container.setVisibility(View.VISIBLE);
        playlistContainer.setOnKeyListener(null);
    }

    public void getPlaylists() {
        getPlaylists(false);
    }

    public void getPlaylists(boolean autoStart) {
        getPlaylists(autoStart, true, "");
    }

    public void getPlaylists(boolean autoStart, boolean isReset, String post_sn) {
        PlaylistsFragment playlistsFragment = new PlaylistsFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("autoStart", autoStart);
        bundle.putBoolean("isReset", isReset);
        bundle.putString("post_sn", post_sn);
        playlistsFragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.playlistContainer, playlistsFragment, "playlistContainer");
        transaction.commit();
        isPlaylist = true;
    }

    public void removeArticle(String post_sn) {

        String urlString = "/deletePost.do?post_sn=" + post_sn;
        String url = GlobalConstants.DOMAIN + urlString;
        HttpRequest httpRequest = new HttpRequest(this);
        HttpRequest.HttpGetRequestTask studiosHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, final Context context) {
                try {
                    JSONObject result = jsonObject.getJSONObject("result");
                    String success_yn = result.getString("success_yn");
                    if (success_yn.equals("Y")) {
                        Bundle bundle = new Bundle();
                        bundle.putString("member_sn", getPreferences("member_sn"));
                        Utils.removeAndReplaceFragment(MainActivity.this, new MemberFragment(), new MemberFragment(), bundle);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        studiosHttpGetRequestTask.execute(url);
    }

    public void viewPostDetail(Fragment fragment, String post_sn) {
        Bundle bundle = new Bundle();
        bundle.putString("post_sn", post_sn);
        Utils.replaceFragment(this, fragment, bundle);
    }

    public void viewStudynote(String post_sn) {
        viewPostDetail(new StudynoteFragment(), post_sn);
    }

    public void viewStudio(String post_sn) {
        viewPostDetail(new StudioFragment(), post_sn);
    }

    public void viewMusic(String post_sn) {
        viewPostDetail(new MusicFragment(), post_sn);
    }

    public void viewVideo(String post_sn) {
        viewPostDetail(new VideoFragment(), post_sn);
    }

    public void viewMarket(String post_sn) {
        viewPostDetail(new MarketFragment(), post_sn);
    }

    public void viewEntry(String post_sn) {
        viewPostDetail(new EntryFragment(), post_sn);
    }

    public void viewFacebookLogin(String jobCode) {
        Bundle bundle = new Bundle();
        bundle.putString("jobCode", jobCode);
        Utils.replaceFragment(this, new FacebookLoginFragment(), bundle);
    }

    public void showArticleMenu(View view) {
        TextView post_snTextView = (TextView) ((LinearLayout) view.getParent()).findViewById(R.id.post_snTextView);
        final String post_sn = (String) post_snTextView.getText();
        TextView textViewCreatorSn = (TextView) ((LinearLayout) view.getParent()).findViewById(R.id.textViewCreatorSn);
        final String creator_sn = (String) textViewCreatorSn.getText();

        PopupMenu popupMenu = new PopupMenu(this, view);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.menu_article_card, popupMenu.getMenu());
        Menu articleMenu = popupMenu.getMenu();
        if (creator_sn.equals(getPreferences("member_sn"))) {
            articleMenu.getItem(1).setVisible(true);
            articleMenu.getItem(2).setVisible(true);
        } else {
            articleMenu.getItem(1).setVisible(false);
            articleMenu.getItem(2).setVisible(false);
        }

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.action_detail) {
                    viewArticle(post_sn);
                } else if (id == R.id.item_remove) {
                    removeArticle(post_sn);
                }
                return true;
            }
        });
        popupMenu.show();
    }

    public void showStudynoteMenu(View view) {
        TextView post_snTextView = (TextView) ((LinearLayout) view.getParent()).findViewById(R.id.post_sn);
        final String post_sn = (String) post_snTextView.getText();
        PopupMenu popupMenu = new PopupMenu(this, view);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.post_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.action_detail) {
                    viewStudynote(post_sn);
                }
                return true;
            }
        });
        popupMenu.show();
    }

    public void showMusicMenu(View view) {
        TextView post_snTextView = (TextView) ((LinearLayout) view.getParent()).findViewById(R.id.post_sn);
        final String post_sn = (String) post_snTextView.getText();
        PopupMenu popupMenu = new PopupMenu(this, view);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.post_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.action_detail) {
                    viewMusic(post_sn);
                }
                return true;
            }
        });
        popupMenu.show();
    }

    public void showVideoMenu(View view) {
        TextView post_snTextView = (TextView) ((LinearLayout) view.getParent()).findViewById(R.id.post_snTextView);
        final String post_sn = (String) post_snTextView.getText();
        PopupMenu popupMenu = new PopupMenu(this, view);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.post_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.action_detail) {
                    viewVideo(post_sn);
                }
                return true;
            }
        });
        popupMenu.show();
    }

    public void showEntryMenu(View view) {
        TextView post_snTextView = (TextView) ((LinearLayout) view.getParent()).findViewById(R.id.post_snTextView);
        final String post_sn = (String) post_snTextView.getText();
        PopupMenu popupMenu = new PopupMenu(this, view);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.entry_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.action_detail) {
                    viewEntry(post_sn);
                }
                if (id == R.id.action_share) {
//                    viewEntry(post_sn);
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    Uri u = Uri.parse(GlobalConstants.DOMAIN + "/share?target=entry&post_sn=" + post_sn);
                    i.setData(u);
                    startActivity(i);
                }
                return true;
            }
        });
        popupMenu.show();
    }

    public void savePreferences(String key, String value) {
        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getPreferences(String key) {
        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
        return pref.getString(key, "");
    }

    public void removeAllPreferences() {
        String location = getPreferences("location");
        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.apply();
        savePreferences("location", location);
    }

    public void isLogined(final boolean messageTf, final Callback_isLoggedIn callback_isLoggedIn) {
        String member_sn = getPreferences("member_sn");

        if (member_sn != null && !member_sn.equals("")) {

            Map<String, String> parameterMap = new HashMap<>();
            try {
                parameterMap.put("session_id", URLEncoder.encode(getPreferences("session_id"), "UTF-8"));
                parameterMap.put("client_auth_cd", URLEncoder.encode(getPreferences("client_auth_cd"), "UTF-8"));

                Log.v(GlobalConstants.LOG_TAG, "getPreferences(\"fcm_token\") : " + getPreferences("fcm_token"));
                Log.v(GlobalConstants.LOG_TAG, "Utils.getFcmToken() : " + Utils.getFcmToken());

                if (getPreferences("fcm_token") == null || !getPreferences("fcm_token").equals(Utils.getFcmToken())) {
                    parameterMap.put("fcm_token", URLEncoder.encode(Utils.getFcmToken(), "UTF-8"));
                    parameterMap.put("member_sn", URLEncoder.encode(member_sn, "UTF-8"));
                    savePreferences("fcm_token", Utils.getFcmToken());
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            String url = GlobalConstants.DOMAIN + "/setLoginAuth.do?";
            for (String key : parameterMap.keySet()) {
                url += key + "=" + parameterMap.get(key) + "&";
            }
            HttpRequest httpRequest = new HttpRequest(this);
            HttpRequest.HttpGetRequestTask musicHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                @Override
                public void callback(JSONObject jsonObject, Context context) {
                    try {
                        boolean isLoggedIn = false;

                        JSONObject result = jsonObject.getJSONObject("result");

                        if (result.getString("success_yn").equals("Y")) {
                            isLoggedIn = true;
                            JSONObject member = jsonObject.getJSONObject("member");

                            if (member.getString("member_sn").equals("")) {
                                setLoginInfo();
                            } else {
                                removeAllPreferences();
                                savePreferences("member_sn", member.getString("member_sn"));
                                savePreferences("member_name", member.getString("member_name"));
                                savePreferences("member_summary", member.getString("member_summary"));
                                savePreferences("member_image_url", member.getString("member_image_url"));
                                savePreferences("home_image_url", member.getString("home_image_url"));
                                savePreferences("fcm_token", member.getString("fcm_token"));
                                savePreferences("session_id", member.getJSONObject("loginAuth").getString("session_id"));
                                savePreferences("session_id_raw", jsonObject.getString("session_id"));
                                savePreferences("client_auth_cd", member.getJSONObject("loginAuth").getString("client_auth_cd"));
                                setLoginInfo();
                            }
                        } else {
                            logout();
                            if (messageTf) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setMessage("Not logged in. Would you like to log in?");
                                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        viewLogIn(new View(MainActivity.this));
                                    }
                                });
                                builder.setNegativeButton("No", null);
                                builder.show();
                            }
                        }
                        if (callback_isLoggedIn != null)
                            callback_isLoggedIn.callback(context, isLoggedIn);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            musicHttpGetRequestTask.execute(url);
        } else {
            if (messageTf) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Not logged in. Would you like to log in?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        viewLogIn(new View(MainActivity.this));
                    }
                });
                builder.setNegativeButton("No", null);
                builder.show();
            }
            if (callback_isLoggedIn != null) callback_isLoggedIn.callback(this, false);
        }

    }

    public interface Callback_isLoggedIn extends Serializable {
        void callback(Context context, boolean isLoggedIn);
    }

    public void setFollow(final String member_sn, final int myFollowCount, final Fragment currentFragment, final Bundle fragmentBundle) {

        isLogined(true, new MainActivity.Callback_isLoggedIn() {
            @Override
            public void callback(Context context, boolean isLoggedIn) {

                if (!member_sn.equals(getPreferences("member_sn"))) {
                    if (isLoggedIn) {

                        String url = GlobalConstants.DOMAIN + "/setFollow.do?member_sn=" + member_sn;
                        if (myFollowCount > 0) {
                            url = GlobalConstants.DOMAIN + "/deleteFollow.do?member_sn=" + member_sn;
                        }

                        HttpRequest httpRequest = new HttpRequest(context);
                        HttpRequest.HttpGetRequestTask musicHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                            @Override
                            public void callback(JSONObject jsonObject, Context context) {
//                                Utils.removeAndReplaceFragment(MainActivity.this, currentFragment, currentFragment, fragmentBundle);
                                Utils.replaceFragment(MainActivity.this, currentFragment, fragmentBundle);
                            }
                        });
                        musicHttpGetRequestTask.execute(url);

                    }
                }
            }
        });
    }

    public void setFollow2(final String member_sn, final int myFollowCount, final int position, final ArrayList<Post> posts, final RecyclerView.Adapter adapter) {

        isLogined(true, new MainActivity.Callback_isLoggedIn() {
            @Override
            public void callback(Context context, boolean isLoggedIn) {

                if (!member_sn.equals(getPreferences("member_sn"))) {
                    if (isLoggedIn) {

                        String url = GlobalConstants.DOMAIN + "/setFollow.do?member_sn=" + member_sn;
                        if (myFollowCount > 0) {
                            url = GlobalConstants.DOMAIN + "/deleteFollow.do?member_sn=" + member_sn;
                            posts.get(position).setFollow_sn("");
                        } else {
                            posts.get(position).setFollow_sn("1");
                        }

                        HttpRequest httpRequest = new HttpRequest(context);
                        HttpRequest.HttpGetRequestTask musicHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                            @Override
                            public void callback(JSONObject jsonObject, Context context) {
                                adapter.notifyItemChanged(position + 1);
                            }
                        });
                        musicHttpGetRequestTask.execute(url);

                    }
                }
            }
        });
    }

    public void setLike(final String target_dc, final String post_sn, final String my_like_count, final Fragment currentFragment, final Bundle fragmentBundle) {

        isLogined(true, new MainActivity.Callback_isLoggedIn() {
            @Override
            public void callback(Context context, boolean isLoggedIn) {
                if (isLoggedIn) {
                    Map<String, String> parameterMap = new HashMap<>();
                    parameterMap.put("target_dc", target_dc);
                    parameterMap.put("target_sn", post_sn);
                    String urlString = "/setLike.do";
                    if (my_like_count.equals("1")) {
                        urlString = "/deleteLike.do";
                    }
                    String url = GlobalConstants.DOMAIN + urlString + "?";
                    for (String key : parameterMap.keySet()) {
                        url += key + "=" + parameterMap.get(key) + "&";
                    }
                    HttpRequest httpRequest = new HttpRequest(context);
                    HttpRequest.HttpGetRequestTask studiosHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                        @Override
                        public void callback(JSONObject jsonObject, final Context context) {
                            try {
                                JSONObject result = jsonObject.getJSONObject("result");
                                String success_yn = result.getString("success_yn");
                                if (success_yn.equals("Y")) {
//                                    Utils.removeAndReplaceFragment((MainActivity) context, currentFragment, currentFragment, fragmentBundle);
                                    Utils.refreshFragment((MainActivity) context);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    studiosHttpGetRequestTask.execute(url);
                }
            }
        });
    }

    public void setLike2(final String target_dc, final String post_sn, final String my_like_count, final int position, final ArrayList<Post> posts, final RecyclerView.Adapter adapter) {
        isLogined(true, new MainActivity.Callback_isLoggedIn() {
            @Override
            public void callback(Context context, boolean isLoggedIn) {
                if (isLoggedIn) {
                    Map<String, String> parameterMap = new HashMap<>();
                    parameterMap.put("target_dc", target_dc);
                    parameterMap.put("target_sn", post_sn);
                    String urlString = "/setLike.do";
                    int likeCount = Integer.parseInt(posts.get(position).getLike_count());
                    if (Integer.parseInt(my_like_count) > 0) {
                        urlString = "/deleteLike.do";
                        posts.get(position).setMy_like_count("0");
                        posts.get(position).setLike_count(String.valueOf(likeCount - 1));
                    } else {
                        posts.get(position).setMy_like_count("1");
                        posts.get(position).setLike_count(String.valueOf(likeCount + 1));
                    }
                    String url = GlobalConstants.DOMAIN + urlString + "?";
                    for (String key : parameterMap.keySet()) {
                        url += key + "=" + parameterMap.get(key) + "&";
                    }
                    HttpRequest httpRequest = new HttpRequest(context);
                    HttpRequest.HttpGetRequestTask studiosHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                        @Override
                        public void callback(JSONObject jsonObject, final Context context) {
                            try {
                                JSONObject result = jsonObject.getJSONObject("result");
                                String success_yn = result.getString("success_yn");
                                if (success_yn.equals("Y")) {
//                                    Utils.removeAndReplaceFragment((MainActivity) context, currentFragment, currentFragment, fragmentBundle);
//                                    Utils.refreshFragment((MainActivity) context);
                                    adapter.notifyItemChanged(position + 1);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    studiosHttpGetRequestTask.execute(url);
                }
            }
        });
    }

    public void setLike3(final String target_dc, final String post_sn, final String my_like_count, final int position, final ArrayList<Post> posts, final RecyclerView.Adapter adapter) {
        isLogined(true, new MainActivity.Callback_isLoggedIn() {
            @Override
            public void callback(Context context, boolean isLoggedIn) {
                if (isLoggedIn) {
                    Map<String, String> parameterMap = new HashMap<>();
                    parameterMap.put("target_dc", target_dc);
                    parameterMap.put("target_sn", post_sn);
                    String urlString = "/setLike.do";
                    int likeCount = Integer.parseInt(posts.get(position).getLike_count());
                    if (Integer.parseInt(my_like_count) > 0) {
                        urlString = "/deleteLike.do";
                        posts.get(position).setMy_like_count("0");
                        posts.get(position).setLike_count(String.valueOf(likeCount - 1));
                    } else {
                        posts.get(position).setMy_like_count("1");
                        posts.get(position).setLike_count(String.valueOf(likeCount + 1));
                    }
                    String url = GlobalConstants.DOMAIN + urlString + "?";
                    for (String key : parameterMap.keySet()) {
                        url += key + "=" + parameterMap.get(key) + "&";
                    }
                    HttpRequest httpRequest = new HttpRequest(context);
                    HttpRequest.HttpGetRequestTask studiosHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                        @Override
                        public void callback(JSONObject jsonObject, final Context context) {
                            try {
                                JSONObject result = jsonObject.getJSONObject("result");
                                String success_yn = result.getString("success_yn");
                                if (success_yn.equals("Y")) {
//                                    Utils.removeAndReplaceFragment((MainActivity) context, currentFragment, currentFragment, fragmentBundle);
//                                    Utils.refreshFragment((MainActivity) context);
                                    adapter.notifyItemChanged(position);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    studiosHttpGetRequestTask.execute(url);
                }
            }
        });
    }

    public void setPlaylist(String target_dc, String target_sn, String prev_playlist_sn) {
        Map<String, String> parameterMap = new HashMap<>();
        parameterMap.put("target_dc", target_dc);
        parameterMap.put("target_sn", target_sn);
        parameterMap.put("prev_playlist_sn", prev_playlist_sn);
        String urlString = "/setPlaylist.do";
        String url = GlobalConstants.DOMAIN + urlString + "?";
        for (String key : parameterMap.keySet()) {
            url += key + "=" + parameterMap.get(key) + "&";
        }
        HttpRequest httpRequest = new HttpRequest(this);
        HttpRequest.HttpGetRequestTask playlistHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, final Context context) {
                getPlaylists(true);
            }
        });
        playlistHttpGetRequestTask.execute(url);
    }

    public void deletePlaylist(String playlist_sn) {

        Map<String, String> parameterMap = new HashMap<>();
        parameterMap.put("playlist_sn", playlist_sn);
        String urlString = "/deletePlaylist.do";
        String url = GlobalConstants.DOMAIN + urlString + "?";
        for (String key : parameterMap.keySet()) {
            url += key + "=" + parameterMap.get(key) + "&";
        }
        HttpRequest httpRequest = new HttpRequest(this);
        HttpRequest.HttpGetRequestTask playlistHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, final Context context) {
                boolean isPlay = false;
                if (mediaPlayer != null) {
                    if (mediaPlayer.isPlaying()) {
                        isPlay = true;
                    }
                }
                getPlaylists(isPlay, false, getPostSnActivePlaylist());
            }
        });
        playlistHttpGetRequestTask.execute(url);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent notificationPlayerService = new Intent(MainActivity.this, NotificationPlayerService.class);
        bindService(notificationPlayerService, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    public void startNotificationPlayerService(String cover_image_url, String title, String artist_name, String audio_file_url, boolean isAutoPlay, boolean isRestart, String postSn) {
        Intent notificationPlayerService = new Intent(MainActivity.this, NotificationPlayerService.class);
        notificationPlayerService.putExtra("cover_image_url", cover_image_url);
        notificationPlayerService.putExtra("title", title);
        notificationPlayerService.putExtra("artist_name", artist_name);
        notificationPlayerService.putExtra("audio_file_url", audio_file_url);
        notificationPlayerService.putExtra("isAutoPlay", isAutoPlay);
        notificationPlayerService.putExtra("isRestart", isRestart);
        notificationPlayerService.putExtra("postSn", postSn);
        notificationPlayerService.setAction(NotificationPlayerConstants.ACTION.START_ACTION);
//        bindService(notificationPlayerService, serviceConnection, Context.BIND_AUTO_CREATE);
        startService(notificationPlayerService);

    }

    NotificationPlayerService notificationPlayerService;
    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            NotificationPlayerService.LocalBinder binder = (NotificationPlayerService.LocalBinder) service;
            notificationPlayerService = binder.getServiceInstance();
            notificationPlayerService.registerClient(MainActivity.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

//    public void playNotificationPlayerService(String cover_image_url, String title, String artist_name, String audio_file_url) {
//        Intent notificationPlayerService = new Intent(MainActivity.this, NotificationPlayerService.class);
//        notificationPlayerService.putExtra("cover_image_url", cover_image_url);
//        notificationPlayerService.putExtra("title", title);
//        notificationPlayerService.putExtra("artist_name", artist_name);
//        notificationPlayerService.putExtra("audio_file_url", audio_file_url);
//        notificationPlayerService.setAction(NotificationPlayerConstants.ACTION.PLAY_ACTION);
//        startService(notificationPlayerService);
//    }

    @Override
    public void playNextCallback() {
        playNextPlaylist();
    }

    @Override
    public void playPrevCallback() {
        playPrevPlaylist();
    }

    @Override
    public void playCallback() {
        btn_playImageButton.setImageResource(R.drawable.ic_pause_white_24dp);
    }

    @Override
    public void pauseCallback() {
        btn_playImageButton.setImageResource(R.drawable.ic_play_arrow_white_24dp);
    }

    @Override
    public String getActivePostSn() {
        return getPostSnActivePlaylist();
    }

    public void setPlaylistForService(ArrayList<Playlist> Playlist) {
        if (notificationPlayerService != null) {
            notificationPlayerService.playlists = Playlist;
        }
    }

    public void viewNotifications() {
        Utils.replaceFragment(this, new NotificationsFragment(), new Bundle());
    }


}
