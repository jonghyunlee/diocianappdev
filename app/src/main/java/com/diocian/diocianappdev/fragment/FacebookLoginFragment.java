package com.diocian.diocianappdev.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class FacebookLoginFragment extends Fragment {

    private static final String target_url_prefix = "www.diocian.com";
    private Context context;
    private WebView webViewFacebookLoginPop;
    private FrameLayout frameLayoutFacebookLogin;
    private String jobCode;

    View view;

    public FacebookLoginFragment() {
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_facebook_login, container, false);

        Bundle bundle = this.getArguments();
        jobCode = "logIn";
        if (bundle != null) {
            jobCode = bundle.getString("jobCode");
        }

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);

        frameLayoutFacebookLogin = (FrameLayout) view.findViewById(R.id.frameLayoutFacebookLogin);
        WebView webViewFacebookLogin = (WebView) view.findViewById(R.id.webViewFacebookLogin);
        if (webViewFacebookLogin != null) {
            webViewFacebookLogin.clearCache(true);
            webViewFacebookLogin.clearHistory();
            clearCookies(getActivity());
            WebSettings webSettingsFacebookLogin = webViewFacebookLogin.getSettings();
            webSettingsFacebookLogin.setJavaScriptEnabled(true);
            webSettingsFacebookLogin.setAppCacheEnabled(true);
            webSettingsFacebookLogin.setJavaScriptCanOpenWindowsAutomatically(true);
            webSettingsFacebookLogin.setSupportMultipleWindows(true);

            FacebookLoginJavascriptInterface facebookLoginJavascriptInterface = new FacebookLoginJavascriptInterface(getActivity(), webViewFacebookLogin);
            webViewFacebookLogin.addJavascriptInterface(facebookLoginJavascriptInterface, "DiocianApp");

            webViewFacebookLogin.setWebViewClient(new FacebookWebViewClient());
            webViewFacebookLogin.setWebChromeClient(new FacebookWebChromeClient());
            webViewFacebookLogin.loadUrl(GlobalConstants.DOMAIN + "/facebook/login/");
        }
        context = getActivity();

        return view;
    }

    @SuppressWarnings("deprecation")
    public static void clearCookies(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }

    private class FacebookWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            String host = Uri.parse(url).getHost();
            if (host.equals(target_url_prefix)) {
                if (webViewFacebookLoginPop != null) {
                    webViewFacebookLoginPop.setVisibility(View.GONE);
                    frameLayoutFacebookLogin.removeView(webViewFacebookLoginPop);
                    webViewFacebookLoginPop = null;
                }
                return false;
            }
            if (host.equals("m.facebook.com") || host.equals("www.facebook.com")) {
                return false;
            }
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            return true;
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            //super.onReceivedSslError(view, handler, error);
        }
    }

    class FacebookWebChromeClient extends WebChromeClient {

        @SuppressLint("SetJavaScriptEnabled")
        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
            webViewFacebookLoginPop = new WebView(context);
            webViewFacebookLoginPop.setVerticalScrollBarEnabled(false);
            webViewFacebookLoginPop.setHorizontalScrollBarEnabled(false);
            webViewFacebookLoginPop.setWebViewClient(new FacebookWebViewClient());
            webViewFacebookLoginPop.getSettings().setJavaScriptEnabled(true);
            webViewFacebookLoginPop.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            frameLayoutFacebookLogin.addView(webViewFacebookLoginPop);
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(webViewFacebookLoginPop);
            resultMsg.sendToTarget();
            return true;
        }

        @Override
        public void onCloseWindow(WebView window) {
            Log.d("onCloseWindow", "called");
        }

    }

    public class FacebookLoginJavascriptInterface {

        private Activity activity;
        private WebView webView;

        public FacebookLoginJavascriptInterface(Activity activity, WebView webView) {
            this.activity = activity;
            this.webView = webView;
        }

        @JavascriptInterface
        public void facebookLoginCallback(String message) throws JSONException {
//            Snackbar.make(webView, jobCode + " : " + message, Snackbar.LENGTH_LONG).setAction("Action", null).show();
            JSONObject jsonObject = new JSONObject(message);
            if (jobCode.equals("signUp")) {
                signUp("F", jsonObject.getString("name"), jsonObject.getString("id"), "", jsonObject.getString("email"));
            } else if (jobCode.equals("logIn")) {
                login("F", jsonObject.getString("id"), "");
            }

        }

        @JavascriptInterface
        public void showMessageToSnackbar(String message) {
//            Snackbar.make(webView, message, Snackbar.LENGTH_LONG).setAction("Action", null).show();

        }

    }

    public void signUp(final String member_dc, String member_name, String member_id, String member_pw, String email_address) {
        try {
            member_name = URLEncoder.encode(member_name, "UTF-8");
            member_id = URLEncoder.encode(member_id, "UTF-8");
            member_pw = URLEncoder.encode(member_pw, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url = GlobalConstants.DOMAIN + "/signup.do?member_dc=" + member_dc + "&member_name=" + member_name + "&member_id=" + member_id + "&member_pw=" + member_pw + "&email_address=" + email_address;

        HttpRequest httpRequest = new HttpRequest(getContext());
        HttpRequest.HttpGetRequestTask studiosHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, Context context) {
                try {
                    JSONObject result = jsonObject.getJSONObject("result");
                    if (result.getString("success_yn").equals("Y")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Sing up succeeded.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                fragmentManager.popBackStackImmediate();
                                fragmentManager.popBackStackImmediate();
                            }
                        });
                        builder.show();

                    } else {

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Sing up failed. (Already registered account.)");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                fragmentManager.popBackStackImmediate();
                            }
                        });
                        builder.show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        studiosHttpGetRequestTask.execute(url);

    }

    public void login(String member_dc, String member_id, String member_pw) {
        try {
            member_id = URLEncoder.encode(member_id, "UTF-8");
            member_pw = URLEncoder.encode(member_pw, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url = GlobalConstants.DOMAIN + "/login.do?member_dc=" + member_dc + "&member_id=" + member_id + "&member_pw=" + member_pw;

        HttpRequest httpRequest = new HttpRequest(getContext());
        HttpRequest.HttpGetRequestTask studiosHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, Context context) {
                try {
                    JSONObject result = jsonObject.getJSONObject("result");
                    if (result.getString("success_yn").equals("Y")) {
                        JSONObject member = jsonObject.getJSONObject("member");

                        ((MainActivity) getActivity()).removeAllPreferences();
                        ((MainActivity) getActivity()).savePreferences("member_sn", member.getString("member_sn"));
                        ((MainActivity) getActivity()).savePreferences("member_name", member.getString("member_name"));
                        ((MainActivity) getActivity()).savePreferences("member_summary", member.getString("member_summary"));
                        ((MainActivity) getActivity()).savePreferences("member_image_url", member.getString("member_image_url"));
                        ((MainActivity) getActivity()).savePreferences("home_image_url", member.getString("home_image_url"));
                        ((MainActivity) getActivity()).savePreferences("session_id", member.getJSONObject("loginAuth").getString("session_id"));
                        ((MainActivity) getActivity()).savePreferences("session_id_raw", jsonObject.getString("session_id"));
                        ((MainActivity) getActivity()).savePreferences("client_auth_cd", member.getJSONObject("loginAuth").getString("client_auth_cd"));
                        ((MainActivity) getActivity()).isPlaylist = false;
                        ((MainActivity) getActivity()).setLoginInfo();

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Welcome! " + member.getString("member_name") + ".");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                fragmentManager.popBackStackImmediate();
                                fragmentManager.popBackStackImmediate();
                            }
                        });
                        builder.show();

                    } else {

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Login failed.");
                        builder.setPositiveButton("Ok", null);
                        builder.show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        studiosHttpGetRequestTask.execute(url);

    }

}