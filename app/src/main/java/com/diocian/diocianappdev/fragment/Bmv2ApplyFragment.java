package com.diocian.diocianappdev.fragment;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.HttpRequestByMultipart;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class Bmv2ApplyFragment extends Fragment {

    AppCompatEditText editTextTitle;
    AppCompatEditText editTextDescription;

    TextInputLayout textInputLayoutTitle;
    TextInputLayout textInputLayoutDescription;

    TextView textViewFileInfo;
    TextView textViewAudioInfo;
    TextView textViewImageInfo;
    TextView textViewRecodeInfo;

    Uri fileUri = null;
    Uri audioFileUri = null;
    Uri imageFileUri = null;
    Uri recodedFileUri = null;


    private boolean loading = false;
    private boolean isEnd = false;
    AppCompatSpinner appCompatSpinnerRefSn;

    int tabPosition;

    ContentLoadingProgressBar contentLoadingProgressBarSend;
    Button btn_login_with_email;

    Button buttonRecode;
    Button buttonStopRecode;

    List<Post> musics;

    View view;

    private String audioFilePath2;
    private String audioFilePath;

    public Bmv2ApplyFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_bmv1_apply, container, false);

        audioFilePath = Environment.getExternalStorageDirectory().getPath() + "/diocian_recorded.mp4";
        audioFilePath2 = getActivity().getFilesDir() + "/diocian_recorded.mp4";

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.app_bar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        String toolbar_title = getString(R.string.registration).toUpperCase();
        toolbar.setTitle(toolbar_title);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        loadMorePosts();

        editTextTitle = (AppCompatEditText) view.findViewById(R.id.editTextTitle);
        editTextDescription = (AppCompatEditText) view.findViewById(R.id.editTextDescription);

        textInputLayoutTitle = (TextInputLayout) view.findViewById(R.id.textInputLayoutTitle);
        textInputLayoutDescription = (TextInputLayout) view.findViewById(R.id.textInputLayoutDescription);

        Utils.addTextChangedListenerToCheckLengthInTextInputLayout(textInputLayoutTitle, 0, "Title is required.");

        textViewFileInfo = (TextView) view.findViewById(R.id.textViewFileInfo);
        textViewAudioInfo = (TextView) view.findViewById(R.id.textViewAudioInfo);
        textViewImageInfo = (TextView) view.findViewById(R.id.textViewImageInfo);
        textViewRecodeInfo = (TextView) view.findViewById(R.id.textViewRecodeInfo);

        ImageButton imageButtonVideoPlay = (ImageButton) view.findViewById(R.id.imageButtonVideoPlay);
        ImageButton imageButtonAudioPlay = (ImageButton) view.findViewById(R.id.imageButtonAudioPlay);
        ImageButton imageButtonImagePreview = (ImageButton) view.findViewById(R.id.imageButtonImagePreview);
        ImageButton imageButtonRecodedAudioPlay = (ImageButton) view.findViewById(R.id.imageButtonRecodedAudioPlay);

        textViewFileInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fileUri != null) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    i.setDataAndType(fileUri, "video/*");
                    startActivity(i);
                }
            }
        });
        imageButtonVideoPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fileUri != null) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    i.setDataAndType(fileUri, "video/*");
                    startActivity(i);
                }
            }
        });

        textViewAudioInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (audioFileUri != null) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    i.setDataAndType(audioFileUri, "audio/*");
                    startActivity(i);
                }
            }
        });
        imageButtonAudioPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (audioFileUri != null) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    i.setDataAndType(audioFileUri, "audio/*");
                    startActivity(i);
                }
            }
        });

        textViewImageInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageFileUri != null) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setDataAndType(imageFileUri, "image/*");
                    startActivity(i);
                }
            }
        });
        imageButtonImagePreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageFileUri != null) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setDataAndType(imageFileUri, "image/*");
                    startActivity(i);
                }
            }
        });

        textViewRecodeInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recodedFileUri != null) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setDataAndType(recodedFileUri, "audio/*");
                    startActivity(i);
                }
            }
        });
        imageButtonRecodedAudioPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recodedFileUri != null) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setDataAndType(recodedFileUri, "audio/*");
                    startActivity(i);
                }
            }
        });


        TabLayout tabLayoutApplyDc = (TabLayout) view.findViewById(R.id.tabLayoutApplyDc);
        tabLayoutApplyDc.addTab(tabLayoutApplyDc.newTab().setText("Audio"));
        tabLayoutApplyDc.addTab(tabLayoutApplyDc.newTab().setText("Video"));
        tabLayoutApplyDc.addTab(tabLayoutApplyDc.newTab().setText("Record"));
        tabLayoutApplyDc.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.d("TEST---->", "setOnTabSelectedListener : " + tab.getPosition());
                LinearLayout linearLayoutAudio = (LinearLayout) view.findViewById(R.id.linearLayoutAudio);
                LinearLayout linearLayoutVideo = (LinearLayout) view.findViewById(R.id.linearLayoutVideo);
                LinearLayout linearLayoutRecode = (LinearLayout) view.findViewById(R.id.linearLayoutRecode);
                LinearLayout linearLayoutImage = (LinearLayout) view.findViewById(R.id.linearLayoutImage);

                linearLayoutAudio.setVisibility(View.GONE);
                linearLayoutVideo.setVisibility(View.GONE);
                linearLayoutRecode.setVisibility(View.GONE);
                linearLayoutImage.setVisibility(View.GONE);

                tabPosition = tab.getPosition();
                if (tabPosition == 0) {
                    linearLayoutAudio.setVisibility(View.VISIBLE);
                    linearLayoutImage.setVisibility(View.VISIBLE);
                } else if (tabPosition == 1) {
                    linearLayoutVideo.setVisibility(View.VISIBLE);
                } else if (tabPosition == 2) {
                    linearLayoutRecode.setVisibility(View.VISIBLE);
                    linearLayoutImage.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        Button buttonSelectAudio = (Button) view.findViewById(R.id.buttonSelectAudio);
        buttonSelectAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser("audio/*", AUDIO_FILE_SELECT_CODE);
            }
        });

        Button btn_select_file = (Button) view.findViewById(R.id.btn_select_file);
        btn_select_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser("video/*", FILE_SELECT_CODE);
            }
        });

        Button buttonSelectImage = (Button) view.findViewById(R.id.buttonSelectImage);
        buttonSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser("image/*", IMAGE_FILE_SELECT_CODE);
            }
        });

        buttonRecode = (Button) view.findViewById(R.id.buttonRecode);
        buttonRecode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    recodeAudio();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        buttonStopRecode = (Button) view.findViewById(R.id.buttonStopRecode);
        buttonStopRecode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopRecodeAudio();
            }
        });

        btn_login_with_email = (Button) view.findViewById(R.id.btn_signup_with_email);
        btn_login_with_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                String parent_sn = "7852";
                String target_dc = "entry";
                String ref_sn;
                String title = editTextTitle.getText().toString();
                String description = editTextDescription.getText().toString();
                String public_dc = "Y";
                String base_url = "bmv2-1";
                String apply_dc;

                ref_sn = String.valueOf(appCompatSpinnerRefSn.getSelectedItemPosition());
                Log.v(GlobalConstants.LOG_TAG, "appCompatSpinnerRefSn.getSelectedItemPosition() ----->" + appCompatSpinnerRefSn.getSelectedItemPosition());
                Log.v(GlobalConstants.LOG_TAG, "ref_sn ----->" + ref_sn);
                if (ref_sn.equals("0")) {
                    ref_sn = "7841";
                } else if (ref_sn.equals("1")) {
                    ref_sn = "7842";
                } else if (ref_sn.equals("2")) {
                    ref_sn = "7843";
                }
                Log.v(GlobalConstants.LOG_TAG, "ref_sn ----->" + ref_sn);
                apply_dc = String.valueOf(tabPosition + 1);

                // TODO : B 오디션 참가 Error Message 변수로 변경
                // TODO : B 오디션 참가 Error Message 다국어

                Utils.checkLengthInTextInputLayout(textInputLayoutTitle, 0, "Title is required.");

                if (textInputLayoutTitle.isErrorEnabled()) {
                    Log.d("Dummy", "");
                } else if (apply_dc.equals("1") && audioFileUri == null) {
                    textViewAudioInfo.setTextSize(getResources().getDimension(R.dimen.font_size_body_shorter) / getResources().getDisplayMetrics().density);
                    textViewAudioInfo.setTextColor(ContextCompat.getColor(getContext(), R.color.error));
                    textViewAudioInfo.setText("Audio file is required.");
                } else if (apply_dc.equals("3") && recodedFileUri == null) {
                    textViewRecodeInfo.setTextSize(getResources().getDimension(R.dimen.font_size_body_shorter) / getResources().getDisplayMetrics().density);
                    textViewRecodeInfo.setTextColor(ContextCompat.getColor(getContext(), R.color.error));
                    textViewRecodeInfo.setText("Recode file is required.");
                } else if ((apply_dc.equals("1") || apply_dc.equals("3")) && imageFileUri == null) {
                    textViewImageInfo.setTextSize(getResources().getDimension(R.dimen.font_size_body_shorter) / getResources().getDisplayMetrics().density);
                    textViewImageInfo.setTextColor(ContextCompat.getColor(getContext(), R.color.error));
                    textViewImageInfo.setText("Image file is required.");
                } else if (apply_dc.equals("2") && fileUri == null) {
                    textViewFileInfo.setTextSize(getResources().getDimension(R.dimen.font_size_body_shorter) / getResources().getDisplayMetrics().density);
                    textViewFileInfo.setTextColor(ContextCompat.getColor(getContext(), R.color.error));
                    textViewFileInfo.setText("Video file is required.");
                } else {
                    if (apply_dc.equals("1")) {
                        fileUri = audioFileUri;
                    } else if (apply_dc.equals("3")) {
                        fileUri = recodedFileUri;
                    }
                    send(parent_sn, target_dc, ref_sn, title, description, public_dc, base_url, apply_dc, fileUri, imageFileUri);
                }
            }
        });
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_competition, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menuItemYoutube) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            Uri u = Uri.parse("https://www.youtube.com/channel/UC1zo0v3_GHwKRUM5cF3yGpg");
            i.setData(u);
            startActivity(i);
            return true;
        } else if (id == R.id.menuItemFacebook) {
            String lang_cd = getResources().getString(R.string.lang_cd);
            String uriString = "https://www.facebook.com/diocianglobal/";
            if (lang_cd.equals("jp")) {
                uriString = "https://www.facebook.com/diocianjapan/";
            } else if (lang_cd.equals("kr")) {
                uriString = "https://www.facebook.com/diociankorea/";
            } else if (lang_cd.equals("vn")) {
                uriString = "https://www.facebook.com/diocianvietnam/";
            }
            Intent i = new Intent(Intent.ACTION_VIEW);
            Uri u = Uri.parse(uriString);
            i.setData(u);
            startActivity(i);
            return true;
        } else if (id == android.R.id.home) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.popBackStackImmediate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void send(String parent_sn, String target_dc, String ref_sn, String title, String description, String public_dc, String base_url, String apply_dc, Uri fileUri, Uri imageFileUri) {

        contentLoadingProgressBarSend = (ContentLoadingProgressBar) view.findViewById(R.id.contentLoadingProgressBarSend);
        contentLoadingProgressBarSend.setVisibility(View.VISIBLE);
        btn_login_with_email.setVisibility(View.GONE);

        try {
            title = URLEncoder.encode(title, "UTF-8");
            description = URLEncoder.encode(description, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url = GlobalConstants.DOMAIN + "/setEntry.do?parent_sn=" + parent_sn + "&target_dc=" + target_dc + "&ref_sn=" + ref_sn + "&title=" + title + "&description=" + description + "&public_dc=" + public_dc + "&base_url=" + base_url + "&apply_dc=" + apply_dc;

        HttpRequestByMultipart httpRequest = new HttpRequestByMultipart(getContext());
        HttpRequestByMultipart.HttpGetRequestTask studiosHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequestByMultipart.CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, Context context) {
                try {
                    contentLoadingProgressBarSend.setVisibility(View.GONE);
                    btn_login_with_email.setVisibility(View.VISIBLE);
                    JSONObject result = jsonObject.getJSONObject("result");
                    if (result.getString("success_yn").equals("Y")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Applications completed.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Bundle bundle = new Bundle();
                                bundle.putInt("tabNo", 1);
                                Utils.replaceFragment((MainActivity) getActivity(), new Bmv2Fragment(), bundle, false);
                            }
                        });
                        builder.show();
                    } else {

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Applications failed. " + result.getString("message"));
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ((MainActivity) getActivity()).viewLogIn(view);
                            }
                        });
                        builder.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, fileUri, imageFileUri);
        studiosHttpGetRequestTask.execute(url);

    }

    private static final int FILE_SELECT_CODE = 0;
    private static final int AUDIO_FILE_SELECT_CODE = 1;
    private static final int IMAGE_FILE_SELECT_CODE = 4;

    private void showFileChooser(String intentType, int fileTypeCode) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType(intentType);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), fileTypeCode);
        } catch (android.content.ActivityNotFoundException e) {
        }
    }

    final static long MAXIMUM_FILE_SIZE = 104857600;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == FragmentActivity.RESULT_OK) {
                    fileUri = data.getData();

                    ContentResolver contentResolver = getContext().getContentResolver();
                    Cursor cursor = contentResolver.query(fileUri, null, null, null, null);
                    if (cursor != null) {
                        int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                        cursor.moveToFirst();
                        String fileName = cursor.getString(nameIndex);
                        nameIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
                        cursor.moveToFirst();
                        long fileSize = Long.parseLong(cursor.getString(nameIndex));

                        nameIndex = cursor.getColumnIndexOrThrow("mime_type");
                        cursor.moveToFirst();
                        String fileType = cursor.getString(nameIndex);

//                        for (int i = 0; i < cursor.getColumnCount(); i++) {
//                            Log.d("TEST----->","getColumnName("+i+") : "+cursor.getColumnName(i));
//                        }

                        cursor.close();

                        if (fileSize > MAXIMUM_FILE_SIZE) {
                            textViewFileInfo.setTextSize(getResources().getDimension(R.dimen.font_size_body_shorter) / getResources().getDisplayMetrics().density);
                            textViewFileInfo.setTextColor(ContextCompat.getColor(getContext(), R.color.error));
                            textViewFileInfo.setText("File is too large. (Maximum 100MB)");
                            fileUri = null;
                        } else {
                            textViewFileInfo.setTextSize(getResources().getDimension(R.dimen.font_size_body) / getResources().getDisplayMetrics().density);
                            textViewFileInfo.setTextColor(ContextCompat.getColor(getContext(), R.color.BlackSecondary));
                            textViewFileInfo.setText(fileName + " / " + Math.round(fileSize / 1024 / 1024) + "MB");
                        }
                    }

                }
                break;
            case AUDIO_FILE_SELECT_CODE:
                if (resultCode == FragmentActivity.RESULT_OK) {
                    audioFileUri = data.getData();

                    ContentResolver contentResolver = getContext().getContentResolver();
                    Cursor cursor = contentResolver.query(audioFileUri, null, null, null, null);
                    if (cursor != null) {
                        int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                        cursor.moveToFirst();
                        String fileName = cursor.getString(nameIndex);
                        nameIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
                        cursor.moveToFirst();
                        long fileSize = Long.parseLong(cursor.getString(nameIndex));
                        cursor.close();

                        if (fileSize > MAXIMUM_FILE_SIZE) {
                            textViewAudioInfo.setTextSize(getResources().getDimension(R.dimen.font_size_body_shorter) / getResources().getDisplayMetrics().density);
                            textViewAudioInfo.setTextColor(ContextCompat.getColor(getContext(), R.color.error));
                            textViewAudioInfo.setText("File is too large. (Maximum 100MB)");
                            audioFileUri = null;
                        } else {
                            textViewAudioInfo.setTextSize(getResources().getDimension(R.dimen.font_size_body) / getResources().getDisplayMetrics().density);
                            textViewAudioInfo.setTextColor(ContextCompat.getColor(getContext(), R.color.BlackSecondary));
                            textViewAudioInfo.setText(fileName + " / " + Math.round(fileSize / 1024 / 1024) + "MB");
                        }
                    }
                }
                break;
            case IMAGE_FILE_SELECT_CODE:
                if (resultCode == FragmentActivity.RESULT_OK) {
                    imageFileUri = data.getData();

                    ContentResolver contentResolver = getContext().getContentResolver();
                    Cursor cursor = contentResolver.query(imageFileUri, null, null, null, null);
                    if (cursor != null) {
                        int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                        cursor.moveToFirst();
                        String fileName = cursor.getString(nameIndex);
                        nameIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
                        cursor.moveToFirst();
                        long fileSize = Long.parseLong(cursor.getString(nameIndex));
                        cursor.close();

                        if (fileSize > MAXIMUM_FILE_SIZE) {
                            textViewImageInfo.setTextSize(getResources().getDimension(R.dimen.font_size_body_shorter) / getResources().getDisplayMetrics().density);
                            textViewImageInfo.setTextColor(ContextCompat.getColor(getContext(), R.color.error));
                            textViewImageInfo.setText("File is too large. (Maximum 100MB)");
                            imageFileUri = null;
                        } else {
                            textViewImageInfo.setTextSize(getResources().getDimension(R.dimen.font_size_body) / getResources().getDisplayMetrics().density);
                            textViewImageInfo.setTextColor(ContextCompat.getColor(getContext(), R.color.BlackSecondary));
                            textViewImageInfo.setText(fileName + " / " + Math.round(fileSize / 1024 / 1024) + "MB");
                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void loadMorePosts() {
        if (!loading && !isEnd) {
            loading = true;
            String url = GlobalConstants.DOMAIN + "/json/bmv2.jsp";
            HttpRequest httpRequest = new HttpRequest(getContext());
            HttpRequest.HttpGetRequestTask studiosHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                @Override
                public void callback(JSONObject jsonObject, Context context) {
                    try {
                        String lang_cd = getResources().getString(R.string.lang_cd);
                        JSONArray mrs = jsonObject.getJSONArray("mrs");
                        JSONObject mr;
                        ArrayList<String> arrayList = new ArrayList<>();
                        musics = new ArrayList<>();
                        for (int i = 0; i < mrs.length(); i++) {
                            mr = mrs.getJSONObject(i);
                            arrayList.add(mr.getString("title") + "-" + mr.getString("artist_name"));
                            musics.add(new Post().setMusic("", mr.getString("title"), mr.getString("artist_name"), GlobalConstants.DOMAIN + mr.getString("cover_image_url"), "", "", "", "", "", GlobalConstants.DOMAIN + mr.getString("audio_file_url"), "", "", ""));
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, arrayList);

                        appCompatSpinnerRefSn = (AppCompatSpinner) view.findViewById(R.id.appCompatSpinnerRefSn);
                        appCompatSpinnerRefSn.setPrompt("Remake Music");
                        appCompatSpinnerRefSn.setAdapter(adapter);

                        Handler mHandler = new Handler();
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                isEnd = true;
                            }
                        }, 500);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            studiosHttpGetRequestTask.execute(url);
        }
    }

//    protected boolean hasMicrophone() {
//        PackageManager packageManager = getActivity().getPackageManager();
//        return packageManager.hasSystemFeature(PackageManager.FEATURE_MICROPHONE);
//    }

    private MediaRecorder mediaRecorder;


//    private boolean isRecording = false;

    public void recodeAudio() throws IOException {


        if (Utils.checkPermission(getActivity(), Manifest.permission.RECORD_AUDIO, 0) && Utils.checkPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE, 0)) {
//            isRecording = true;

            ((MainActivity) getActivity()).playMediaPlayer2(musics.get(appCompatSpinnerRefSn.getSelectedItemPosition()));

            buttonRecode.setVisibility(View.GONE);
            buttonStopRecode.setVisibility(View.VISIBLE);

            if (mediaRecorder != null) {
                mediaRecorder.stop();
                mediaRecorder.release();
                mediaRecorder = null;
            }
            mediaRecorder = new MediaRecorder();
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);

            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            File file = new File(audioFilePath);
            if (file.exists()) {
                file.delete();
            }
            mediaRecorder.setOutputFile(audioFilePath);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);

            mediaRecorder.prepare();

            mediaRecorder.start();
        }
    }

    public void stopRecodeAudio() {
        ((MainActivity) getActivity()).stopMediaPlayer2();

        buttonRecode.setVisibility(View.VISIBLE);
        buttonStopRecode.setVisibility(View.GONE);

        mediaRecorder.stop();
        mediaRecorder.release();
        mediaRecorder = null;
//        isRecording = false;

        File file = new File(audioFilePath);
        recodedFileUri = Uri.fromFile(file);

        if (file.length() > MAXIMUM_FILE_SIZE) {
            textViewRecodeInfo.setTextSize(getResources().getDimension(R.dimen.font_size_body_shorter) / getResources().getDisplayMetrics().density);
            textViewRecodeInfo.setTextColor(ContextCompat.getColor(getContext(), R.color.error));
            textViewRecodeInfo.setText("File is too large. (Maximum 100MB)");
            fileUri = null;
        } else {
            textViewRecodeInfo.setTextSize(getResources().getDimension(R.dimen.font_size_body) / getResources().getDisplayMetrics().density);
            textViewRecodeInfo.setTextColor(ContextCompat.getColor(getContext(), R.color.BlackSecondary));
            textViewRecodeInfo.setText(file.getName() + " / " + Math.ceil(file.length() / 1024 / 1024) + "MB");
        }

    }


}