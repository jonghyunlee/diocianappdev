package com.diocian.diocianappdev.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class SignupFragment extends Fragment {

    AppCompatEditText edit_text_member_name;
    AppCompatEditText edit_text_member_id;
    AppCompatEditText edit_text_member_pw;
    AppCompatEditText edit_text_member_pw2;

    TextInputLayout textInputLayoutMemberName;
    TextInputLayout textInputLayoutMemberId;
    TextInputLayout textInputLayoutMemberPw;
    TextInputLayout textInputLayoutMemberPw2;

    View view;

    public SignupFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_signup, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.app_bar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        String toolbar_title = getString(R.string.signup).toUpperCase();
        toolbar.setTitle(toolbar_title);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Button buttonSignUpByFacebook = (Button) view.findViewById(R.id.buttonSignUpByFacebook);
        buttonSignUpByFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).viewFacebookLogin("signUp");
            }
        });

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
            Utils.setBackgroundTintUnderLollipop(getActivity(), buttonSignUpByFacebook, R.color.facebook);

        edit_text_member_name = (AppCompatEditText) view.findViewById(R.id.edit_text_member_name);
        edit_text_member_id = (AppCompatEditText) view.findViewById(R.id.edit_text_member_id);
        edit_text_member_pw = (AppCompatEditText) view.findViewById(R.id.edit_text_member_pw);
        edit_text_member_pw2 = (AppCompatEditText) view.findViewById(R.id.edit_text_member_pw2);

        textInputLayoutMemberName = (TextInputLayout) view.findViewById(R.id.TextInputLayoutMemberName);
        textInputLayoutMemberId = (TextInputLayout) view.findViewById(R.id.TextInputLayoutMemberId);
        textInputLayoutMemberPw = (TextInputLayout) view.findViewById(R.id.TextInputLayoutMemberPw);
        textInputLayoutMemberPw2 = (TextInputLayout) view.findViewById(R.id.TextInputLayoutMemberPw2);

        // TODO : SignUp Error Message 변수로 변경
        // TODO : SignUp Error Message 다국어

        Utils.addTextChangedListenerToCheckLengthInTextInputLayout(textInputLayoutMemberName, 0, "Name is required.");
        Utils.addTextChangedListenerToCheckLengthInTextInputLayout(textInputLayoutMemberId, 0, "ID is required.");
        Utils.addTextChangedListenerToCheckLengthInTextInputLayout(textInputLayoutMemberPw, 7, "Password is required. (Minimum 8 characters.)");
        Utils.addTextChangedListenerToCheckLengthInTextInputLayout(textInputLayoutMemberPw2, 7, "Confirm password is required. (Minimum 8 characters.)");

        Button btn_login_with_email = (Button) view.findViewById(R.id.btn_signup_with_email);
        btn_login_with_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                String member_dc = "E";
                String member_name = edit_text_member_name.getText().toString();
                String member_id = edit_text_member_id.getText().toString();
                String member_pw = edit_text_member_pw.getText().toString();
                String member_pw2 = edit_text_member_pw2.getText().toString();

                Utils.checkLengthInTextInputLayout(textInputLayoutMemberName, 0, "Name is required.");
                Utils.checkLengthInTextInputLayout(textInputLayoutMemberId, 0, "ID is required.");
                Utils.checkLengthInTextInputLayout(textInputLayoutMemberPw, 7, "Password is required. (Minimum 8 characters.)");
                Utils.checkLengthInTextInputLayout(textInputLayoutMemberPw2, 7, "Confirm password is required. (Minimum 8 characters.)");

                if (textInputLayoutMemberName.isErrorEnabled()) {
                    Log.d("", "");
                } else if (textInputLayoutMemberId.isErrorEnabled()) {
                    Log.d("", "");
                } else if (textInputLayoutMemberPw.isErrorEnabled()) {
                    Log.d("", "");
                } else if (textInputLayoutMemberPw2.isErrorEnabled()) {
                    Log.d("", "");
                } else if (!member_pw.equals(member_pw2)) {
                    textInputLayoutMemberPw2.setErrorEnabled(true);
                    textInputLayoutMemberPw2.setError("Password is not correct.");
                } else {
                    signUp(member_dc, member_name, member_id, member_pw);
                }

            }
        });
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_none, menu);
        Utils.showMenuNotificationCount(menu, getActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_notification) {
            ((MainActivity) getActivity()).viewNotifications();
            return true;
        } else if (id == android.R.id.home) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.popBackStackImmediate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void signUp(final String member_dc, String member_name, String member_id, String member_pw) {
        try {
            member_name = URLEncoder.encode(member_name, "UTF-8");
            member_id = URLEncoder.encode(member_id, "UTF-8");
            member_pw = URLEncoder.encode(member_pw, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url = GlobalConstants.DOMAIN + "/signup.do?member_dc=" + member_dc + "&member_name=" + member_name + "&member_id=" + member_id + "&member_pw=" + member_pw;

        HttpRequest httpRequest = new HttpRequest(getContext());
        HttpRequest.HttpGetRequestTask studiosHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, Context context) {
                try {
                    JSONObject result = jsonObject.getJSONObject("result");
                    if (result.getString("success_yn").equals("Y")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("DIOCIAN has sent an e-mail. Please email authentication.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                fragmentManager.popBackStackImmediate();
                            }
                        });
                        builder.show();

                    } else {

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Sing up failed.");
                        builder.setPositiveButton("Ok", null);
                        builder.show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        studiosHttpGetRequestTask.execute(url);

    }
}