package com.diocian.diocianappdev.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.listener.EndlessRecyclerViewScrollListener;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.adapter.MusicRecyclerAdapter;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MusicsFragment extends Fragment {
    private RecyclerView musicRecyclerView;
    private ArrayList<Post> musics = new ArrayList<>();
    private MusicRecyclerAdapter musicRecyclerAdapter;
    private ImageFetcher imageFetcher;
    private StaggeredGridLayoutManager musicLayoutManager;

    private int get_start_no = 0;
    private boolean loading = false;

    private boolean isEnd = false;

    private  String search_word;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageFetcher = new ImageFetcher(getActivity());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        musicLayoutManager.setSpanCount(Utils.calColumnCount(getContext()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        musicRecyclerView.removeAllViews();
//        musicRecyclerView = null;
//        System.gc();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_musics, container, false);

        search_word = null;
        MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity != null) {
            search_word = mainActivity.query;
        }
        if (search_word == null) {
            search_word = "";
        }

        musicRecyclerView = (RecyclerView) view.findViewById(R.id.musics_recyclerView);
        musicLayoutManager = new StaggeredGridLayoutManager(Utils.calColumnCount(getContext()), StaggeredGridLayoutManager.VERTICAL);
        musicRecyclerView.setLayoutManager(musicLayoutManager);
        musicRecyclerAdapter = new MusicRecyclerAdapter(musics, imageFetcher, getContext());
        if (isEnd) {
            musicRecyclerAdapter.removeFooter();
        }
        musicRecyclerView.setAdapter(musicRecyclerAdapter);
        musicRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(musicLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadMorePosts();
            }
        });

        if (musics.size() == 0) {
            loadMorePosts();
        }

        return view;
    }

    private void loadMorePosts() {
        if (!loading && !isEnd) {
            loading = true;
            Map<String, String> parameterMap = new HashMap<>();
            String target_dc = "music";
            parameterMap.put("target_dc", target_dc);
            parameterMap.put("get_start_no", String.valueOf(get_start_no));
            int get_count = 12;
            parameterMap.put("get_count", String.valueOf(get_count));
            parameterMap.put("search_word", search_word);
            String url = GlobalConstants.DOMAIN + "/getPosts.do?";
            for (String key : parameterMap.keySet()) {
                url += key + "=" + parameterMap.get(key) + "&";
            }
            HttpRequest httpRequest = new HttpRequest(getContext());
            HttpRequest.HttpGetRequestTask musicsHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                @Override
                public void callback(JSONObject jsonObject, Context context) {
                    try {
                        int postCount = jsonObject.getInt("postCount");
                        JSONArray posts = jsonObject.getJSONArray("posts");
                        JSONArray images = new JSONArray();
                        if(jsonObject.has("images")) {
                            images = jsonObject.getJSONArray("images");
                        }
                        JSONObject post, image;
                        String imageUrl;
                        for (int i = 0; i < posts.length(); i++) {
                            post = posts.getJSONObject(i);
                            imageUrl = null;
                            for (int j = 0; j < images.length(); j++) {
                                image = images.getJSONObject(j);
                                if (post.getString("post_sn").equals(image.getString("target_sn"))) {
                                    imageUrl = GlobalConstants.DOMAIN + image.getString("file_url");
                                    break;
                                }
                            }
                            musics.add(new Post().setMusic(post.getString("post_sn"), post.getString("title"), post.getString("artist_name"), imageUrl, post.getString("genre_name"), post.getString("like_sn"), post.getString("comment_sn"), post.getString("member_name"), GlobalConstants.DOMAIN + post.getString("member_image_url"), GlobalConstants.DOMAIN + post.getString("audio_file_url"), post.getString("creator_sn"), post.getString("my_like_count"), post.getString("follow_sn")));
                            musicRecyclerAdapter.notifyItemInserted(get_start_no + 1 + i);
                        }

                        get_start_no = get_start_no + posts.length();
                        if (musics.size() < postCount) {
                            loading = false;
                        } else {
                            Handler mHandler = new Handler();
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    musicRecyclerAdapter.removeFooter();
                                    isEnd = true;
                                    musicRecyclerAdapter.notifyItemRemoved(get_start_no + 2);
                                }
                            }, 500);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            musicsHttpGetRequestTask.execute(url);
        }
    }

}
