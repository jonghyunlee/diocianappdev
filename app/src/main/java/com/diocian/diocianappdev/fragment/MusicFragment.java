package com.diocian.diocianappdev.fragment;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.DownloadListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.DOWNLOAD_SERVICE;

public class MusicFragment extends Fragment {

    private View view;
    private ImageFetcher imageFetcher;
    private FloatingActionButton fab;
    private String aid;
    private String am;
    private String target_dc;
    private String target_sn;
    private String post_target_dc;
    private String audio_file_url;
    private String title2;
    private String artist_name2;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        imageFetcher = new ImageFetcher(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_music, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.detail_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        fab = (FloatingActionButton) view.findViewById(R.id.fab_logout);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        Map<String, String> parameterMap = new HashMap<>();
//        String target_dc = "music";
//        parameterMap.put("target_dc", target_dc);
        Bundle bundle = this.getArguments();
        String post_sn = "";
        String buyYn = "";
        if (bundle != null) {
            post_sn = bundle.getString("post_sn");
            buyYn = bundle.getString("buyYn");
        }

        if (buyYn != null && buyYn.equals("Y")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(getResources().getString(R.string.download_now));
            builder.setPositiveButton("Download", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (Utils.checkPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE, 0)) {
                        WebView webViewDownload = (WebView) view.findViewById(R.id.webViewDownload);
                        WebSettings webSettingsFacebookLogin = webViewDownload.getSettings();
                        webSettingsFacebookLogin.setJavaScriptEnabled(true);
                        webSettingsFacebookLogin.setAppCacheEnabled(true);
                        webSettingsFacebookLogin.setJavaScriptCanOpenWindowsAutomatically(true);
                        webSettingsFacebookLogin.setSupportMultipleWindows(true);
                        webViewDownload.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                if (url.startsWith(GlobalConstants.DOMAIN + "/getMp3FileByPath.do")) {
                                    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                                    request.allowScanningByMediaScanner();
                                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, title2 + "-" + artist_name2 + ".mp3");

                                    DownloadManager downloadManager = (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
                                    downloadManager.enqueue(request);
                                } else {
                                    view.loadUrl(url);
                                }
                                return true;
                            }
                        });

                        try {
                            webViewDownload.loadUrl(GlobalConstants.DOMAIN + "/app/music/download.jsp?audio_file_url=" + URLEncoder.encode(audio_file_url, "UTF-8") + "&title=" + URLEncoder.encode(title2.replaceAll("/|'", ""), "UTF-8") + "&artist_name=" + URLEncoder.encode(artist_name2, "UTF-8"));

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            builder.setNegativeButton("No", null);
            builder.show();
        }

        parameterMap.put("post_sn", String.valueOf(post_sn));
        String url = GlobalConstants.DOMAIN + "/getPost.do?";
        for (String key : parameterMap.keySet()) {
            url += key + "=" + parameterMap.get(key) + "&";
        }
        HttpRequest httpRequest = new HttpRequest(getContext());
        HttpRequest.HttpGetRequestTask musicHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, Context context) {
                try {
                    JSONObject post = jsonObject.getJSONObject("post");
                    JSONArray images = jsonObject.getJSONArray("images");
                    JSONObject image;
                    String imageUrl;
                    imageUrl = null;
                    for (int j = 0; j < images.length(); j++) {
                        image = images.getJSONObject(j);
                        if (post.getString("post_sn").equals(image.getString("target_sn"))) {
                            imageUrl = GlobalConstants.DOMAIN + image.getString("file_url");
                            break;
                        }
                    }

                    ImageView musicImage = (ImageView) view.findViewById(R.id.image);
                    TextView genre_name = (TextView) view.findViewById(R.id.genre_name);
                    TextView like_count = (TextView) view.findViewById(R.id.like_count);
                    TextView comment_count = (TextView) view.findViewById(R.id.comment_count);
                    TextView title = (TextView) view.findViewById(R.id.title);
                    TextView artist_name = (TextView) view.findViewById(R.id.artist_name);
                    TextView creator_name = (TextView) view.findViewById(R.id.creator_name);
                    ImageView creator_image = (ImageView) view.findViewById(R.id.creator_image);
                    TextView description = (TextView) view.findViewById(R.id.description);

                    imageFetcher.setImageToImageView(imageUrl, musicImage, 360);
                    genre_name.setText(post.getString("genre_name"));
                    like_count.setText(post.getString("like_sn"));
                    comment_count.setText(post.getString("comment_sn"));
                    title.setText(post.getString("title"));
                    artist_name.setText(post.getString("artist_name"));
                    creator_name.setText(post.getString("member_name"));
                    imageFetcher.setImageToImageView(GlobalConstants.DOMAIN + post.getString("member_image_url"), creator_image, 32);
                    description.setText(post.getString("description"));



                    audio_file_url = post.getString("audio_file_url");
                    title2 = post.getString("title");
                    artist_name2 = post.getString("artist_name");

                    String location = ((MainActivity) getActivity()).getPreferences("location");
                    if (location.equals("US")) {
                        am = post.getString("US_price");
                    } else if (location.equals("JP")) {
                        am = post.getString("JP_price");
                        target_sn = post.getString("post_sn");
                    } else if (location.equals("KR")) {
                        am = post.getString("KR_price");
                        target_sn = post.getString("post_sn");
                    } else if (location.equals("VN")) {
                        am = post.getString("US_price");
                    } else {
                        am = post.getString("US_price");
                    }

                    if (post.getString("purchase_sn").equals("")&&!am.equals("0")) {
                        if (location.equals("US")) {
                        } else if (location.equals("JP")) {
                            fab.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("aid", "111150");
                                    bundle.putString("am", am);
                                    bundle.putString("target_dc", "post");
                                    bundle.putString("target_sn", target_sn);
                                    bundle.putString("post_target_dc", "music");
                                    Utils.replaceFragment((MainActivity) getActivity(), new CloudPaymentFragment(), bundle);
                                }
                            });
                        } else if (location.equals("KR")) {
                            fab.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("aid", "111150");
                                    bundle.putString("am", am);
                                    bundle.putString("target_dc", "post");
                                    bundle.putString("target_sn", target_sn);
                                    bundle.putString("post_target_dc", "music");
                                    Utils.replaceFragment((MainActivity) getActivity(), new DanalFragment(), bundle);
                                }
                            });
                        } else if (location.equals("VN")) {
                        }
                    } else {
                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Utils.checkPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE, 0)) {
                                    WebView webViewDownload = (WebView) view.findViewById(R.id.webViewDownload);
                                    WebSettings webSettingsFacebookLogin = webViewDownload.getSettings();
                                    webSettingsFacebookLogin.setJavaScriptEnabled(true);
                                    webSettingsFacebookLogin.setAppCacheEnabled(true);
                                    webSettingsFacebookLogin.setJavaScriptCanOpenWindowsAutomatically(true);
                                    webSettingsFacebookLogin.setSupportMultipleWindows(true);
                                    webViewDownload.setWebViewClient(new WebViewClient() {
                                        @Override
                                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                            if (url.startsWith(GlobalConstants.DOMAIN + "/getMp3FileByPath.do")) {
                                                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                                                request.allowScanningByMediaScanner();
                                                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                                                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, title2 + "-" + artist_name2 + ".mp3");

                                                DownloadManager downloadManager = (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
                                                downloadManager.enqueue(request);
                                            } else {
                                                view.loadUrl(url);
                                            }
                                            return true;
                                        }
                                    });

                                    try {
                                        webViewDownload.loadUrl(GlobalConstants.DOMAIN + "/app/music/download.jsp?audio_file_url=" + URLEncoder.encode(audio_file_url, "UTF-8") + "&title=" + URLEncoder.encode(title2.replaceAll("/|'", ""), "UTF-8") + "&artist_name=" + URLEncoder.encode(artist_name2, "UTF-8"));

                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        musicHttpGetRequestTask.execute(url);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_none, menu);
        Utils.showMenuNotificationCount(menu, getActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_notification) {
            ((MainActivity) getActivity()).viewNotifications();
            return true;
        } else if (id == android.R.id.home) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.popBackStackImmediate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
