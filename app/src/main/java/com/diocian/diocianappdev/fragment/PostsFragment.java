package com.diocian.diocianappdev.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.adapter.PostRecyclerAdapter;
import com.diocian.diocianappdev.listener.EndlessRecyclerViewScrollListener;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PostsFragment extends Fragment {

    private View view;
    private ImageFetcher imageFetcher;

    private RecyclerView contentRecyclerView;
    private PostRecyclerAdapter postRecyclerAdapter;
    private ArrayList<Post> contents = new ArrayList<>();
    private StaggeredGridLayoutManager contentLayoutManager;

    private int get_start_no = 0;
    private boolean loading = false;

    private boolean isEnd = false;

    String member_sn = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageFetcher = new ImageFetcher(getActivity());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        contentLayoutManager.setSpanCount(Utils.calColumnCount(getContext()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        contentRecyclerView.removeAllViews();
//        contentRecyclerView = null;
//        System.gc();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_posts, container, false);

        final Bundle bundle = this.getArguments();
        if (bundle != null) {
            member_sn = bundle.getString("member_sn");
        }

        // TODO : Article 삭제 기능 추가.
        // TODO : Article 수정 기능 추가.

        contentRecyclerView = (RecyclerView) view.findViewById(R.id.contents_recyclerView);
        contentLayoutManager = new StaggeredGridLayoutManager(Utils.calColumnCount(getContext()), StaggeredGridLayoutManager.VERTICAL);
        contentRecyclerView.setLayoutManager(contentLayoutManager);
        postRecyclerAdapter = new PostRecyclerAdapter(contents, imageFetcher, getContext());
        if (isEnd) {
            postRecyclerAdapter.removeFooter();
        }
        contentRecyclerView.setAdapter(postRecyclerAdapter);
        contentRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(contentLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadMorePosts();
            }
        });

        if (contents.size() == 0) {
            loadMorePosts();
        }

        return view;
    }

    public void loadMorePosts() {
        if (!loading && !isEnd) {
            loading = true;
            Map<String, String> parameterMap = new HashMap<>();
            parameterMap.put("get_start_no", String.valueOf(get_start_no));
            int get_count = 12;
            parameterMap.put("get_count", String.valueOf(get_count));
            parameterMap.put("creator_sn", member_sn);
            String url = GlobalConstants.DOMAIN + "/getPosts.do?targetDcsArray[]=music&targetDcsArray[]=video&targetDcsArray[]=study%20note&targetDcsArray[]=studio&targetDcsArray[]=market&targetDcsArray[]=article&";
            for (String key : parameterMap.keySet()) {
                url += key + "=" + parameterMap.get(key) + "&";
            }

            HttpRequest httpRequest = new HttpRequest(getContext());
            HttpRequest.HttpGetRequestTask studynotesHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                @Override
                public void callback(JSONObject jsonObject, Context context) {
                    try {
                        int postCount = jsonObject.getInt("postCount");
                        JSONArray posts = jsonObject.getJSONArray("posts");
                        JSONArray images = null;
                        if (jsonObject.has("images")) {
                            images = jsonObject.getJSONArray("images");
                        }
                        JSONObject post, image;
                        String imageUrl;
                        int duration_min;
                        int duration_sec;
                        for (int i = 0; i < posts.length(); i++) {
                            post = posts.getJSONObject(i);
                            imageUrl = null;
                            if (images != null) {
                                for (int j = 0; j < images.length(); j++) {
                                    image = images.getJSONObject(j);
                                    if (post.getString("post_sn").equals(image.getString("target_sn"))) {
                                        imageUrl = GlobalConstants.DOMAIN + image.getString("file_url");
                                        break;
                                    }
                                }
                            }
                            if (post.getString("target_dc").equals("study note")) {
                                if(!post.getString("play_sec").equals("")) {
                                    duration_min = (int) Math.floor(Double.parseDouble(post.getString("play_sec")) / 60);
                                    duration_sec = (int) Math.floor(Double.parseDouble(post.getString("play_sec")) % 60);
                                } else {
                                    duration_min = 0;
                                    duration_sec = 0;
                                }
                                Post tempPost = new Post().setStudynote(post.getString("post_sn"), imageUrl, post.getString("genre_name"), post.getString("job_name"), "$" + post.getString("price"), duration_min + ":" + duration_sec, GlobalConstants.DOMAIN + post.getString("image_file_url"), post.getString("title"), post.getString("member_name"), post.getString("like_sn"), post.getString("comment_sn"), Utils.getTimeLag(post.getString("created_datetime").substring(0, 19), getContext()), GlobalConstants.DOMAIN + post.getString("member_image_url"), GlobalConstants.DOMAIN + post.getString("audio_file_url"), post.getString("creator_sn"), post.getString("my_like_count"), post.getString("follow_sn"));
                                tempPost.setRedirectFragment(new PostsFragment());
                                Bundle paramBundle = new Bundle();
                                paramBundle.putString("member_sn", member_sn);
                                tempPost.setParamBundle(paramBundle);
                                contents.add(tempPost);
                                postRecyclerAdapter.notifyItemInserted(get_start_no + 1 + i);
                            } else if (post.getString("target_dc").equals("studio")) {
                                Post tempPost = new Post().setStudio(post.getString("post_sn"), post.getString("genre_name"), post.getString("job_name").replaceAll(",", " "), post.getString("title"), post.getString("member_name"), imageUrl, post.getString("like_sn"), post.getString("comment_sn"), Utils.getTimeLag(post.getString("created_datetime").substring(0, 19), getContext()), GlobalConstants.DOMAIN + post.getString("member_image_url"), post.getString("creator_sn"), post.getString("my_like_count"), post.getString("follow_sn"));
                                tempPost.setRedirectFragment(new PostsFragment());
                                Bundle paramBundle = new Bundle();
                                paramBundle.putString("member_sn", member_sn);
                                tempPost.setParamBundle(paramBundle);
                                contents.add(tempPost);
                                postRecyclerAdapter.notifyItemInserted(get_start_no + 1 + i);
                            } else if (post.getString("target_dc").equals("music")) {
                                Post tempPost = new Post().setMusic(post.getString("post_sn"), post.getString("title"), post.getString("artist_name"), imageUrl, post.getString("genre_name"), post.getString("like_sn"), post.getString("comment_sn"), post.getString("member_name"), GlobalConstants.DOMAIN + post.getString("member_image_url"), GlobalConstants.DOMAIN + post.getString("audio_file_url"), post.getString("creator_sn"), post.getString("my_like_count"), post.getString("follow_sn"));
                                tempPost.setRedirectFragment(new PostsFragment());
                                Bundle paramBundle = new Bundle();
                                paramBundle.putString("member_sn", member_sn);
                                tempPost.setParamBundle(paramBundle);
                                contents.add(tempPost);
                                postRecyclerAdapter.notifyItemInserted(get_start_no + 1 + i);
                            } else if (post.getString("target_dc").equals("video")) {
                                Post tempPost = new Post().setVideo(post.getString("youtube_video_id"), post.getString("post_sn"), post.getString("genre_name"), "http://img.youtube.com/vi/" + post.getString("youtube_video_id") + "/0.jpg", post.getString("title"), post.getString("member_name"), post.getString("like_sn"), post.getString("comment_sn"), Utils.getTimeLag(post.getString("created_datetime").substring(0, 19), getContext()), GlobalConstants.DOMAIN + post.getString("member_image_url"), post.getString("my_like_count"), post.getString("creator_sn"), post.getString("follow_sn"), post.getString("audio_file_url"));
                                tempPost.setRedirectFragment(new PostsFragment());
                                Bundle paramBundle = new Bundle();
                                paramBundle.putString("member_sn", member_sn);
                                tempPost.setParamBundle(paramBundle);
                                contents.add(tempPost);
                                postRecyclerAdapter.notifyItemInserted(get_start_no + 1 + i);
                            } else if (post.getString("target_dc").equals("market")) {
                                Post tempPost = new Post().setMarket(post.getString("post_sn"), "$" + post.getString("price"), imageUrl, post.getString("title"), post.getString("member_name"), post.getString("like_sn"), post.getString("comment_sn"), post.getString("quantity_count") + " / " + post.getString("quantity_count"), GlobalConstants.DOMAIN + post.getString("member_image_url"), post.getString("creator_sn"), post.getString("my_like_count"), post.getString("follow_sn"));
                                tempPost.setRedirectFragment(new PostsFragment());
                                Bundle paramBundle = new Bundle();
                                paramBundle.putString("member_sn", member_sn);
                                tempPost.setParamBundle(paramBundle);
                                contents.add(tempPost);
                                postRecyclerAdapter.notifyItemInserted(get_start_no + 1 + i);
                            } else if (post.getString("target_dc").equals("article")) {
                                Post tempPost = new Post().setArticle(post.getString("post_sn"), imageUrl, post.getString("title"), post.getString("member_name"), post.getString("like_sn"), post.getString("comment_sn"), Utils.getTimeLag(post.getString("created_datetime").substring(0, 19), getContext()), GlobalConstants.DOMAIN + post.getString("member_image_url"), post.getString("description"), post.getString("creator_sn"), post.getString("my_like_count"), post.getString("follow_sn"));
                                tempPost.setRedirectFragment(new PostsFragment());
                                Bundle paramBundle = new Bundle();
                                paramBundle.putString("member_sn", member_sn);
                                tempPost.setParamBundle(paramBundle);
                                contents.add(tempPost);
                                postRecyclerAdapter.notifyItemInserted(get_start_no + 1 + i);
                            }

                        }

                        get_start_no = contents.size();

                        if (contents.size() < postCount) {
                            loading = false;
                        } else {
                            Handler mHandler = new Handler();
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    postRecyclerAdapter.removeFooter();
                                    isEnd = true;
                                    postRecyclerAdapter.notifyItemRemoved(get_start_no + 2);
                                }
                            }, 500);
                        }

                    } catch (JSONException | ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
            studynotesHttpGetRequestTask.execute(url);
        }
    }

}
