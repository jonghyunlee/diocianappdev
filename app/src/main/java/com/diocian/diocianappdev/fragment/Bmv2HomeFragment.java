package com.diocian.diocianappdev.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.adapter.PortletMusicRecyclerAdapter;
import com.diocian.diocianappdev.adapter.PortletRecyclerAdapter;
import com.diocian.diocianappdev.adapter.PortletRecyclerAdapter2;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Bmv2HomeFragment extends Fragment {
    private ImageFetcher imageFetcher;

    private int get_start_no = 0;
    private boolean loading = false;

    private boolean isEnd = false;

    private View view;
    private ArrayList<Post> sources;
    private RecyclerView portletSourceRecyclerView;
    private PortletRecyclerAdapter portletSourceRecyclerAdapter;

    private ArrayList<Post> videos;
    private RecyclerView portletVideoRecyclerView;
    private PortletRecyclerAdapter2 portletVideoRecyclerAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageFetcher = new ImageFetcher(getActivity());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        System.gc();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_bmv2_home, container, false);

        portletSourceRecyclerView = (RecyclerView) view.findViewById(R.id.portlet_source_recyclerView);
        sources = new ArrayList<>();
        portletSourceRecyclerAdapter = new PortletRecyclerAdapter(sources, imageFetcher, getContext());
        portletSourceRecyclerView.setAdapter(portletSourceRecyclerAdapter);
        HttpRequest httpRequest = new HttpRequest(getContext());
        HttpRequest.HttpGetRequestTask sourceHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, Context context) {
                try {
                    JSONArray posts = jsonObject.getJSONArray("posts");
//                    if (posts.length() > 0) {
//                        view.findViewById(R.id.portlet_musics).setVisibility(View.VISIBLE);
//                    }
                    JSONObject post;
                    for (int i = 0; i < posts.length(); i++) {
                        post = posts.getJSONObject(i);
                        sources.add(new Post().setPostFromJSONObject(post));
                    }
                    portletSourceRecyclerAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        sourceHttpGetRequestTask.execute(GlobalConstants.DOMAIN + "/getPosts.do?parent_sn=7852&target_dc=source&get_start_no=0&get_count=20");

        portletVideoRecyclerView = (RecyclerView) view.findViewById(R.id.portlet_video_recyclerView);
        videos = new ArrayList<>();
        portletVideoRecyclerAdapter = new PortletRecyclerAdapter2(videos, imageFetcher, getContext());
        portletVideoRecyclerView.setAdapter(portletVideoRecyclerAdapter);
        HttpRequest httpRequest2 = new HttpRequest(getContext());
        HttpRequest.HttpGetRequestTask videoHttpGetRequestTask = httpRequest2.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, Context context) {
                try {
                    JSONArray posts = jsonObject.getJSONArray("posts");
//                    if (posts.length() > 0) {
//                        view.findViewById(R.id.portlet_musics).setVisibility(View.VISIBLE);
//                    }
                    JSONObject post;
                    for (int i = 0; i < posts.length(); i++) {
                        post = posts.getJSONObject(i);
                        videos.add(new Post().setPostFromJSONObject(post));
                    }
                    portletVideoRecyclerAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        videoHttpGetRequestTask.execute(GlobalConstants.DOMAIN + "/getPosts.do?master_sn=7852&material_dc=video&get_start_no=0&get_count=20");

        return view;
    }

}
