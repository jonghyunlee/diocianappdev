package com.diocian.diocianappdev.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

public class StudynoteFragment extends Fragment {

    private View view;
    private ImageFetcher imageFetcher;
    private Post studynote;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        imageFetcher = new ImageFetcher(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_studynote, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.detail_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab_logout);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).playMediaPlayer(studynote);
//                Snackbar.make(view, "Replace with your own detail action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
            }
        });

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        Map<String, String> parameterMap = new HashMap<>();
        String target_dc = "study%20note";
        parameterMap.put("target_dc", target_dc);
        Bundle bundle = this.getArguments();
        String post_sn = "";
        if (bundle != null) {
            post_sn = bundle.getString("post_sn");
        }
        parameterMap.put("post_sn", String.valueOf(post_sn));
        String url = GlobalConstants.DOMAIN + "/getPost.do?";
        for (String key : parameterMap.keySet()) {
            url += key + "=" + parameterMap.get(key) + "&";
        }
        HttpRequest httpRequest = new HttpRequest(getContext());
        HttpRequest.HttpGetRequestTask musicHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
            @SuppressLint("SetTextI18n")
            @Override
            public void callback(JSONObject jsonObject, Context context) {
                try {
                    JSONObject post = jsonObject.getJSONObject("post");

                    JSONArray images = jsonObject.getJSONArray("images");
                    JSONObject image;
                    String imageUrl;
                    int duration_min = (int) Math.floor(Double.parseDouble(post.getString("play_sec")) / 60);
                    int duration_sec = (int) Math.floor(Double.parseDouble(post.getString("play_sec")) % 60);
                    imageUrl = null;
                    for (int j = 0; j < images.length(); j++) {
                        image = images.getJSONObject(j);
                        if (post.getString("post_sn").equals(image.getString("target_sn"))) {
                            imageUrl = GlobalConstants.DOMAIN + image.getString("file_url");
                            break;
                        }
                    }

                    studynote = new Post().setStudynote(post.getString("post_sn"), imageUrl, post.getString("genre_name"), post.getString("job_name"), "$" + post.getString("price"), duration_min + ":" + duration_sec, GlobalConstants.DOMAIN + post.getString("image_file_url"), post.getString("title"), post.getString("member_name"), post.getString("like_sn"), post.getString("comment_sn"), Utils.getTimeLag(post.getString("created_datetime").substring(0, 19), getContext()), GlobalConstants.DOMAIN + post.getString("member_image_url"), GlobalConstants.DOMAIN + post.getString("audio_file_url"), post.getString("creator_sn"), post.getString("my_like_count"), post.getString("follow_sn"));

                    ImageView musicImage = (ImageView) view.findViewById(R.id.image);
                    TextView genre_name = (TextView) view.findViewById(R.id.genre_name);
                    TextView job_name = (TextView) view.findViewById(R.id.job_name);
                    ImageView wave_image = (ImageView) view.findViewById(R.id.wave_image);
                    TextView like_count = (TextView) view.findViewById(R.id.like_count);
                    TextView comment_count = (TextView) view.findViewById(R.id.comment_count);
                    TextView title = (TextView) view.findViewById(R.id.title);
                    TextView created_datetime = (TextView) view.findViewById(R.id.created_datetime);
                    TextView price = (TextView) view.findViewById(R.id.price);
                    TextView creator_name = (TextView) view.findViewById(R.id.creator_name);
                    ImageView creator_image = (ImageView) view.findViewById(R.id.creator_image);
                    TextView description = (TextView) view.findViewById(R.id.description);

                    if (imageUrl != null) {
                        if (!imageUrl.equals(GlobalConstants.DOMAIN)) {
                            imageFetcher.setImageToImageView(imageUrl, musicImage, 360);
                        }
                    }

                    if (!post.getString("job_name").equals("")) {
                        genre_name.setVisibility(View.GONE);
                        job_name.setVisibility(View.VISIBLE);
                    } else {
                        genre_name.setVisibility(View.VISIBLE);
                        job_name.setVisibility(View.GONE);
                    }

                    genre_name.setText(post.getString("genre_name"));
                    job_name.setText(post.getString("job_name"));

                    imageFetcher.setImageToImageView(GlobalConstants.DOMAIN + post.getString("image_file_url"), wave_image, 360);

                    like_count.setText(post.getString("like_sn"));
                    comment_count.setText(post.getString("comment_sn"));
                    title.setText(post.getString("title"));
                    created_datetime.setText(Utils.getTimeLag(post.getString("created_datetime").substring(0, 19), getContext()));
                    price.setText("$" + post.getString("price"));
                    creator_name.setText(post.getString("member_name"));
                    imageFetcher.setImageToImageView(GlobalConstants.DOMAIN + post.getString("member_image_url"), creator_image, 32);
                    description.setText(post.getString("description"));

                } catch (JSONException | ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        musicHttpGetRequestTask.execute(url);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_none, menu);
        Utils.showMenuNotificationCount(menu, getActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_notification) {
            ((MainActivity) getActivity()).viewNotifications();
            return true;
        } else if (id == android.R.id.home) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.popBackStackImmediate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
