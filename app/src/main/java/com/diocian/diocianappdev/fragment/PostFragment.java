package com.diocian.diocianappdev.fragment;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.HttpRequestByMultipart;
import com.diocian.diocianappdev.util.HttpRequestPost;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class PostFragment extends Fragment {

    AppCompatEditText editTextDescription;
    TextInputLayout textInputLayoutDescription;
    TextView textViewImageInfo;
    Uri imageFileUri = null;
    RadioGroup radioGroupPublicDc;
    String member_sn = "";

    ContentLoadingProgressBar contentLoadingProgressBarSend;
    Button buttonPost;
    ImageView imageViewPreview;
    String[] imagesArray;

    View view;

    public PostFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_post, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.app_bar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        String toolbar_title = "Add Article".toUpperCase();
        toolbar.setTitle(toolbar_title);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            member_sn = bundle.getString("member_sn");
        }
        imageViewPreview = (ImageView) view.findViewById(R.id.imageViewPreview);
        editTextDescription = (AppCompatEditText) view.findViewById(R.id.editTextDescription);
        textInputLayoutDescription = (TextInputLayout) view.findViewById(R.id.textInputLayoutDescription);
        textViewImageInfo = (TextView) view.findViewById(R.id.textViewImageInfo);

        radioGroupPublicDc = (RadioGroup) view.findViewById(R.id.radioGroupPublicDc);

        ImageButton imageButtonImagePreview = (ImageButton) view.findViewById(R.id.imageButtonImagePreview);

        textViewImageInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageFileUri != null) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setDataAndType(imageFileUri, "image/*");
                    startActivity(i);
                }
            }
        });
        imageButtonImagePreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageFileUri != null) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setDataAndType(imageFileUri, "image/*");
                    startActivity(i);
                }
            }
        });

        Button buttonSelectImage = (Button) view.findViewById(R.id.buttonSelectImage);
        buttonSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.checkPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE, 0)) {
                    showFileChooser("image/*", IMAGE_FILE_SELECT_CODE);
                }
            }
        });

        buttonPost = (Button) view.findViewById(R.id.buttonPost);
        buttonPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                String target_dc = "article";
                String description = editTextDescription.getText().toString();
                String public_dc;
                RadioButton checkedRadioButton = (RadioButton) view.findViewById(radioGroupPublicDc.getCheckedRadioButtonId());
                public_dc = checkedRadioButton.getTag().toString();

                /*
                if (imageFileUri != null) {

                    try {
                        int maxBufferSize = 2048;
                        InputStream fileInputStream = getActivity().getContentResolver().openInputStream(imageFileUri);
                        if (fileInputStream != null) {
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            int bytesAvailable = fileInputStream.available();
                            int bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            byte[] buffer = new byte[bufferSize];
                            int bytesRead = fileInputStream.read(buffer, 0, bufferSize);
//                            byte[] byteArray = new byte[0];
                            while (bytesRead > 0) {
                                byteArrayOutputStream.write(buffer, 0, bufferSize);
//                                byte[] byteArray2 = byteArrayOutputStream.toByteArray();
//                                byte[] byteArray3 = new byte[byteArray.length + byteArray2.length];
//                                System.arraycopy(byteArray, 0, byteArray3, 0, byteArray.length);
//                                System.arraycopy(byteArray2, 0, byteArray3, byteArray.length, byteArray2.length);
//                                byteArray = byteArray3;
                                bytesAvailable = fileInputStream.available();
                                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                            }
                            byte[] byteArray = byteArrayOutputStream.toByteArray();
                            String encoded = "data:image/jpeg;base64," + Base64.encodeToString(byteArray, Base64.DEFAULT);
//                            Log.d("TEST----->", "encoded : " + encoded);
                            imagesArray = new String[1];
                            imagesArray[0] = encoded;
                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                */
                if (imageFileUri != null) {

                }
                send(target_dc, description, public_dc, imagesArray);
            }
        });
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_none, menu);
        Utils.showMenuNotificationCount(menu, getActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_notification) {
            ((MainActivity) getActivity()).viewNotifications();
            return true;
        } else if (id == android.R.id.home) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.popBackStackImmediate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void send(String target_dc, String description, String public_dc, String[] imagesArray) {

        contentLoadingProgressBarSend = (ContentLoadingProgressBar) view.findViewById(R.id.contentLoadingProgressBarSend);
        contentLoadingProgressBarSend.setVisibility(View.VISIBLE);
        buttonPost.setVisibility(View.GONE);

        String imageBase64 = "";

        try {
//            description = URLEncoder.encode(description, "UTF-8");
            if (imagesArray != null && imagesArray.length > 0) {
                imageBase64 = URLEncoder.encode(imagesArray[0], "UTF-8");
//                imageBase64 = imagesArray[0];
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url = GlobalConstants.DOMAIN + "/setPost.do?target_dc=" + target_dc + "&description=" + description + "&public_dc=" + public_dc + "&imagesArray[]=" + imageBase64;

        HttpRequestPost httpRequest = new HttpRequestPost(getContext());
        HttpRequestPost.HttpGetRequestTask postHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequestPost.CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, Context context) {
                try {
                    contentLoadingProgressBarSend.setVisibility(View.GONE);
                    buttonPost.setVisibility(View.VISIBLE);
                    JSONObject result = jsonObject.getJSONObject("result");
                    if (result.getString("success_yn").equals("Y")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Post completed.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Bundle bundle = new Bundle();
                                bundle.putString("member_sn", member_sn);
//                                Utils.removeAndReplaceFragment((MainActivity) getActivity(), new PostFragment(), new MemberFragment(), bundle);
                                Utils.replaceFragment((MainActivity) getActivity(), new MemberFragment(), bundle);
                            }
                        });
                        builder.show();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Post failed. " + result.getString("message"));
                        builder.setPositiveButton("Ok", null);
                        builder.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        postHttpGetRequestTask.execute(url);

    }

    private static final int IMAGE_FILE_SELECT_CODE = 4;

    private void showFileChooser(String intentType, int fileTypeCode) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType(intentType);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), fileTypeCode);
        } catch (android.content.ActivityNotFoundException e) {
        }
    }

    final static long MAXIMUM_FILE_SIZE = 104857600;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case IMAGE_FILE_SELECT_CODE:
                if (resultCode == FragmentActivity.RESULT_OK) {
                    imageFileUri = data.getData();

                    ContentResolver contentResolver = getContext().getContentResolver();
                    Cursor cursor = contentResolver.query(imageFileUri, null, null, null, null);

                    if (cursor != null) {
                        int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                        cursor.moveToFirst();
                        String fileName = cursor.getString(nameIndex);
                        nameIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
                        cursor.moveToFirst();
                        long fileSize = Long.parseLong(cursor.getString(nameIndex));
                        cursor.close();

                        Log.v(GlobalConstants.LOG_TAG, "fileSize : " + fileSize);

                        if (fileSize > MAXIMUM_FILE_SIZE) {
                            textViewImageInfo.setTextSize(getResources().getDimension(R.dimen.font_size_body_shorter) / getResources().getDisplayMetrics().density);
                            textViewImageInfo.setTextColor(ContextCompat.getColor(getContext(), R.color.error));
                            textViewImageInfo.setText("File is too large. (Maximum 100MB)");
                            imageFileUri = null;
                        } else {
                            imagesArray = new String[0];
                            textViewImageInfo.setTextSize(getResources().getDimension(R.dimen.font_size_body) / getResources().getDisplayMetrics().density);
                            textViewImageInfo.setTextColor(ContextCompat.getColor(getContext(), R.color.BlackSecondary));
                            textViewImageInfo.setText(fileName + " / " + Math.round(fileSize / 1024 / 1024) + "MB");
                            imagesArray = new String[1];
                            imagesArray[0] = Utils.encodeBase64FromUri(getActivity(), Utils.rotateAndResizeImageFromUri2(getActivity(), imageFileUri, imageViewPreview, 300));
                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}