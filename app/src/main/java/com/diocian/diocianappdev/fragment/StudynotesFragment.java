package com.diocian.diocianappdev.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.listener.EndlessRecyclerViewScrollListener;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.adapter.StudynoteRecyclerAdapter;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class StudynotesFragment extends Fragment {
    private RecyclerView studynoteRecyclerView;
    private StudynoteRecyclerAdapter studynoteRecyclerAdapter;
    private ImageFetcher imageFetcher;
    private ArrayList<Post> studynotes = new ArrayList<>();
    private StaggeredGridLayoutManager studynoteLayoutManager;

    private int get_start_no = 0;
    private boolean loading = false;

    private boolean isEnd = false;

    private  String search_word;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageFetcher = new ImageFetcher(getActivity());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        studynoteLayoutManager.setSpanCount(Utils.calColumnCount(getContext()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        studynoteRecyclerView.removeAllViews();
//        studynoteRecyclerView = null;
//        System.gc();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_studynotes, container, false);

        search_word = null;
        MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity != null) {
            search_word = mainActivity.query;
        }
        if (search_word == null) {
            search_word = "";
        }

        studynoteRecyclerView = (RecyclerView) view.findViewById(R.id.studynotes_recyclerView);
        studynoteLayoutManager = new StaggeredGridLayoutManager(Utils.calColumnCount(getContext()), StaggeredGridLayoutManager.VERTICAL);
        studynoteRecyclerView.setLayoutManager(studynoteLayoutManager);
        studynoteRecyclerAdapter = new StudynoteRecyclerAdapter(studynotes, imageFetcher, getContext());
        if (isEnd) {
            studynoteRecyclerAdapter.removeFooter();
        }
        studynoteRecyclerView.setAdapter(studynoteRecyclerAdapter);
        studynoteRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(studynoteLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadMorePosts();
            }
        });

        if (studynotes.size() == 0) {
            loadMorePosts();
        }

        return view;
    }

    public void loadMorePosts() {
        if (!loading && !isEnd) {
            loading = true;
            Map<String, String> parameterMap = new HashMap<>();
            String target_dc = "study%20note";
            parameterMap.put("target_dc", target_dc);
            parameterMap.put("get_start_no", String.valueOf(get_start_no));
            int get_count = 12;
            parameterMap.put("get_count", String.valueOf(get_count));
            parameterMap.put("search_word", search_word);
            String url = GlobalConstants.DOMAIN + "/getPosts.do?";
            for (String key : parameterMap.keySet()) {
                url += key + "=" + parameterMap.get(key) + "&";
            }

            HttpRequest httpRequest = new HttpRequest(getContext());
            HttpRequest.HttpGetRequestTask studynotesHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                @Override
                public void callback(JSONObject jsonObject, Context context) {
                    try {
                        int postCount = jsonObject.getInt("postCount");
                        JSONArray posts = jsonObject.getJSONArray("posts");
                        JSONArray images = new JSONArray();
                        if(jsonObject.has("images")) {
                            images = jsonObject.getJSONArray("images");
                        }
                        JSONObject post, image;
                        String imageUrl;
                        int duration_min;
                        int duration_sec;
                        for (int i = 0; i < posts.length(); i++) {
                            post = posts.getJSONObject(i);
                            duration_min = (int) Math.floor(Double.parseDouble(post.getString("play_sec")) / 60);
                            duration_sec = (int) Math.floor(Double.parseDouble(post.getString("play_sec")) % 60);
                            imageUrl = null;
                            for (int j = 0; j < images.length(); j++) {
                                image = images.getJSONObject(j);
                                if (post.getString("post_sn").equals(image.getString("target_sn"))) {
                                    imageUrl = GlobalConstants.DOMAIN + image.getString("file_url");
                                    break;
                                }
                            }
                            studynotes.add(new Post().setStudynote(post.getString("post_sn"), imageUrl, post.getString("genre_name"), post.getString("job_name"), "$" + post.getString("price"), duration_min + ":" + duration_sec, GlobalConstants.DOMAIN + post.getString("image_file_url"), post.getString("title"), post.getString("member_name"), post.getString("like_sn"), post.getString("comment_sn"), Utils.getTimeLag(post.getString("created_datetime").substring(0, 19), getContext()), GlobalConstants.DOMAIN + post.getString("member_image_url"), GlobalConstants.DOMAIN + post.getString("audio_file_url"), post.getString("creator_sn"), post.getString("my_like_count"), post.getString("follow_sn")));
                            studynoteRecyclerAdapter.notifyItemInserted(get_start_no + 1 + i);
                        }

                        get_start_no = studynotes.size();

                        if (studynotes.size() < postCount) {
                            loading = false;
                        } else {
                            Handler mHandler = new Handler();
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    studynoteRecyclerAdapter.removeFooter();
                                    isEnd = true;
                                    studynoteRecyclerAdapter.notifyItemRemoved(get_start_no + 2);
                                }
                            }, 500);
                        }

                    } catch (JSONException | ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
            studynotesHttpGetRequestTask.execute(url);
        }
    }

}
