package com.diocian.diocianappdev.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONException;

import java.net.URISyntaxException;
import java.util.List;

public class DanalFragment extends Fragment {

    private final Handler handler = new Handler();

    public static final String TAG = "DanalPayCardSample";
    public static final String APPSCHEME = "danalpaycardsample://";

    private static final String target_url_prefix = "www.diocian.com";
    private Context context;
    private WebView webViewFacebookLoginPop;
    private FrameLayout frameLayoutCloudPayment;
    private String aid;
    private String am;
    private String target_dc;
    private String target_sn;
    private String post_target_dc;

    View view;

    public DanalFragment() {
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_cloudpayment, container, false);

        Bundle bundle = this.getArguments();
        aid = "";
        am = "";
        target_dc = "";
        target_sn = "";
        post_target_dc = "";
        if (bundle != null) {
            aid = bundle.getString("aid");
            am = bundle.getString("am");
            target_dc = bundle.getString("target_dc");
            target_sn = bundle.getString("target_sn");
            post_target_dc = bundle.getString("post_target_dc");
        }

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);

        frameLayoutCloudPayment = (FrameLayout) view.findViewById(R.id.frameLayoutCloudPayment);
        WebView webViewCloudPayment = (WebView) view.findViewById(R.id.webViewCloudPayment);
        if (webViewCloudPayment != null) {
            webViewCloudPayment.clearCache(true);
            webViewCloudPayment.clearHistory();
            clearCookies(getActivity());
            WebSettings webSettingsFacebookLogin = webViewCloudPayment.getSettings();
            webSettingsFacebookLogin.setJavaScriptEnabled(true);
            webSettingsFacebookLogin.setAppCacheEnabled(true);
            webSettingsFacebookLogin.setJavaScriptCanOpenWindowsAutomatically(true);
            webSettingsFacebookLogin.setSupportMultipleWindows(true);

            CloudPaymentJavascriptInterface facebookLoginJavascriptInterface = new CloudPaymentJavascriptInterface(getActivity(), webViewFacebookLoginPop);
            webViewCloudPayment.addJavascriptInterface(facebookLoginJavascriptInterface, "DiocianApp");

            webViewCloudPayment.setWebViewClient(new FacebookWebViewClient());
            webViewCloudPayment.setWebChromeClient(new FacebookWebChromeClient());
//            String cloudPaymentUrl = "/app/payment/cloudpayment/form.jsp?aid=" + aid + "&am=" + am + "&formtype=Android&target_dc=" + target_dc + "&target_sn=" + target_sn + "&post_target_dc=" + post_target_dc + "&creator_sn=" + ((MainActivity) getActivity()).getPreferences("member_sn");
            String cloudPaymentUrl = "/danal/mOrder.jsp?aid=" + aid + "&am=" + am + "&formtype=Android&target_dc=" + target_dc + "&target_sn=" + target_sn + "&post_target_dc=" + post_target_dc + "&creator_sn=" + ((MainActivity) getActivity()).getPreferences("member_sn");
            webViewCloudPayment.loadUrl(GlobalConstants.DOMAIN + cloudPaymentUrl);
        }
        context = getActivity();

        return view;
    }

    @SuppressWarnings("deprecation")
    public static void clearCookies(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }

    private class FacebookWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            String host = Uri.parse(url).getHost();
            if (host.equals(target_url_prefix)) {
                if (webViewFacebookLoginPop != null) {
                    webViewFacebookLoginPop.setVisibility(View.GONE);
                    frameLayoutCloudPayment.removeView(webViewFacebookLoginPop);
                    webViewFacebookLoginPop = null;
                }
                return false;
            }

            if (url != null && !url.equals("about:blank")) {
                if (url.startsWith("http://") || url.startsWith("https://")) {
                    if (url.contains("http://market.android.com") ||
                            url.contains("http://m.ahnlab.com/kr/site/download") ||
                            url.endsWith(".apk")) {
                        return urlSchemeProc(view, url);
                    } else {
                        view.loadUrl(url);
                        return false;
                    }
                } else if (url.startsWith("mailto:")) {
                    return false;
                } else if (url.startsWith("tel:")) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                    startActivity(intent);
                    return true;
                } else {
                    return urlSchemeProc(view, url);
                }
            }

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            return true;
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            //super.onReceivedSslError(view, handler, error);
        }
    }

    class FacebookWebChromeClient extends WebChromeClient {

        @SuppressLint("SetJavaScriptEnabled")
        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
            webViewFacebookLoginPop = new WebView(context);
            webViewFacebookLoginPop.setVerticalScrollBarEnabled(false);
            webViewFacebookLoginPop.setHorizontalScrollBarEnabled(false);

            CloudPaymentJavascriptInterface facebookLoginJavascriptInterface = new CloudPaymentJavascriptInterface(getActivity(), webViewFacebookLoginPop);
            webViewFacebookLoginPop.addJavascriptInterface(facebookLoginJavascriptInterface, "DiocianApp");

            webViewFacebookLoginPop.setWebViewClient(new FacebookWebViewClient());
            webViewFacebookLoginPop.getSettings().setJavaScriptEnabled(true);
            webViewFacebookLoginPop.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            frameLayoutCloudPayment.addView(webViewFacebookLoginPop);
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(webViewFacebookLoginPop);
            resultMsg.sendToTarget();
            return true;
        }

        @Override
        public void onCloseWindow(WebView window) {
            Log.d("onCloseWindow", "called");
        }

    }

    private class DanalCreditBridge {

        @JavascriptInterface
        public void runISP(final String argUrl, final String packageName) {
            handler.post(new Runnable() {
                public void run() {
                    boolean install = true;
                    String strUrl;

                    PackageManager packMgr = getActivity().getPackageManager();
                    List<ApplicationInfo> installedAppList = packMgr.getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES);

                    for (ApplicationInfo appInfo : installedAppList) {
                        if ((appInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                            if (appInfo.packageName.indexOf(packageName) > -1) {
                                install = false;
                                break;
                            }
                        }
                    }

                    strUrl = (install == true)
                            ? "market://details?id=" + packageName
                            : argUrl;

                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUrl));
                    startActivity(intent);
                }
            });
        }
    }

    private boolean urlSchemeProc(WebView view, String url) {

        if (url != null &&
                (
                        url.contains("cloudpay")
                                || url.contains("hanaansim")
                                || url.contains("citispayapp")
                                || url.contains("citicardapp")
                                || url.contains("mvaccine")
                                || url.contains("com.TouchEn.mVaccine.webs")
                                || url.contains("market://")
                                || url.contains("com.ahnlab.v3mobileplus")
                                || url.contains("droidxantivirus")
                                || url.contains("v3mobile")
                                || url.endsWith(".apk")
                                || url.contains("market://")
                                || url.contains("ansimclick")
                                || url.contains("market://details?id=com.shcard.smartpay")
                                || url.contains("shinhan-sr-ansimclick://")
                                || url.contains("http://m.ahnlab.com/kr/site/download")
                                || url.contains("com.lotte.lottesmartpay")
                                || url.startsWith("lottesmartpay://")
                                || url.contains("http://market.android.com")
                                || url.contains("vguard")
                                || url.contains("smhyundaiansimclick://")
                                || url.contains("smshinhanansimclick://")
                                || url.contains("smshinhancardusim://")
                                || url.contains("smartwall://")
                                || url.contains("appfree://")
                                || url.startsWith("kb-acp://")
                                || url.startsWith("intent://")
                                || url.startsWith("ahnlabv3mobileplus")
                )) {
            try {
                Intent intent = null;
                try {
                    intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                    Log.i(TAG, "intent getScheme +++===>" + intent.getScheme());
                    Log.i(TAG, "intent getDataString +++===>" + intent.getDataString());
                } catch (URISyntaxException ex) {
                    Log.e("Browser", "Bad URI " + url + ":" + ex.getMessage());
                    return false;
                }
                //chrome 버젼 방식 : 2014.01 추가
                if (url.startsWith("intent")) { // chrome 버젼 방식
                    // 앱설치 체크를 합니다.
                    if (getActivity().getPackageManager().resolveActivity(intent, 0) == null) {
                        String packagename = intent.getPackage();
                        if (packagename != null) {
                            Uri uri = Uri.parse("market://search?q=pname:" + packagename);
                            intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                            return true;
                        }
                    }

                    Uri uri = Uri.parse(intent.getDataString());
                    intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);

                } else { // 구 방식
                    Uri uri = Uri.parse(url);
                    intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
            } catch (ActivityNotFoundException e) {
                Log.e("error ===>", e.getMessage());
                e.printStackTrace();
                return false;
            }
        } else {
            view.loadUrl(url);
            return false;
        }
        return true;

    }

    public class CloudPaymentJavascriptInterface {

        private Activity activity;
        private WebView webView;

        public CloudPaymentJavascriptInterface(Activity activity, WebView webView) {
            this.activity = activity;
            this.webView = webView;
        }

        @JavascriptInterface
        public void cloudPaymentCallback(String target_dc, String target_sn) throws JSONException {
//            Snackbar.make(webView, jobCode + " : " + message, Snackbar.LENGTH_LONG).setAction("Action", null).show();
            Log.d(GlobalConstants.LOG_TAG, target_dc+"/"+target_sn);
            if (target_dc.equals("music")) {
//                ((MainActivity) getActivity()).viewMusic(target_sn);
                Bundle bundle = new Bundle();
                bundle.putString("post_sn", target_sn);
                bundle.putString("buyYn", "Y");
                Utils.removeAndReplaceFragment((MainActivity) getActivity(), new DanalFragment(), new MusicFragment(), bundle);
            } else if (target_dc.equals("market")) {
                Bundle bundle = new Bundle();
                bundle.putString("post_sn", target_sn);
                bundle.putString("buyYn", "Y");
                Utils.removeAndReplaceFragment((MainActivity) getActivity(), new DanalFragment(), new MarketFragment(), bundle);
            }
        }

        @JavascriptInterface
        public void showMessageToSnackbar(String message) {
//            Snackbar.make(webView, message, Snackbar.LENGTH_LONG).setAction("Action", null).show();

        }

    }

}