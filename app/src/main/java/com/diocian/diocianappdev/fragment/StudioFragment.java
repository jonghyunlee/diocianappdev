package com.diocian.diocianappdev.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.adapter.ContentRecyclerAdapter;
import com.diocian.diocianappdev.listener.EndlessRecyclerViewScrollListener;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class StudioFragment extends Fragment {

    private View view;
    private ImageFetcher imageFetcher;

    private RecyclerView contentRecyclerView;
    private ContentRecyclerAdapter contentRecyclerAdapter;
    private ArrayList<Post> contents = new ArrayList<>();
    private StaggeredGridLayoutManager contentLayoutManager;

    private int get_start_no = 0;
    private boolean loading = false;

    private boolean isEnd = false;

    String post_sn = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        imageFetcher = new ImageFetcher(getActivity());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        contentLayoutManager.setSpanCount(Utils.calColumnCount(getContext()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        contentRecyclerView.removeAllViews();
//        contentRecyclerView = null;
//        System.gc();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_studio, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.detail_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab_logout);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own detail action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
            }
        });

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        Map<String, String> parameterMap = new HashMap<>();
        String target_dc = "studio";
        parameterMap.put("target_dc", target_dc);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            post_sn = bundle.getString("post_sn");
        }
        parameterMap.put("post_sn", String.valueOf(post_sn));
        String url = GlobalConstants.DOMAIN + "/getPost.do?";
        for (String key : parameterMap.keySet()) {
            url += key + "=" + parameterMap.get(key) + "&";
        }
        HttpRequest httpRequest = new HttpRequest(getContext());
        HttpRequest.HttpGetRequestTask musicHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, Context context) {
                try {
                    JSONObject post = jsonObject.getJSONObject("post");
                    JSONArray images = jsonObject.getJSONArray("images");
                    JSONObject image;
                    String imageUrl;
                    imageUrl = null;
                    for (int j = 0; j < images.length(); j++) {
                        image = images.getJSONObject(j);
                        if (post.getString("post_sn").equals(image.getString("target_sn"))) {
                            imageUrl = GlobalConstants.DOMAIN + image.getString("file_url");
                            break;
                        }
                    }

                    ImageView musicImage = (ImageView) view.findViewById(R.id.image);
                    TextView genre_name = (TextView) view.findViewById(R.id.genre_name);
                    TextView job_name = (TextView) view.findViewById(R.id.job_name);
                    TextView like_count = (TextView) view.findViewById(R.id.like_count);
                    TextView comment_count = (TextView) view.findViewById(R.id.comment_count);
                    TextView title = (TextView) view.findViewById(R.id.title);
                    TextView created_datetime = (TextView) view.findViewById(R.id.created_datetime);
//                    TextView creator_name = (TextView) view.findViewById(R.id.creator_name);
//                    ImageView creator_image = (ImageView) view.findViewById(R.id.creator_image);
//                    TextView description = (TextView) view.findViewById(R.id.description);

                    imageFetcher.setImageToImageView(imageUrl, musicImage, 360);
                    genre_name.setText(post.getString("genre_name"));
                    job_name.setText(post.getString("job_name").replaceAll(",", " "));
                    like_count.setText(post.getString("like_sn"));
                    comment_count.setText(post.getString("comment_sn"));
                    title.setText(post.getString("title"));
                    created_datetime.setText(Utils.getTimeLag(post.getString("created_datetime").substring(0, 19), getContext()));
//                    creator_name.setText(post.getString("member_name"));
//                    imageFetcher.setImageToImageView(GlobalConstants.DOMAIN + post.getString("member_image_url"), creator_image, 32);
//                    description.setText(post.getString("description"));

                } catch (JSONException | ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        musicHttpGetRequestTask.execute(url);

        contentRecyclerView = (RecyclerView) view.findViewById(R.id.contents_recyclerView);
        contentLayoutManager = new StaggeredGridLayoutManager(Utils.calColumnCount(getContext()), StaggeredGridLayoutManager.VERTICAL);
        contentRecyclerView.setLayoutManager(contentLayoutManager);
        contentRecyclerAdapter = new ContentRecyclerAdapter(contents, imageFetcher, getContext());
        if (isEnd) {
            contentRecyclerAdapter.removeFooter();
        }
        contentRecyclerView.setAdapter(contentRecyclerAdapter);
        contentRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(contentLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadMorePosts();
            }
        });

        if (contents.size() == 0) {
            loadMorePosts();
        }

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_none, menu);
        Utils.showMenuNotificationCount(menu, getActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_notification) {
            ((MainActivity) getActivity()).viewNotifications();
            return true;
        } else if (id == android.R.id.home) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.popBackStackImmediate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void loadMorePosts() {
        if (!loading && !isEnd) {
            loading = true;
            Map<String, String> parameterMap = new HashMap<>();
            parameterMap.put("parent_sn", post_sn);
            parameterMap.put("get_start_no", String.valueOf(get_start_no));
            int get_count = 12;
            parameterMap.put("get_count", String.valueOf(get_count));
            String url = GlobalConstants.DOMAIN + "/getPosts.do?";
            for (String key : parameterMap.keySet()) {
                url += key + "=" + parameterMap.get(key) + "&";
            }

            HttpRequest httpRequest = new HttpRequest(getContext());
            HttpRequest.HttpGetRequestTask studynotesHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                @Override
                public void callback(JSONObject jsonObject, Context context) {
                    try {
                        int postCount = jsonObject.getInt("postCount");
                        JSONArray posts = jsonObject.getJSONArray("posts");
                        JSONArray images = jsonObject.getJSONArray("images");
                        JSONObject post, image;
                        String imageUrl;
                        int duration_min;
                        int duration_sec;
                        for (int i = 0; i < posts.length(); i++) {
                            post = posts.getJSONObject(i);
                            if (post.getString("target_dc").equals("study note")) {
                                duration_min = (int) Math.floor(Double.parseDouble(post.getString("play_sec")) / 60);
                                duration_sec = (int) Math.floor(Double.parseDouble(post.getString("play_sec")) % 60);
                                imageUrl = null;
                                for (int j = 0; j < images.length(); j++) {
                                    image = images.getJSONObject(j);
                                    if (post.getString("post_sn").equals(image.getString("target_sn"))) {
                                        imageUrl = GlobalConstants.DOMAIN + image.getString("file_url");
                                        break;
                                    }
                                }
                                contents.add(new Post().setStudynote(post.getString("post_sn"), imageUrl, post.getString("genre_name"), post.getString("job_name"), "$" + post.getString("price"), duration_min + ":" + duration_sec, GlobalConstants.DOMAIN + post.getString("image_file_url"), post.getString("title"), post.getString("member_name"), post.getString("like_sn"), post.getString("comment_sn"), Utils.getTimeLag(post.getString("created_datetime").substring(0, 19), getContext()), GlobalConstants.DOMAIN + post.getString("member_image_url"), GlobalConstants.DOMAIN + post.getString("audio_file_url"), post.getString("creator_sn"), post.getString("my_like_count"), post.getString("follow_sn")));
                                contentRecyclerAdapter.notifyItemInserted(get_start_no + 1 + i);
                            }
                        }

                        get_start_no = contents.size();

                        if (contents.size() < postCount) {
                            loading = false;
                        } else {
                            Handler mHandler = new Handler();
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    contentRecyclerAdapter.removeFooter();
                                    isEnd = true;
                                    contentRecyclerAdapter.notifyItemRemoved(get_start_no + 2);
                                }
                            }, 500);
                        }

                    } catch (JSONException | ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
            studynotesHttpGetRequestTask.execute(url);
        }
    }

}
