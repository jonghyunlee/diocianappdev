package com.diocian.diocianappdev.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

public class MarketFragment extends Fragment {

    private View view;
    private ImageFetcher imageFetcher;
    FloatingActionButton fab;
    private String aid;
    private String am;
    private String target_dc;
    private String target_sn;
    private String post_target_dc;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        imageFetcher = new ImageFetcher(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_market, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.detail_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        fab = (FloatingActionButton) view.findViewById(R.id.fab_logout);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own detail action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        Map<String, String> parameterMap = new HashMap<>();
        String target_dc = "market";
        parameterMap.put("target_dc", target_dc);
        Bundle bundle = this.getArguments();
        String post_sn = "";
        if (bundle != null) {
            post_sn = bundle.getString("post_sn");
        }
        parameterMap.put("post_sn", String.valueOf(post_sn));
        String url = GlobalConstants.DOMAIN + "/getPost.do?";
        for (String key : parameterMap.keySet()) {
            url += key + "=" + parameterMap.get(key) + "&";
        }
        HttpRequest httpRequest = new HttpRequest(getContext());
        HttpRequest.HttpGetRequestTask musicHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, Context context) {
                try {
                    JSONObject post = jsonObject.getJSONObject("post");
                    JSONArray images = jsonObject.getJSONArray("images");
                    JSONObject image;
                    String imageUrl;
                    imageUrl = null;
                    for (int j = 0; j < images.length(); j++) {
                        image = images.getJSONObject(j);
                        if (post.getString("post_sn").equals(image.getString("target_sn"))) {
                            imageUrl = GlobalConstants.DOMAIN + image.getString("file_url");
                            break;
                        }
                    }

                    ImageView musicImage = (ImageView) view.findViewById(R.id.image);
                    TextView div_name = (TextView) view.findViewById(R.id.div_name);
                    TextView like_count = (TextView) view.findViewById(R.id.like_count);
                    TextView comment_count = (TextView) view.findViewById(R.id.comment_count);
                    TextView title = (TextView) view.findViewById(R.id.title);
                    TextView quantity = (TextView) view.findViewById(R.id.quantity);
                    TextView price = (TextView) view.findViewById(R.id.price);
                    TextView creator_name = (TextView) view.findViewById(R.id.creator_name);
                    ImageView creator_image = (ImageView) view.findViewById(R.id.creator_image);
                    TextView description = (TextView) view.findViewById(R.id.description);

                    imageFetcher.setImageToImageView(imageUrl, musicImage, 360);
                    div_name.setText(post.getString("div_name"));
                    like_count.setText(post.getString("like_sn"));
                    comment_count.setText(post.getString("comment_sn"));
                    title.setText(post.getString("title"));
                    quantity.setText(post.getString("quantity_count") + " / " + post.getString("quantity_count"));

                    String price2 = "";
                    String location = ((MainActivity) getActivity()).getPreferences("location");
                    if (location.equals("US")) {
                        price2 = "＄" + post.getString("US_price");
                    } else if (location.equals("JP")) {
                        price2 = "￥" + post.getString("JP_price");

                        am = post.getString("JP_price");
                        target_sn = post.getString("post_sn");;

                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Bundle bundle = new Bundle();
                                bundle.putString("aid", "111188");
                                bundle.putString("am", am);
                                bundle.putString("target_dc", "market");
                                bundle.putString("target_sn", target_sn);
                                bundle.putString("post_target_dc", "market");
                                Utils.replaceFragment((MainActivity) getActivity(), new CloudPaymentFragment(), bundle);
                            }
                        });

                    } else if (location.equals("KR")) {
                        price2 = "￦" + post.getString("KR_price");
                        Utils.formatCurrency(post.getString("KR_price"));

                        am = post.getString("KR_price");
                        target_sn = post.getString("post_sn");;

                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Bundle bundle = new Bundle();
                                bundle.putString("aid", "111188");
                                bundle.putString("am", am);
                                bundle.putString("target_dc", "market");
                                bundle.putString("target_sn", target_sn);
                                bundle.putString("post_target_dc", "market");
                                Utils.replaceFragment((MainActivity) getActivity(), new DanalFragment(), bundle);
                            }
                        });
                    } else if (location.equals("VN")) {
                        price2 = "＄" + post.getString("VN_price");
                    }

                    price.setText(price2);
                    creator_name.setText(post.getString("member_name"));
                    imageFetcher.setImageToImageView(GlobalConstants.DOMAIN + post.getString("member_image_url"), creator_image, 32);
                    description.setText(post.getString("description"));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        musicHttpGetRequestTask.execute(url);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_none, menu);
        Utils.showMenuNotificationCount(menu, getActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_notification) {
            ((MainActivity) getActivity()).viewNotifications();
            return true;
        } else if (id == android.R.id.home) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.popBackStackImmediate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
