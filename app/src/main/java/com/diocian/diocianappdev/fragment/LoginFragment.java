package com.diocian.diocianappdev.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class LoginFragment extends Fragment {

    AppCompatEditText edit_text_email;
    AppCompatEditText edit_text_password;
    TextInputLayout textInputLayoutEmail;
    TextInputLayout textInputLayoutPassword;
    View view;

    public LoginFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_login, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.app_bar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        String toolbar_title = getString(R.string.login).toUpperCase();
        toolbar.setTitle(toolbar_title);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Button buttonLogInByFacebook = (Button) view.findViewById(R.id.buttonLogInByFacebook);
        buttonLogInByFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).viewFacebookLogin("logIn");
            }
        });

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
            Utils.setBackgroundTintUnderLollipop(getActivity(), buttonLogInByFacebook, R.color.facebook);

        edit_text_email = (AppCompatEditText) view.findViewById(R.id.edit_text_email);
        edit_text_password = (AppCompatEditText) view.findViewById(R.id.edit_text_password);

        textInputLayoutEmail = (TextInputLayout) view.findViewById(R.id.textInputLayoutEmail);
        textInputLayoutPassword = (TextInputLayout) view.findViewById(R.id.textInputLayoutPassword);

        // TODO : Login 정상 입력 시 입력 오류 메시지 삭제
        // TODO : Login Error Message 변수로 변경
        // TODO : Login Error Message 다국어

        Button btn_login_with_email = (Button) view.findViewById(R.id.btn_login_with_email);
        btn_login_with_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                String member_dc = "E";
                String member_id = edit_text_email.getText().toString();
                String member_pw = edit_text_password.getText().toString();

                textInputLayoutEmail.setErrorEnabled(false);
                textInputLayoutPassword.setErrorEnabled(false);

                if (member_id.equals("")) {
                    textInputLayoutEmail.setErrorEnabled(true);
                    textInputLayoutEmail.setError("Email is required.");
                } else if (member_pw.equals("")) {
                    textInputLayoutPassword.setErrorEnabled(true);
                    textInputLayoutPassword.setError("Password is required.");
                } else {
                    login(member_dc, member_id, member_pw);
                }

            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_none, menu);
        Utils.showMenuNotificationCount(menu, getActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_notification) {
            ((MainActivity) getActivity()).viewNotifications();
            return true;
        } else if (id == android.R.id.home) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.popBackStackImmediate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void login(String member_dc, String member_id, String member_pw) {
        String fcm_token = "";
        try {
            member_id = URLEncoder.encode(member_id, "UTF-8");
            member_pw = URLEncoder.encode(member_pw, "UTF-8");
            fcm_token = URLEncoder.encode(Utils.getFcmToken(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url = GlobalConstants.DOMAIN + "/login.do?member_dc=" + member_dc + "&member_id=" + member_id + "&member_pw=" + member_pw + "&fcm_token=" + fcm_token;

        HttpRequest httpRequest = new HttpRequest(getContext());
        HttpRequest.HttpGetRequestTask studiosHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, Context context) {
                try {
                    JSONObject result = jsonObject.getJSONObject("result");
                    if (result.getString("success_yn").equals("Y")) {
                        JSONObject member = jsonObject.getJSONObject("member");

                        ((MainActivity) getActivity()).removeAllPreferences();
                        ((MainActivity) getActivity()).savePreferences("member_sn", member.getString("member_sn"));
                        ((MainActivity) getActivity()).savePreferences("member_name", member.getString("member_name"));
                        ((MainActivity) getActivity()).savePreferences("member_summary", member.getString("member_summary"));
                        ((MainActivity) getActivity()).savePreferences("member_image_url", member.getString("member_image_url"));
                        ((MainActivity) getActivity()).savePreferences("home_image_url", member.getString("home_image_url"));
                        ((MainActivity) getActivity()).savePreferences("fcm_token", member.getString("fcm_token"));
                        ((MainActivity) getActivity()).savePreferences("session_id", member.getJSONObject("loginAuth").getString("session_id"));
                        ((MainActivity) getActivity()).savePreferences("session_id_raw", jsonObject.getString("session_id"));
                        ((MainActivity) getActivity()).savePreferences("client_auth_cd", member.getJSONObject("loginAuth").getString("client_auth_cd"));
                        ((MainActivity) getActivity()).isPlaylist = false;
                        ((MainActivity) getActivity()).setLoginInfo();

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Welcome! " + member.getString("member_name") + ".");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                fragmentManager.popBackStackImmediate();
                            }
                        });
                        builder.show();

                    } else {

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Login failed.");
                        builder.setPositiveButton("Ok", null);
                        builder.show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        studiosHttpGetRequestTask.execute(url);

    }
}