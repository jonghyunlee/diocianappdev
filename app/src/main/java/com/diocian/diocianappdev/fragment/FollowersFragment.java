package com.diocian.diocianappdev.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.adapter.MemberRecyclerAdapter;
import com.diocian.diocianappdev.listener.EndlessRecyclerViewScrollListener;
import com.diocian.diocianappdev.model.Member;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FollowersFragment extends Fragment {

    private View view;
    private ImageFetcher imageFetcher;

    private RecyclerView recyclerViewMembers;
    private MemberRecyclerAdapter memberRecyclerAdapter;
    private ArrayList<Member> members = new ArrayList<>();
    private StaggeredGridLayoutManager layoutManagerMembers;

    private int get_start_no = 0;
    private boolean loading = false;

    private boolean isEnd = false;

    String member_sn = "";
    int followerCount = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageFetcher = new ImageFetcher(getActivity());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        layoutManagerMembers.setSpanCount(Utils.calColumnCount(getContext()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        recyclerViewMembers.removeAllViews();
//        recyclerViewMembers = null;
//        System.gc();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_members, container, false);

        final Bundle bundle = this.getArguments();
        if (bundle != null) {
            member_sn = bundle.getString("member_sn");
            followerCount = bundle.getInt("followerCount");
        }

        // TODO : Article 삭제 기능 추가.
        // TODO : Article 수정 기능 추가.

        recyclerViewMembers = (RecyclerView) view.findViewById(R.id.recyclerViewMembers);
        layoutManagerMembers = new StaggeredGridLayoutManager(Utils.calColumnCount(getContext()), StaggeredGridLayoutManager.VERTICAL);
        recyclerViewMembers.setLayoutManager(layoutManagerMembers);
        Bundle bundle_param = new Bundle();
        bundle_param.putString("member_sn", member_sn);
        bundle_param.putInt("tabNo", 1);
        memberRecyclerAdapter = new MemberRecyclerAdapter(members, imageFetcher, getContext(), new MemberFragment(), bundle_param);
        if (isEnd) {
            memberRecyclerAdapter.removeFooter();
        }
        recyclerViewMembers.setAdapter(memberRecyclerAdapter);
        recyclerViewMembers.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManagerMembers) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadMorePosts();
            }
        });

        if (members.size() == 0) {
            loadMorePosts();
        }

        return view;
    }

    public void loadMorePosts() {
        if (!loading && !isEnd) {
            loading = true;
            Map<String, String> parameterMap = new HashMap<>();
            parameterMap.put("get_start_no", String.valueOf(get_start_no));
            int get_count = 12;
            parameterMap.put("get_count", String.valueOf(get_count));
            parameterMap.put("member_sn", member_sn);
            String url = GlobalConstants.DOMAIN + "/getFollowers.do?";
            for (String key : parameterMap.keySet()) {
                url += key + "=" + parameterMap.get(key) + "&";
            }

            HttpRequest httpRequest = new HttpRequest(getContext());
            HttpRequest.HttpGetRequestTask studynotesHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                @Override
                public void callback(JSONObject jsonObject, Context context) {
                    try {
//                        int postCount = jsonObject.getInt("postCount");
                        JSONArray tempMembers = jsonObject.getJSONArray("followers");
                        JSONObject member;
                        for (int i = 0; i < tempMembers.length(); i++) {
                            member = tempMembers.getJSONObject(i);
//                            duration_min = (int) Math.floor(Double.parseDouble(post.getString("play_sec")) / 60);
//                            duration_sec = (int) Math.floor(Double.parseDouble(post.getString("play_sec")) % 60);
                            Member tempMember = new Member().setMember(member.getString("member_sn"), member.getString("member_name"), GlobalConstants.DOMAIN + member.getString("member_image_url"), member.getString("member_summary"), member.getString("creator_sn"), member.getString("follow_sn"));
//                            tempPost.setRedirectFragment(new FollowersFragment());
//                            Bundle paramBundle = new Bundle();
//                            paramBundle.putString("member_sn", member_sn);
//                            tempPost.setParamBundle(paramBundle);
                            members.add(tempMember);
                            memberRecyclerAdapter.notifyItemInserted(get_start_no + 1 + i);
                        }

                        get_start_no = members.size();



                        if (members.size() < followerCount) {
                            loading = false;
                        } else {
                            Handler mHandler = new Handler();
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    memberRecyclerAdapter.removeFooter();
                                    isEnd = true;
                                    memberRecyclerAdapter.notifyItemRemoved(get_start_no + 2);
                                }
                            }, 500);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            studynotesHttpGetRequestTask.execute(url);
        }
    }

}
