package com.diocian.diocianappdev.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

public class EntryFragment extends Fragment {

    private View view;
    private ImageFetcher imageFetcher;
    private String youtube_video_id;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        imageFetcher = new ImageFetcher(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_video, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.detail_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab_logout);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(Intent.ACTION_VIEW);
                Uri u = Uri.parse("https://www.youtube.com/watch?v=" +youtube_video_id);
                i.setData(u);
                startActivity(i);
            }
        });

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        Map<String, String> parameterMap = new HashMap<>();
        String target_dc = "entry";
        parameterMap.put("target_dc", target_dc);
        Bundle bundle = this.getArguments();
        String post_sn = "";
        if (bundle != null) {
            post_sn = bundle.getString("post_sn");
        }
        parameterMap.put("post_sn", String.valueOf(post_sn));
        String url = GlobalConstants.DOMAIN + "/getPost.do?";
        for (String key : parameterMap.keySet()) {
            url += key + "=" + parameterMap.get(key) + "&";
        }
        HttpRequest httpRequest = new HttpRequest(getContext());
        HttpRequest.HttpGetRequestTask musicHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, Context context) {
                try {
                    JSONObject post = jsonObject.getJSONObject("post");

                    ImageView musicImage = (ImageView) view.findViewById(R.id.image);
                    TextView genre_name = (TextView) view.findViewById(R.id.genre_name);
                    TextView like_count = (TextView) view.findViewById(R.id.like_count);
                    TextView comment_count = (TextView) view.findViewById(R.id.comment_count);
                    TextView title = (TextView) view.findViewById(R.id.title);
                    TextView created_datetime = (TextView) view.findViewById(R.id.created_datetime);
                    TextView creator_name = (TextView) view.findViewById(R.id.creator_name);
                    ImageView creator_image = (ImageView) view.findViewById(R.id.creator_image);
                    TextView description = (TextView) view.findViewById(R.id.description);

                    youtube_video_id = post.getString("youtube_video_id");
                    imageFetcher.setImageToImageView("http://img.youtube.com/vi/" + post.getString("youtube_video_id") + "/0.jpg", musicImage, 360);
                    genre_name.setText(post.getString("genre_name"));
                    like_count.setText(post.getString("like_sn"));
                    comment_count.setText(post.getString("comment_sn"));
                    title.setText(post.getString("title"));
                    created_datetime.setText(Utils.getTimeLag(post.getString("created_datetime").substring(0, 19), getContext()));
                    creator_name.setText(post.getString("member_name"));
                    imageFetcher.setImageToImageView(GlobalConstants.DOMAIN + post.getString("member_image_url"), creator_image, 32);
                    description.setText(post.getString("description"));

                } catch (JSONException | ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        musicHttpGetRequestTask.execute(url);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_none, menu);
        Utils.showMenuNotificationCount(menu, getActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_notification) {
            ((MainActivity) getActivity()).viewNotifications();
            return true;
        } else if (id == android.R.id.home) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.popBackStackImmediate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
