package com.diocian.diocianappdev.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.adapter.NotificationRecyclerAdapter;
import com.diocian.diocianappdev.listener.EndlessRecyclerViewScrollListener;
import com.diocian.diocianappdev.model.Notification;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NotificationsFragment extends Fragment {
    private RecyclerView recyclerViewNotifications;
    private ArrayList<Notification> notifications = new ArrayList<>();
    private NotificationRecyclerAdapter notificationRecyclerAdapter;
    private ImageFetcher imageFetcher;
    private StaggeredGridLayoutManager notificationLayoutManager;

    private int get_start_no = 0;
    private boolean loading = false;

    private boolean isEnd = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageFetcher = new ImageFetcher(getActivity());
        setHasOptionsMenu(true);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        notificationLayoutManager.setSpanCount(1);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        recyclerViewPurchaseHistory.removeAllViews();
//        recyclerViewPurchaseHistory = null;
//        System.gc();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.app_bar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        String toolbar_title = "Notifications".toUpperCase();
        toolbar.setTitle(toolbar_title);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        recyclerViewNotifications = (RecyclerView) view.findViewById(R.id.recyclerViewPurchaseHistory);
        notificationLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        recyclerViewNotifications.setLayoutManager(notificationLayoutManager);
        notificationRecyclerAdapter = new NotificationRecyclerAdapter(notifications, imageFetcher, getContext());
        if (isEnd) {
            notificationRecyclerAdapter.removeFooter();
        }
        recyclerViewNotifications.setAdapter(notificationRecyclerAdapter);
        recyclerViewNotifications.addOnScrollListener(new EndlessRecyclerViewScrollListener(notificationLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadMorePosts();
            }
        });

        if (notifications.size() == 0) {
            loadMorePosts();
        }

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_none, menu);
        Utils.showMenuNotificationCount(menu, getActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_notification) {
            ((MainActivity) getActivity()).viewNotifications();
            return true;
        } else if (id == android.R.id.home) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.popBackStackImmediate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void loadMorePosts() {
        if (!loading && !isEnd) {
            loading = true;
            Map<String, String> parameterMap = new HashMap<>();
            int get_count = 12;
            parameterMap.put("target_member_sn", ((MainActivity) getActivity()).getPreferences("member_sn"));
            parameterMap.put("get_count", String.valueOf(get_count));
            parameterMap.put("get_start_no", String.valueOf(get_start_no));
            String url = GlobalConstants.DOMAIN + "/getNotifications.do?";
            for (String key : parameterMap.keySet()) {
                url += key + "=" + parameterMap.get(key) + "&";
            }
            HttpRequest httpRequest = new HttpRequest(getContext());
            HttpRequest.HttpGetRequestTask studiosHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                @Override
                public void callback(JSONObject jsonObject, Context context) {
                    try {
                        int count = jsonObject.getInt("count");
                        JSONArray notificationJSONArray = jsonObject.getJSONArray("notifications");
                        JSONObject notificationJSONObject;
                        for (int i = 0; i < notificationJSONArray.length(); i++) {
                            notificationJSONObject = notificationJSONArray.getJSONObject(i);
                            notifications.add(new Notification().setNotification(
                                    notificationJSONObject.getString("notification_sn"),
                                    notificationJSONObject.getString("target_member_sn"),
                                    notificationJSONObject.getString("target_dc"),
                                    notificationJSONObject.getString("target_sn"),
                                    notificationJSONObject.getString("post_dc"),
                                    notificationJSONObject.getString("action_cd"),
                                    notificationJSONObject.getString("action_member_sn"),
                                    notificationJSONObject.getString("description"),
                                    notificationJSONObject.getString("read_yn"),
                                    Utils.getTimeLag(notificationJSONObject.getString("created_datetime").substring(0, 19), getContext())
                            ));
                            notificationRecyclerAdapter.notifyItemInserted(get_start_no + 1 + i);
                        }
                        get_start_no = get_start_no + notificationJSONArray.length();
                        if (notifications.size() < count) {
                            loading = false;
                        } else {
                            Handler mHandler = new Handler();
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    notificationRecyclerAdapter.removeFooter();
                                    isEnd = true;
                                    notificationRecyclerAdapter.notifyItemRemoved(get_start_no + 2);
                                }
                            }, 500);
                        }

                    } catch (JSONException | ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
            studiosHttpGetRequestTask.execute(url);
        }
    }

}
