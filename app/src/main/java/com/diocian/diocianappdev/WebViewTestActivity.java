package com.diocian.diocianappdev;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.diocian.diocianappdev.util.GlobalConstants;

public class WebViewTestActivity extends AppCompatActivity {

    private static final String target_url_prefix = "www.diocian.com";
    private Context context;
    private WebView webViewTestPop;
    private FrameLayout frameLayoutWebView;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview_test);

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);

        frameLayoutWebView = (FrameLayout) findViewById(R.id.frameLayoutWebView);
        WebView webViewTest = (WebView) findViewById(R.id.webViewTest);
        if (webViewTest != null) {
            webViewTest.clearCache(true);
            webViewTest.clearHistory();
            clearCookies(this);
            WebSettings webSettingsTest = webViewTest.getSettings();
            webSettingsTest.setJavaScriptEnabled(true);
            webSettingsTest.setAppCacheEnabled(true);
            webSettingsTest.setJavaScriptCanOpenWindowsAutomatically(true);
            webSettingsTest.setSupportMultipleWindows(true);

            TestJavascriptInterface testJavascriptInterface = new TestJavascriptInterface(this, webViewTest);
            webViewTest.addJavascriptInterface(testJavascriptInterface, "Diocian");

            webViewTest.setWebViewClient(new TestWebViewClient());
            webViewTest.setWebChromeClient(new TestWebChromeClient());
            webViewTest.loadUrl(GlobalConstants.DOMAIN + "/facebook/login/");
        }
        context = getApplicationContext();

    }

    @SuppressWarnings("deprecation")
    public static void clearCookies(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }

    private class TestWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            String host = Uri.parse(url).getHost();
            if (host.equals(target_url_prefix)) {
                if (webViewTestPop != null) {
                    webViewTestPop.setVisibility(View.GONE);
                    frameLayoutWebView.removeView(webViewTestPop);
                    webViewTestPop = null;
                }
                return false;
            }
            if (host.equals("m.facebook.com") || host.equals("www.facebook.com")) {
                return false;
            }
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            return true;
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
//            Log.d("TEST----->", "onReceivedSslError");
            //super.onReceivedSslError(view, handler, error);
        }
    }

    class TestWebChromeClient extends WebChromeClient {

        @SuppressLint("SetJavaScriptEnabled")
        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
            webViewTestPop = new WebView(context);
            webViewTestPop.setVerticalScrollBarEnabled(false);
            webViewTestPop.setHorizontalScrollBarEnabled(false);
            webViewTestPop.setWebViewClient(new TestWebViewClient());
            webViewTestPop.getSettings().setJavaScriptEnabled(true);
//            webViewTestPop.getSettings().setSavePassword(false);
            webViewTestPop.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            frameLayoutWebView.addView(webViewTestPop);
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(webViewTestPop);
            resultMsg.sendToTarget();
            return true;
        }

        @Override
        public void onCloseWindow(WebView window) {
            Log.d("onCloseWindow", "called");
        }

    }

    public class TestJavascriptInterface {

        private Activity activity;
        private WebView webView;

        public TestJavascriptInterface(Activity activity, WebView webView) {
            this.activity = activity;
            this.webView = webView;
        }

        @JavascriptInterface
        public void showMessageToSnackbar(String message) {
            Snackbar.make(webView, message, Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }

    }

}
