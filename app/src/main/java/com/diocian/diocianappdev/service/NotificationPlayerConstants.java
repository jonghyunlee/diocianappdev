package com.diocian.diocianappdev.service;

public class NotificationPlayerConstants {
    public interface ACTION {
        String MAIN_ACTION = "com.diocian.diocianappdev.player.action.main";
        String INIT_ACTION = "com.diocian.diocianappdev.player.action.init";
        String PREV_ACTION = "com.diocian.diocianappdev.player.action.prev";
        String PLAY_ACTION = "com.diocian.diocianappdev.player.action.play";
        String NEXT_ACTION = "com.diocian.diocianappdev.player.action.next";
        String START_ACTION = "com.diocian.diocianappdev.player.action.start";
        String STOP_ACTION = "com.diocian.diocianappdev.player.action.stop";
    }

    public interface NOTIFICATION_ID {
        int FOREGROUND_SERVICE = 101;
    }

}
