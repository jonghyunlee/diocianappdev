package com.diocian.diocianappdev.service;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.model.Playlist;
import com.diocian.diocianappdev.util.GlobalConstants;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class NotificationPlayerService extends Service {

    RemoteViews views;
    Notification status;
    String cover_image_url;
    String title;
    String artist_name;
    String audio_file_url;
    boolean isAutoPlay;
    boolean isRestart;
    String postSn;
    public ArrayList<Playlist> playlists = new ArrayList<>();

    private final IBinder mBinder = new LocalBinder();
    Callbacks activity;

    public MediaPlayer mediaPlayer;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {
        public NotificationPlayerService getServiceInstance() {
            return NotificationPlayerService.this;
        }
    }

    public void registerClient(Activity activity) {
        this.activity = (Callbacks) activity;
    }

    public interface Callbacks {
        void playNextCallback();

        void playPrevCallback();

        void playCallback();

        void pauseCallback();

        String getActivePostSn();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            if (intent.getAction().equals(NotificationPlayerConstants.ACTION.START_ACTION)) {
                cover_image_url = intent.getStringExtra("cover_image_url");
                title = intent.getStringExtra("title");
                artist_name = intent.getStringExtra("artist_name");
                audio_file_url = intent.getStringExtra("audio_file_url");
                isAutoPlay = intent.getBooleanExtra("isAutoPlay", false);
                isRestart = intent.getBooleanExtra("isRestart", false);
                postSn = intent.getStringExtra("postSn");
                showNotification();
//                Toast.makeText(this, "Service Started", Toast.LENGTH_SHORT).show();
            } else if (intent.getAction().equals(NotificationPlayerConstants.ACTION.PREV_ACTION)) {
                if (activity.getActivePostSn().equals("")) {
                    playPrev();
                } else {
                    activity.playPrevCallback();
                }
//                playPrev();
//                Toast.makeText(this, "Clicked Previous", Toast.LENGTH_SHORT).show();
            } else if (intent.getAction().equals(NotificationPlayerConstants.ACTION.PLAY_ACTION)) {
                playPausePlayer();
//                Toast.makeText(this, "Clicked Play", Toast.LENGTH_SHORT).show();
            } else if (intent.getAction().equals(NotificationPlayerConstants.ACTION.NEXT_ACTION)) {
                Log.d(GlobalConstants.LOG_TAG, "activity.getActivePostSn() : " + activity.getActivePostSn());
                if (activity.getActivePostSn().equals("")) {
                    playNext();
                } else {
                    activity.playNextCallback();
                }
//                playNext();
//                Toast.makeText(this, "Clicked Next", Toast.LENGTH_SHORT).show();
            } else if (intent.getAction().equals(NotificationPlayerConstants.ACTION.STOP_ACTION)) {
                Toast.makeText(this, "Service Stoped", Toast.LENGTH_SHORT).show();
                if (mediaPlayer != null) {
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
                stopForeground(true);
                stopSelf();
            }
        }
        return START_STICKY;
    }

    public void playPausePlayer() {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                views.setImageViewResource(R.id.imageButtonPlay, R.drawable.ic_play_arrow_white_24dp);
                activity.pauseCallback();
            } else {
                mediaPlayer.start();
                views.setImageViewResource(R.id.imageButtonPlay, R.drawable.ic_pause_white_24dp);
                activity.playCallback();
            }
            startForeground(NotificationPlayerConstants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);
        }
    }

    private void showNotification() {
        views = new RemoteViews(getPackageName(), R.layout.status_bar_player);

        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.putExtra("isCalledPlayerService", true);
        notificationIntent.putExtra("activePlayPostSn", postSn);
        if (mediaPlayer != null) {
            notificationIntent.putExtra("isPlayWithStart", mediaPlayer.isPlaying());
        } else {
            notificationIntent.putExtra("isPlayWithStart", false);
        }

        notificationIntent.putExtra("testextra", "Y");

        notificationIntent.setAction(NotificationPlayerConstants.ACTION.MAIN_ACTION);
//        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent prevIntent = new Intent(this, NotificationPlayerService.class);
        prevIntent.setAction(NotificationPlayerConstants.ACTION.PREV_ACTION);
        PendingIntent pendingPrevIntent = PendingIntent.getService(this, 0, prevIntent, 0);

        Intent playIntent = new Intent(this, NotificationPlayerService.class);
        playIntent.setAction(NotificationPlayerConstants.ACTION.PLAY_ACTION);
        PendingIntent pendingPlayIntent = PendingIntent.getService(this, 0, playIntent, 0);

        Intent nextIntent = new Intent(this, NotificationPlayerService.class);
        nextIntent.setAction(NotificationPlayerConstants.ACTION.NEXT_ACTION);
        PendingIntent pendingNextIntent = PendingIntent.getService(this, 0, nextIntent, 0);

        Intent closeIntent = new Intent(this, NotificationPlayerService.class);
        closeIntent.setAction(NotificationPlayerConstants.ACTION.STOP_ACTION);
        PendingIntent pendingCloseIntent = PendingIntent.getService(this, 0, closeIntent, 0);

        views.setOnClickPendingIntent(R.id.imageButtonPlay, pendingPlayIntent);
        views.setOnClickPendingIntent(R.id.imageButtonNext, pendingNextIntent);
        views.setOnClickPendingIntent(R.id.imageButtonPrev, pendingPrevIntent);
        views.setOnClickPendingIntent(R.id.imageButtonClose, pendingCloseIntent);
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                views.setImageViewResource(R.id.imageButtonPlay, R.drawable.ic_pause_white_24dp);
                activity.playCallback();
            } else {
                views.setImageViewResource(R.id.imageButtonPlay, R.drawable.ic_play_arrow_white_24dp);
                activity.pauseCallback();
            }
            startForeground(NotificationPlayerConstants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);
        }

        if (cover_image_url == null || cover_image_url.equals("") || cover_image_url.equals(GlobalConstants.DOMAIN)) {
            views.setImageViewResource(R.id.imageViewCoverImage, R.drawable.image_none);
        } else {
            BitmapWorkerTask task = new BitmapWorkerTask();
            task.execute(cover_image_url);
        }

        views.setTextViewText(R.id.textViewTitle, title);
        views.setTextViewText(R.id.textViewArtistName, artist_name);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            status = new Notification.Builder(this).build();
            status.contentView = views;
            status.flags = Notification.FLAG_ONGOING_EVENT;
            status.icon = R.drawable.ic_notification;
            status.contentIntent = pendingIntent;
            if(audio_file_url != null) {
                setBottomPlayer(isAutoPlay, audio_file_url, isRestart);
            }
            startForeground(NotificationPlayerConstants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);
        }
    }

    public class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
        public BitmapWorkerTask() {
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            Bitmap bitmap = null;
            try {
                URL url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                bitmap = BitmapFactory.decodeStream(input);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            views.setImageViewBitmap(R.id.imageViewCoverImage, bitmap);
            startForeground(NotificationPlayerConstants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);
        }
    }

    public void setBottomPlayer(final boolean isAutoPlay, String audio_file_url, boolean isRestart) {
        Log.d(GlobalConstants.LOG_TAG, "audio_file_url : " + audio_file_url);
        Log.d(GlobalConstants.LOG_TAG, "isAutoPlay : " + isAutoPlay);
        Log.d(GlobalConstants.LOG_TAG, "isRestart : " + isRestart);
        try {
            if (isRestart) {
                if (mediaPlayer != null) {
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setDataSource(audio_file_url);
                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        if (isAutoPlay) {
                            mp.start();
                            views.setImageViewResource(R.id.imageButtonPlay, R.drawable.ic_pause_white_24dp);
                            startForeground(NotificationPlayerConstants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);
                            activity.playCallback();
//                            btn_playImageButton.setImageResource(R.drawable.ic_pause_white_24dp);
                        }
                    }
                });
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        activity.playNextCallback();
                    }
                });

                mediaPlayer.prepareAsync();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void playNext() {
        int nextIndex = 0;
        for (int i = 0; i < playlists.size(); i++) {
            if (playlists.get(i).getTarget_sn().equals(postSn)) {
                nextIndex = i + 1;
            }
        }
        if (nextIndex >= playlists.size()) nextIndex = 0;
        postSn = playlists.get(nextIndex).getTarget_sn();
        cover_image_url = playlists.get(nextIndex).getCover_image_url();
        if (cover_image_url == null || cover_image_url.equals("") || cover_image_url.equals(GlobalConstants.DOMAIN)) {
            views.setImageViewResource(R.id.imageViewCoverImage, R.drawable.image_none);
        } else {
            BitmapWorkerTask task = new BitmapWorkerTask();
            task.execute(cover_image_url);
        }
        title = playlists.get(nextIndex).getTitle();
        artist_name = playlists.get(nextIndex).getArtist_name();
        if (artist_name.equals("")) artist_name = playlists.get(nextIndex).getMember_name();
        views.setTextViewText(R.id.textViewTitle, title);
        views.setTextViewText(R.id.textViewArtistName, artist_name);
        startForeground(NotificationPlayerConstants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);

        setBottomPlayer(true, playlists.get(nextIndex).getAudio_file_url(), true);
    }

    private void playPrev() {
        int nextIndex = 0;
        for (int i = 0; i < playlists.size(); i++) {
            if (playlists.get(i).getTarget_sn().equals(postSn)) {
                nextIndex = i - 1;
            }
        }
        if (nextIndex < 0) nextIndex = playlists.size() - 1;
        postSn = playlists.get(nextIndex).getTarget_sn();
        cover_image_url = playlists.get(nextIndex).getCover_image_url();
        if (cover_image_url == null || cover_image_url.equals("") || cover_image_url.equals(GlobalConstants.DOMAIN)) {
            views.setImageViewResource(R.id.imageViewCoverImage, R.drawable.image_none);
        } else {
            BitmapWorkerTask task = new BitmapWorkerTask();
            task.execute(cover_image_url);
        }
        title = playlists.get(nextIndex).getTitle();
        artist_name = playlists.get(nextIndex).getArtist_name();
        if (artist_name.equals("")) artist_name = playlists.get(nextIndex).getMember_name();
        views.setTextViewText(R.id.textViewTitle, title);
        views.setTextViewText(R.id.textViewArtistName, artist_name);
        startForeground(NotificationPlayerConstants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);

        setBottomPlayer(true, playlists.get(nextIndex).getAudio_file_url(), true);
    }
}
