package com.diocian.diocianappdev.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GlobalConstants {

    public static final String DOMAIN = "http://www.diocian.com";
    public static final String LOG_TAG = "DIOCIAN Test ";
    public static final String LOG_PREFIX = "-------------------- ";
    public static final String LOCATION_CODES[] = {"US", "JP", "KR", "VN"};


}
