package com.diocian.diocianappdev.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.v4.util.LruCache;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.URL;

public class ImageFetcher {

    private LruCache<String, Bitmap> mMemoryCache;
    protected Resources mResources;

    private DiskLruCache mDiskLruCache;
    private final Object mDiskCacheLock = new Object();
    private boolean mDiskCacheStarting = true;
    private static final int DISK_CACHE_SIZE = 1024 * 1024 * 20; // 10MB
    private static final String DISK_CACHE_SUBDIR = "thumbnails";

    public static final int IO_BUFFER_SIZE = 8 * 1024;

    private Bitmap.CompressFormat mCompressFormat = Bitmap.CompressFormat.JPEG;
    private int mCompressQuality = 70;

    public ImageFetcher(Context context) {
        mResources = context.getResources();
        initBitmapMemoryCache();
        File cacheDir = getDiskCacheDir(context, DISK_CACHE_SUBDIR);
        new InitDiskCacheTask().execute(cacheDir);
    }

    public void initBitmapMemoryCache() {
        if (mMemoryCache == null) {
            final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
            final int cacheSize = maxMemory / 16;
            mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
                @Override
                protected int sizeOf(String key, Bitmap bitmap) {
                    return bitmap.getByteCount() / 1024;
                }
            };
        }
    }

    public static File getDiskCacheDir(Context context, String uniqueName) {
        final String cachePath = Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) || !isExternalStorageRemovable() ? getExternalCacheDir(context).getPath() : context.getCacheDir().getPath();
        return new File(cachePath + File.separator + uniqueName);
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public static boolean isExternalStorageRemovable() {
        return !Utils.hasGingerbread() || Environment.isExternalStorageRemovable();
    }

    @TargetApi(Build.VERSION_CODES.FROYO)
    public static File getExternalCacheDir(Context context) {
        if (Utils.hasFroyo()) {
            return context.getExternalCacheDir();
        }
        final String cacheDir = "/Android/data/" + context.getPackageName() + "/cache/";
        return new File(Environment.getExternalStorageDirectory().getPath() + cacheDir);
    }

    class InitDiskCacheTask extends AsyncTask<File, Void, Void> {
        @Override
        protected Void doInBackground(File... params) {
            synchronized (mDiskCacheLock) {
                File cacheDir = params[0];
                try {
                    mDiskLruCache = DiskLruCache.open(cacheDir, 1, 1, DISK_CACHE_SIZE);
                    mDiskCacheStarting = false;
                    mDiskCacheLock.notifyAll();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) throws IOException {
        key = getKeyFromUrl(key);
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
        synchronized (mDiskCacheLock) {
            if (mDiskLruCache != null && mDiskLruCache.get(key) == null) {
                put(key, bitmap);
            }
        }
    }

    public String getKeyFromUrl(String url) {
        String[] keys = url.split("/");
        String key;
        if (keys.length > 1) {
            key = keys[keys.length - 1];
            if (key.length() < 7 && keys.length > 2) {
                key = keys[keys.length - 2];
            }
        } else {
            key = url;
        }
        return key;
    }

    public void put(String key, Bitmap data) {
        key = getKeyFromUrl(key);
        DiskLruCache.Editor editor = null;
        try {
            editor = mDiskLruCache.edit(key);
            if (editor == null) {
                return;
            }
            if (writeBitmapToFile(data, editor, key)) {
                mDiskLruCache.flush();
                editor.commit();
            } else {
                editor.abort();
            }
        } catch (IOException e) {
            e.printStackTrace();
            try {
                if (editor != null) {
                    editor.abort();
                }
            } catch (IOException ignored) {

            }
        }
    }

    private boolean writeBitmapToFile(Bitmap bitmap, DiskLruCache.Editor editor, String key) throws IOException {
        OutputStream out = null;
        try {
            out = new BufferedOutputStream(editor.newOutputStream(0), IO_BUFFER_SIZE);
            String[] keys = key.split("\\.");
            String ext = "";
            if (keys.length > 1) {
                ext = keys[keys.length - 1].toLowerCase();
            }
            if (ext.equals("jpg") || ext.equals("jpeg") || ext.equals("gif")) {
                mCompressFormat = Bitmap.CompressFormat.JPEG;
                mCompressQuality = 70;
            } else if (ext.equals("png")) {
                mCompressFormat = Bitmap.CompressFormat.PNG;
                mCompressQuality = 100;
            }
            return bitmap.compress(mCompressFormat, mCompressQuality, out);
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        key = getKeyFromUrl(key);
        return mMemoryCache.get(key);
    }

    public Bitmap getBitmap(String key) {
        synchronized (mDiskCacheLock) {
            while (mDiskCacheStarting) {
                try {
                    mDiskCacheLock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (mDiskLruCache != null) {
                Bitmap bitmap = null;
                DiskLruCache.Snapshot snapshot = null;
                try {
                    snapshot = mDiskLruCache.get(getKeyFromUrl(key));
                    if (snapshot == null) {
                        return null;
                    }
                    final InputStream in = snapshot.getInputStream(0);
                    if (in != null) {
                        final BufferedInputStream buffIn = new BufferedInputStream(in, IO_BUFFER_SIZE);
                        bitmap = BitmapFactory.decodeStream(buffIn);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (snapshot != null) {
                        snapshot.close();
                    }
                }
                return bitmap;
            }
        }
        return null;
    }

    public class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {

        private final WeakReference<ImageView> imageViewReference;
        private String data = "";

        int reqWidth = 24;

        public BitmapWorkerTask(ImageView imageView, int reqWidth) {
            imageViewReference = new WeakReference<>(imageView);
            int density = (int) (mResources.getDisplayMetrics().density * 1);
//            int density = (int) imageView.getContext().getResources().getDisplayMetrics().density;
            this.reqWidth = reqWidth * density;
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            data = urls[0];
            Bitmap bitmap = getBitmap(data);
            if (bitmap == null) {
                try {
                    bitmap = decodeSampledBitmapFromUrl(new URL(urls[0]), reqWidth);
                    if (bitmap != null) addBitmapToMemoryCache(data, bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }

            if (imageViewReference != null && bitmap != null) {
                final ImageView imageView = imageViewReference.get();
                final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);
                if (this == bitmapWorkerTask) {
                    imageView.setImageBitmap(bitmap);
                }
            }

        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth) {
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (width > reqWidth) {
            while ((width / inSampleSize) > reqWidth) {
                inSampleSize += 1;
            }
        }
        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromUrl(URL url, int reqWidth) throws IOException {
        final BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(url.openStream(), null, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth);

        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeStream(url.openStream(), null, options);
    }

    public static class AsyncDrawable extends BitmapDrawable {
        private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

        public AsyncDrawable(Resources res, Bitmap bitmap, BitmapWorkerTask bitmapWorkerTask) {
            super(res, bitmap);
            bitmapWorkerTaskReference = new WeakReference<>(bitmapWorkerTask);
        }

        public BitmapWorkerTask getBitmapWorkerTask() {
            return bitmapWorkerTaskReference.get();
        }
    }

    public static boolean cancelPotentialWork(String data, ImageView imageView) {
        final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);
        if (bitmapWorkerTask != null) {
            final String bitmapData = bitmapWorkerTask.data;
            if (bitmapData.equals("") || !bitmapData.equals(data)) {
                bitmapWorkerTask.cancel(true);
            } else {
                return false;
            }
        }
        return true;
    }

    private static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
                return asyncDrawable.getBitmapWorkerTask();
            }
        }
        return null;
    }

    public void setImageToImageView(String url, ImageView imageView, int reqWidth) {
        final Bitmap bitmap = getBitmapFromMemCache(url);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else if (cancelPotentialWork(url, imageView)) {
            final BitmapWorkerTask task = new BitmapWorkerTask(imageView, reqWidth);
            final AsyncDrawable asyncDrawable = new AsyncDrawable(mResources, null, task);
            imageView.setImageDrawable(asyncDrawable);
            task.execute(url);
        }
    }

}
